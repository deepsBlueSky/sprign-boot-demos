package com.xxjob.jobHander;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description: job处理器
 * @author: Kangsen
 * @create: 2021-09-17 09:37
 **/
@Component
@Slf4j
public class DemoJobHandler{

    private static String name = "张三";

    @XxlJob("demoJobHandler")
    public void execute() throws Exception {
        //获取xxl-job配置的参数
        log.info("DemoJobHandler job execute》》》》》》");
        log.info("XXL-JOB, Hello World.");
        name = "lisi";
        log.info("设置name为：lisi");
    }

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("demo1JobHandler")
    public void demoJobHandler() throws Exception {
        XxlJobHelper.log("XXL-JOB, Hello World.");
        log.info("读取name：{}",name);
    }
}
