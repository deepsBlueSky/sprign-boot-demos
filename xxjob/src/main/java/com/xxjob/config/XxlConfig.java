package com.xxjob.config;

import com.xxl.job.core.executor.XxlJobExecutor;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @program: SpringBootDemos
 * @description: job配置文件
 * @author: Kangsen
 * @create: 2021-09-17 09:34
 **/
@Configuration
@ComponentScan(basePackages = "com.xxjob.jobHander")
@Slf4j
public class XxlConfig {
    @Value("${xxl.job.admin.addresses}")
    private String adminAddress;
    @Value("${xxl.job.executor.appname}")
    private String appName;
    @Value("${xxl.job.executor.ip}")
    private String ip;
    @Value("${xxl.job.executor.port}")
    private int port;
    @Value("${xxl.job.accessToken}")
    private String accessToken;
    @Value("${xxl.job.executor.logpath}")
    private String logPath;
    @Value("${xxl.job.executor.logretentiondays}")
    private int logRetentionDays;

    //这样写会在控制台报端口占用错误
    //@Bean(initMethod = "start",destroyMethod = "destroy")
    @Bean()
    public XxlJobSpringExecutor xxlJobExecutor(){
        log.info("===========================xxl-job yingyuehu.redis.config init=================================");
        XxlJobSpringExecutor xxlJobExecutor = new XxlJobSpringExecutor();
        xxlJobExecutor.setAdminAddresses(adminAddress);
        xxlJobExecutor.setAppname(appName);
        xxlJobExecutor.setIp(ip);
        xxlJobExecutor.setPort(port);
        xxlJobExecutor.setAccessToken(accessToken);
        xxlJobExecutor.setLogPath(logPath);
        xxlJobExecutor.setLogRetentionDays(logRetentionDays);
        log.info("===========================xxl-job yingyuehu.redis.config init end==============================");
        return xxlJobExecutor;
    }
}
