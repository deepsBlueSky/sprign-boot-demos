import org.apache.commons.lang3.StringUtils;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-09-16 17:40
 **/
public class Test {
    public static void main(String[] args) {
        String a =  null;
        String b = "";
        System.out.println(StringUtils.isBlank(a));
        System.out.println(StringUtils.isBlank(b));
        System.out.println(StringUtils.isEmpty(a));
        System.out.println(StringUtils.isEmpty(b));
    }
}
