import com.mt.RwDemo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-10 11:34
 **/
@Slf4j
public class TestRwFile {

    public void t1(){
        RwDemo rwDemo = new RwDemo();
        //源文件路径
        String str = "";
        String p1 = "";
        String p2 = "";
        String p3 = "";
        rwDemo.rwBufferFile(str,p1);

        long readSize = 100 * 1024 * 1024;
        rwDemo.rwTheadsFile(str,p2,readSize);

        rwDemo.rwByteFile(str,p3);
    }


    @Test
    public void t2(){
        String str = "HELLO, WORLD";
        byte[] bytes = str.getBytes();
        log.info("bytes:{}   bytes.length:{}",bytes,bytes.length);

        //方法一
        System.out.println(System.getProperty("file.encoding"));

        //方法二
        System.out.println(Charset.defaultCharset());
    }


}
