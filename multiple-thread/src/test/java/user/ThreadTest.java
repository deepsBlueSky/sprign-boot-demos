package user;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-24 10:23
 **/

public class ThreadTest {

    public static class ImplementsRunnable implements Runnable {
        private int counter = 0;
        public void run() {
            counter++;
            System.out.println("ImplementsRunnable : Counter : " + counter);
        }
    }

    //Extend Thread class...
    public static class ExtendsThread extends Thread {
        private int counter = 0;
        public void run() {
            counter++;
            System.out.println("ExtendsThread : Counter : " + counter);
        }
    }

    //Use the above classes here in main to understand the differences more clearly...
    public static void main(String args[]) throws Exception {
        ImplementsRunnable rc = new ImplementsRunnable();
        Thread t1 = new Thread(rc);
        t1.start();

        Thread.sleep(1000); // Waiting for 1 second before starting next thread
        Thread t2 = new Thread(rc);
        t2.start();

        Thread.sleep(1000); // Waiting for 1 second before starting next thread
        Thread t3 = new Thread(rc);
        t3.start();

        // Creating new instance for every thread access.
        ExtendsThread tc1 = new ExtendsThread();
        tc1.start();

        Thread.sleep(1000); // Waiting for 1 second before starting next thread
        ExtendsThread tc2 = new ExtendsThread();
        tc2.start();

        Thread.sleep(1000); // Waiting for 1 second before starting next thread
        ExtendsThread tc3 = new ExtendsThread();
        tc3.start();
    }
}
