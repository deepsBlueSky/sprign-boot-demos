package com.mt;

import lombok.Data;

import java.io.*;
import java.util.concurrent.*;

/**
 * @author kratos
 * @date 2022-07-20 17:47:19
 */
public class RwDemo {
    public void rwTheadsFile(String inFilePath, String outFilePath,long readSize) {
        File file = new File(inFilePath);
        if (!file.exists() || file.length() < 1) {
            return;
        }
        System.out.println("开始用>>>>>>>>>>>>多线程读写文件,文件大小是" + file.length() + "字节");
        long sTime = System.currentTimeMillis();

        try {
            //读取的内容长度控制,一定不要超过int的取值范围，要不然会报错。因为read的入参范围是int，byte数组的范围也是int
            // long readSize = 10 * 1024 * 1024;
           // long readSize = 100 * 1024 * 1024;

            //启动的线程个数,启动线程数要+1，要不然除法不是倍数的关系会导致丢失其余的数据
            int num = (int) (file.length() / readSize + 1);
            System.out.println("开始读取文件,需要创建" + num + "个线程");
            //选用ArrayBlockingQueue有界队列，队列长度为num
            ExecutorService service =
                    new ThreadPoolExecutor(1, num, 0, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));
            int content = 0;
            long off = 0;
            for (int i = 0; i < num; i++) {
                //每次读取i*1024的内容
                if (i == 0) {
                    off = 0;
                } else {
                    off = i * readSize + 1;
                }
                if (i == num - 1) {
                    //最后一次
                    content = (int) (file.length() - readSize * i);
                } else {
                    content = (int) readSize;
                }
                // System.out.println("文件大小是：" + file.length() + ",第" + (i + 1) + "次读取开始,开始位置是：" + off + ",要读取的内容是：" + content);
                Future f = service.submit(new RwTask(file, off, content, outFilePath));

                if (f.isDone()) {
                    continue;
                }
                Thread.sleep(10);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        long eTime = System.currentTimeMillis();
        System.out.println("多线程读写文件结束，共耗时" + (eTime - sTime) + "毫秒");
    }


    @Data
    class RwTask implements Runnable {
        private byte[] fileContent;
        //要读取的文件
        private File file;
        //读取的文件开始位置
        private long off;
        //每次读多少
        private int len;

        private String outFilePath;

        public RwTask(File file, long off, int len, String outFilePath) {
            this.file = file;
            this.off = off;
            this.len = len;
            this.outFilePath = outFilePath;
        }

        @Override
        public void run() {
            RandomAccessFile rand = null;
            //读写文件
            try {
                //  System.out.println(Thread.currentThread().getName());
                rand = new RandomAccessFile(file, "rw");
                fileContent = new byte[len];
                //必须要在读之前重置指针，如果读之后重置指针，会发生下一次读取到上一次存在缓冲区的数据。
                rand.seek(off);
                rand.read(fileContent, 0, len);

                //System.out.println(new String(fileContent));
                OutputStream os = new FileOutputStream(outFilePath, true);
                //分段写，一次写1024字节倍数的内容
                int offset = 1024;
                boolean flag = len % offset == 0;
                int num = flag ? len / offset : len / offset + 1;
                int length = offset;
                int destPosOff = offset;
                for (int i = 0; i < num; i++) {

                    int destPos = 0;
                    if (i > 0) {
                        destPos = destPosOff * i;
                    }

                    //判断最后一次
                    if (i == num - 1) {
                        //因为下标是从0开始的，所以最后一次需要注意数组下标越界
                        if (!flag) {
                            length = offset = len - i * destPosOff;
                            //  offset = len - i * destPosOff;
                        }
                    }
                    byte temp[] = new byte[length];
                    //截取数组
                    // System.out.println("第" + i + "次：destpos=" + destPos + "偏移量=" + offset);
                    System.arraycopy(fileContent, destPos, temp, 0, offset);
                    os.write(temp);
                }
                os.flush();
                os.close();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (null != rand) {
                    try {
                        rand.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }


    public void rwBufferFile(String inFilePth, String outFilePath) {
        File file = new File(inFilePth);
        if (!file.exists() || file.length() < 1) {
            return;
        }
        System.out.println("开始用$$$$$$$$$$$$$$$字符流读写文件,文件大小是:" + file.length() + "字节");
        long sTime = System.currentTimeMillis();

        try {

            BufferedReader is = new BufferedReader(new InputStreamReader(new FileInputStream(inFilePth), "gbk"));
            BufferedWriter os = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFilePath), "gbk"));

            String con = "";
            while ((con = is.readLine()) != null) {
                //System.out.println("con=" + con);
                os.write(con);
                // os.newLine();
            }
            os.flush();
            os.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        long eTime = System.currentTimeMillis();
        System.out.println("字符流读写文件结束，共耗时" + (eTime - sTime) + "毫秒");
    }

    public void rwByteFile(String inFilePth, String outFilePath) {

        File file = new File(inFilePth);
        if (!file.exists() || file.length() < 1) {
            return;
        }
        System.out.println("开始用|||||||||||||||字节流读写文件,文件大小是：" + file.length() + "字节");
        long sTime = System.currentTimeMillis();
        try {

            InputStream is = new FileInputStream(inFilePth);
            OutputStream os = new FileOutputStream(outFilePath);

            int len = 0;
            byte by[] = new byte[1024];

            while ((len = is.read()) != -1) {
                is.read(by, 0, len);
                os.write(by, 0, len);
            }

            os.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long eTime = System.currentTimeMillis();
        System.out.println("字节流读写文件结束，共耗时" + (eTime - sTime) + "毫秒");
    }

}
