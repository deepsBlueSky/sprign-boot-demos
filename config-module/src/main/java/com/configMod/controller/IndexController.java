package com.configMod.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：kangsen
 * @Date：2022/10/24/0024 22:22
 * @Versiion：1.0
 * @Desc:
 */
@RestController
@RefreshScope
public class IndexController {

    @Value("${tt.str}")
    private String testProperties;

    @GetMapping("/index")
    public String test(){
        return testProperties;
    }

}
