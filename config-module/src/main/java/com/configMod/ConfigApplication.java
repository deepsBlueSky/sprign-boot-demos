package com.configMod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author：kangsen
 * @Date：2022/10/24/0024 20:59
 * @Versiion：1.0
 * @Desc:
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigApplication {
    public static void main(String[] args) {
        // 新加代码
        System.setProperty("ignoreUnresolvablePlaceholders", "true");
        SpringApplication.run(ConfigApplication.class,args);
    }
}
