package com.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * todo:2021年10月27日16:09:59 作为nacos 的生产者微服务
 *
 * @program: SpringBootDemos
 * @description: 消费者
 * @author: Kangsen
 * @create: 2021-08-24 10:03
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class,args);
    }

    @Bean
    //切记很重要 要加负载才能访问
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    /*@Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory simpleClientHttpRequestFactory){
        return new RestTemplate(simpleClientHttpRequestFactory);
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory =
                new SimpleClientHttpRequestFactory();
        simpleClientHttpRequestFactory.setReadTimeout(180000);//单位ms
        simpleClientHttpRequestFactory.setConnectTimeout(5000);//单位ms
        return simpleClientHttpRequestFactory;
    }*/
}
