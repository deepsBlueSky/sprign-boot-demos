package com.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-27 16:19
 **/
@RestController
public class ProvideController {
    @Autowired
    private RestTemplate restTemplate;


    /*public ProvideController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }*/

    @GetMapping("/get/{name}")
    public String test(@PathVariable String name){
        return restTemplate.getForObject("http://rb-producter/echo/"+name,String.class);
    }

    public static void main(String[] args) {
        Set<String> paths = new HashSet<>();
        paths.add("https://cdn.xatdyun.com/head/2021/10/26/");
        paths.add("https://cdn.xatdyun.com/head/2021/10/27/");
        paths.add("");
        paths.add("https://cdn.xatdyun.com/head/2021/10/28/");
        paths.add("https://cdn.xatdyun.com/head/2021/10/29/");
        System.out.println(paths);

        String a = "https://img.huil21.com/head/2021/5/9/1620547036316_1440h_864w.jpeg";
        String b = "img.huil21.com";
        System.out.println(a.contains(b));
        System.out.println(b.contains(a));
    }
}
