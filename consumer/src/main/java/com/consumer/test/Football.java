package com.consumer.test;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-18 10:58
 **/
public class Football {
    protected int score;

    public Football(int score) {
        this.score = score;
    }

    public void score(){
        System.out.println("Score : " + score);
    }
    public void kick(){}

    public static void main(String[] args) {
        Football football = new Football(7){
            @Override
            public void score() {
                System.out.println("Anonymous class inside this method " + score);
                super.score();
            }
        };
        football.score();
    }
}
