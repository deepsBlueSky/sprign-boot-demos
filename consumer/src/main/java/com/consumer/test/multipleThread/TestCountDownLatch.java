package com.consumer.test.multipleThread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;

/**
 * @program: SpringBootDemos
 * @description: 同时启动多个线程
 * @author: Kangsen
 * @create: 2022-05-19 15:38
 **/
@Slf4j
public class TestCountDownLatch {
    class Worker implements Runnable{
        CountDownLatch countDownLatch;
        Worker(CountDownLatch countDownLatch){
            this.countDownLatch = countDownLatch;
        }
        @Override
        public void run() {
            try {
                countDownLatch.await();//等待一哈其他线程
                log.info("{} 启动:{}",Thread.currentThread().getName(),System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class Worker1 extends Thread{
        CountDownLatch countDownLatch;
        Worker1(CountDownLatch countDownLatch){
            this.countDownLatch = countDownLatch;
        }
        @Override
        public void run() {
            try {
                countDownLatch.await();//等待一哈其他线程
                log.info("{} 启动:{}",Thread.currentThread().getName(),System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void doTest(){
        final int n = 5;//线程数
        CountDownLatch countDownLatch = new CountDownLatch(n);//
        for (int i = 0; i < n; i++) {
            //new Thread(new Worker1(countDownLatch)).start();
            new Worker1(countDownLatch).start();
            countDownLatch.countDown();
        }
    }

    public static void main(String[] args) {
        TestCountDownLatch testCountDownLatch = new TestCountDownLatch();
        testCountDownLatch.doTest();
    }
}
