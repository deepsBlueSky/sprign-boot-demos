package com.consumer.test.multipleThread;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-19 14:48
 **/

public class Threads {
    public static void main(String[] args) {
        //获取虚拟机可用处理器数
        int coreSize = Runtime.getRuntime().availableProcessors();
        System.out.println("coreSize:"+coreSize);
        //固定长度的线程池
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
        for (int i =0 ; i < 10;i++){
            int index = i;
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("Thread id"+ Thread.currentThread() +"currentTime is :"+System.currentTimeMillis()+"  index is : "+ index);
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
