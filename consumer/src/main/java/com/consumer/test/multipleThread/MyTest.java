package com.consumer.test.multipleThread;

import java.util.concurrent.*;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-19 15:17
 **/

public class MyTest {
    public static void main(String[] args) {


        CountDownLatch countDownLatch = new CountDownLatch(20);
        //ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(3,5,1, TimeUnit.MINUTES,new LinkedBlockingDeque<>(15));
        //ThreadPoolExecutor poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        for (int i = 1; i <= 100; i++) {
            //MyTask myTask = new MyTask("客户"+i,countDownLatch);
            //poolExecutor.submit(myTask);
            new Thread(new MyTask("客户"+i)).start();
            //countDownLatch.countDown();
        }
        //poolExecutor.shutdown();
    }
}
