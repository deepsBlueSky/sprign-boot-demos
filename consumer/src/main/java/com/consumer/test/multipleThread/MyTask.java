package com.consumer.test.multipleThread;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-19 15:06
 **/
@Slf4j
public class MyTask implements Runnable{
    //商品数量
    private static int id = 10;
    //CountDownLatch countDownLatch;
    private static AtomicInteger stock = new AtomicInteger(100);
    //private static int stock = 100;
    //客户名称的变量
    private String username;

    /*public MyTask(String username,CountDownLatch countDownLatch) {*/
    public MyTask(String username) {
        this.username = username;
        //this.countDownLatch = countDownLatch;
    }

    //@SneakyThrows
    @Override
    public void run() {
        //countDownLatch.await();//等待一哈其他线程
        String name = Thread.currentThread().getName();
        //log.info("{} 正在使用 {} 参与秒杀任务",username,name);
        /*try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

//        synchronized (MyTask.class){
            /*if(id > 0){
                log.info("{} 使用 {} 秒杀 {} 商品",username,name,id--);
            }else{
                log.info("{} 使用 {} 秒杀 {} 商品失败",username,name,id);
            }*/


            /*if(stock.get()>0){
                log.info("{} 秒杀{}号库存",username,stock.getAndDecrement());
            }else{
                log.info("{} 秒杀失败",username);
            }*/

            if(stock.get() >= 0){
                log.info("username:{} stock:{}",username,stock.getAndDecrement());
                //stock-=1;
            }
//        }
    }
}
