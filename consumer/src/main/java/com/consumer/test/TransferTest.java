package com.consumer.test;

/**
 * @program: SpringBootDemos
 * @description: 值传递还是引用传递
 * @author: Kangsen
 * @create: 2022-05-18 16:56
 **/
public class TransferTest {

    static class User{
        private String name;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
        public User(String name, Integer age) {
            this.name = name;
            this.age = age;
        }
    }


    public static void main(String[] args) {
        User u = new User("小明",18);
        edit(u);
        System.out.println(u.getName());
    }

    public static void edit(User user){
        System.out.println("===edit===>"+user.getName());
        user.setName("小王");
        System.out.println("===edit===>"+user.getName());
        user = new User("小李",20);
        System.out.println("===edit===>"+user.getName());
    }
}
