package com.ks.mta.apiService;

import com.commonBean.Orders;
import com.ks.api.service.OrderService;
import com.ks.mta.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-12 10:20
 **/
@DubboService(version = "1.0.0")
@Component
@Slf4j
public class ApiOrderService implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public Orders addOrder(int userId,String orderId) {
        Orders order = Orders.builder()
                .createTime(new Date())
                .userId(userId)
                .orderId(orderId)
                .build();
        log.info("插入前  order==>{}",order);
        int insert = orderMapper.insert(order);
        log.info("插入后  order==>{}",order);
        return insert > 0?order:null;
    }

}
