package com.ks.mta;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 10:52
 **/
@SpringBootApplication
@MapperScan("com.ks.mta.mapper.**")
@EnableDiscoveryClient
@EnableDubbo
public class MTApplication2 {
    public static void main(String[] args) {
        SpringApplication.run(MTApplication2.class,args);
    }
}
