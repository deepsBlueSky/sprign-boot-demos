package com.ks.mta.controller;

import com.ks.mta.apiService.ApiStudentService;
import com.ks.mta.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 10:59
 **/
@RestController
@Slf4j
public class IndexController {

    @Resource
    private StudentService studentService;
    @Resource
    private ApiStudentService apiStudentService;

    @GetMapping("/index")
    public Object index(){
        return studentService.studentAll();
    }

    @GetMapping("/index2")
    public Object index2(){
        return apiStudentService.getAllUser();
    }

    @GetMapping("/addStudent")
    public Object addStudent(@RequestParam("name")String name,
                             @RequestParam("cla")String cla){
        log.info("添加student name:{}  cla:{}",name,cla);
        try {
            return apiStudentService.addStudent(name,cla);
        } catch (Exception e) {
            e.printStackTrace();
            return "添加失败";
        }
    }
}
