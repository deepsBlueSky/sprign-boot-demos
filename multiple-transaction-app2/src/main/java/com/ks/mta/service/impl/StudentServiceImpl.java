package com.ks.mta.service.impl;

import com.commonBean.Student;
import com.ks.mta.mapper.StudentMapper;
import com.ks.mta.service.StudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 14:14
 **/
@Service
public class StudentServiceImpl implements StudentService {

    @Resource
    private StudentMapper studentMapper;

    @Override
    public Student getStudent(int id) {
        return studentMapper.selectById(id);
    }

    @Override
    public List<Student> studentAll() {
        return studentMapper.selectByMap(null);
    }

    @Override
    public int insertStudent(Student student) {
        return null == student?0:studentMapper.insert(student);
    }
}
