package com.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * todo:2021年10月27日16:09:59 作为nacos 的生产者微服务
 *
 * @program: SpringBootDemos
 * @description: 生产者
 * @author: Kangsen
 * @create: 2021-08-24 10:04
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class ProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class,args);
    }
}
