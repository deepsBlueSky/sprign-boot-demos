package com.product.controller;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-08-24 10:06
 **/
@RestController
public class IndexController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/send/{msg}")
    public String send(@PathVariable("msg") String msg){
        try {
            //rabbitTemplate.convertAndSend(RabbitMqConfig.TOPIC_EXCHANGE,"ks.1232", msg);
            //sendMessageService.sendMessage(RabbitMqConfig.TOPIC_EXCHANGE,"ks.1232",msg);
        } catch (AmqpException e) {
            e.printStackTrace();
            return "发送失败";
        }
        return "发送成功";
    }

    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
        return "Hello Nacos Discovery " + string;
    }
}
