package com.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：Administrator
 * @Date：2022/8/31/0031 19:22
 * @Versiion：1.0
 */
@RestController
public class TestController {

    @Value("${zk.name}")
    private String zkName;

    @GetMapping("/test")
    public String test(){
        return "testController.test  :  "+ zkName;
    }
}
