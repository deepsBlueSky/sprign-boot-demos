package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:34
 **/

public interface MsgFactory {

    MsgBean getMsg();

}
