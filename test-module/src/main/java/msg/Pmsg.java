package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:36
 **/

public class Pmsg implements MsgBean{
    @Override
    public void sendMsg(String context) {
        System.out.println("推送消息发送");
    }
}
