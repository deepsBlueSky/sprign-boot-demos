package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:39
 **/

public class EmailFactory implements MsgFactory{
    @Override
    public MsgBean getMsg() {
        return new Email();
    }
}
