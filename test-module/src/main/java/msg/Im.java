package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:37
 **/

public class Im implements MsgBean{
    @Override
    public void sendMsg(String context) {
        System.out.println("IM 消息发送");
    }
}
