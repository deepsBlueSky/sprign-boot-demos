package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:35
 **/

public interface MsgBean {

    void sendMsg(String context);

}
