package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:39
 **/

public class Main {
    public static void main(String[] args) {
        MsgFactory msgFactory = new SmsFactory();
        MsgBean msg = msgFactory.getMsg();
        msg.sendMsg("");


        String s1 = new String("3232");
        String s2 = new String("3232");
        System.out.println(s1 == s2);
    }
}
