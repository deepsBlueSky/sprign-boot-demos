package msg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-22 10:38
 **/

public class SmsFactory implements MsgFactory{
    @Override
    public MsgBean getMsg() {
        return new Sms();
    }
}
