package com;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: SpringBootDemos
 * @description: list 方式实现 hashMap 查询的时候效率很低 挨个吧遍历 时间复杂度依赖于长度
 * @author: Kangsen
 * @create: 2022-07-19 15:15
 **/

public class ArrayListHashMap<K,V> {

    private List<Entity> list = new ArrayList<>();

    public void put(K k,V v){
        list.add(new Entity(k,v));
    }

    public V get(K k){
        /*//查询出与参数K相同的元素集合
        Stream<Entity> entityStream = list.stream().filter(entity -> k.equals(entity.k));
        //查询出与参数K相同的元素的集合中的第一个元素
        Optional<Entity> first = list.stream().filter(entity -> k.equals(entity.k)).findFirst();
        //first
        Optional<Object> optionalO = first.map(entity -> entity.v);
        optionalO.orElse(null);*/
        return (V) list.stream().filter(entity -> k.equals(entity.k)).findFirst().map(entity -> entity.v).orElse(null);
    }

    class Entity<K,V>{
        K k;
        V v;
        public Entity(K k,V v){
            this.k = k;
            this.v = v;
        }
    }
}
