package com;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-07 14:50
 **/
@Slf4j
public class Tt {

    @Test
    public void t(){
        LruCache lruCache = new LruCache(5);
        lruCache.put("a","1");
        log.info("lruCache:{}",lruCache);
        lruCache.put("b","2");
        log.info("lruCache:{}",lruCache);
        lruCache.put("c","3");
        log.info("lruCache:{}",lruCache);
        lruCache.put("d","4");
        log.info("lruCache:{}",lruCache);
        lruCache.put("e","5");
        log.info("lruCache:{}",lruCache);
        lruCache.put("f","6");
        log.info("lruCache:{}",lruCache);
        String s = new String();
        System.out.println("23222");

        java.lang.String ss = new java.lang.String();
        System.out.println("111111");


        int[] a = new int[5];
        System.out.println(a[0]);
        Integer[] aa = new Integer[5];
        System.out.println(aa[0]);

    }

    static class Outer{
        static class Inner{}

        public static void foo(){
            new Inner();
        }

        public void bar(){
            new Inner();
        }
    }

    public static void main(String[] args) {
        System.out.println(T.count);

    }
}
class T{
    /**
     * 类加载初始化步骤的时候还是按顺序执行的 所以就是先赋值2 再调用构造
     */
    public static int count = 2;
    public static T t = new T();

    private T() {
        count ++;
    }
}