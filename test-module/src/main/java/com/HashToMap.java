package com;

/**
 * @program: SpringBootDemos
 * @description: 基于hash算法实现hashMap
 * @author: Kangsen
 * @create: 2022-07-19 15:39
 **/

public class HashToMap<K,V> {
    /**
     * 定义一个存储的对象列表
     * */
    private Entity<K, V>[] entry = new Entity[10000];

    public void put(K k,V v){
        //获取一下要存储的位置 也就是hashCode[ hashCode本质就是hash值在hash表中存储的位置]
        int position = k.hashCode() % entry.length;
        entry[position] = new Entity(k,v);
    }

    public V get(K k){
        int position = k.hashCode() % entry.length;
        Entity<K, V> entity = entry[position];
        if (entity == null) {
            return null;
        }else{
            return entity.v;
        }
    }

    /**
     * 存储对象
     * */
    class Entity<K,V>{
        K k;
        V v;
        public Entity(K k,V v){
            this.k = k;
            this.v = v;
        }
    }
}
