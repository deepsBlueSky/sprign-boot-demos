package com.transferType;

/**
 * @Author：kangsen
 * @Date：2022/9/24/0024 9:17
 * @Versiion：1.0
 * @Desc:
 */

public class Main {
    public static void main(String[] args) {
        ShopCar shopCar = new ShopCar();
        Elas elas = new Computer();
        shopCar.add(elas);
        Elas phone = new Phone();
        shopCar.add(phone);
        shopCar.total();
        Computer comp = (Computer) shopCar.get(0);
        comp.web();
        Phone ph = (Phone) shopCar.get(1);
        ph.call();
    }
}
