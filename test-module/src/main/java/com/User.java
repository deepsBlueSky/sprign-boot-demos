package com;

import lombok.Data;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-19 14:36
 **/
@Data
public class User {
    private int age;
    private String name;
}
