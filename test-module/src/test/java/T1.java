import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-02 09:04
 **/

public class T1 {
    public static final DateTimeFormatter F_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
            .withZone(ZoneId.of("Asia/Shanghai")); //GMT+8
    public static final DateTimeFormatter DB_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(ZoneId.of("Asia/Shanghai")); //GMT+8

    @Test
    public void test1(){
        String date = "20220802";
        LocalDateTime localDateTime = LocalDateTime.parse(date + "100000", F_FORMATTER);
        String begin = localDateTime.format(DB_FORMATTER);
        String end = localDateTime.atOffset(ZoneOffset.ofHours(-18)).format(DB_FORMATTER);
        System.out.println("begin : " + begin);
        System.out.println("end : " + end);
    }

    @Test
    public void test2(){
    }


}
