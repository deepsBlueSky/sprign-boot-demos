package utils.common;

import com.alibaba.fastjson.JSON;

import java.util.Base64;
import java.util.List;

/**
 * @program: SpringBootDemos
 * @description: byte工具类
 * @author: Kangsen
 * @create: 2022-05-20 11:24
 **/
public class ByteUtils {
    /**
     * byte数组转对象
     *
     * @param bytes
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T byte2Object(byte[] bytes, Class<T> clazz) {
        T t = null;
        if (null != bytes) {
            try {
                String value = Base64.getEncoder().encodeToString(bytes);
                t = JSON.parseObject(value, clazz);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return t;
    }

    /**
     * byte数组转集合
     *
     * @param bytes
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> byte2Array(byte[] bytes, Class<T> clazz) {
        List<T> list = null;
        if (null != bytes) {
            try {
                String value = Base64.getEncoder().encodeToString(bytes);
                list = JSON.parseArray(value, clazz);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
