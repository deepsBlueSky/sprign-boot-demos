package com.nettyServer.protoHandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-02 16:33
 *
 * 自定义的解码器
 *
 **/
@Slf4j
public class ProtobufDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        //获取一下发送的数据长度
        int length = in.readUnsignedByte();
        //发送的数据长度 大于 可读取的数据长度 说明只读数据是个半包的数据
        if (length > in.readableBytes()) {
            //读到半包数据
            log.error("读取的长度不够 只有半包数据");
            return;
        }
        //正常数据包处理
    }

}
