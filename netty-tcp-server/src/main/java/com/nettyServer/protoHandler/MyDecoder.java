package com.nettyServer.protoHandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-02 14:20
 **/

public class MyDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        byte[] bytes = new byte[in.readableBytes()];
        in.readBytes(bytes);
        String str = new String(bytes);
        out.add(toHexString(bytes));
    }

    public static String toHexString(byte[] bytes){
        StringBuffer stringBuffer = new StringBuffer();
        for (byte aByte : bytes) {
            stringBuffer.append(toHexString(aByte));
        }
        return stringBuffer.toString();
    }

    private static String toHexString(byte b){
        String str = Integer.toHexString(b & 0xFF);
        if (str.length() == 1) {
            return "0" + str.toUpperCase();
        }else{
            return str.toUpperCase();
        }
    }
}
