package com.nettyServer.protoHandler;

import com.nettyServer.handler.Constants;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @program: SpringBootDemos
 * @description: 基于长度拆包器LengthFieldBasedFrameDecoder 开发 自己的长度分割器，用于拆包
 * @author: Kangsen
 * @create: 2022-08-01 16:44
 **/

public class PackageSpliter extends LengthFieldBasedFrameDecoder {
    public PackageSpliter() {
        super(  Constants.MAX_FRAME_LENGTH,
                Constants.LENGTH_FIELD_OFFSET,
                Constants.LENGTH_FIELD_LENGTH,
                Constants.LENGTH_ADJUSTMENT ,
                Constants.INITIAL_BYTES_TO_STRIP);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        return super.decode(ctx, in);
    }
}
