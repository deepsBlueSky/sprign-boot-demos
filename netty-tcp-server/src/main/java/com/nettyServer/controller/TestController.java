package com.nettyServer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-01 15:18
 **/
@RestController
public class TestController {
    @Value("${tcp-server.port}")
    private int tcpPort;

    @GetMapping("/getPort")
    public String test(){
        return String.valueOf(tcpPort);
    }
}
