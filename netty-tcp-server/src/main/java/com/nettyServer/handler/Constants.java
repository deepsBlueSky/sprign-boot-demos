package com.nettyServer.handler;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-01 16:41
 **/

public class Constants {

    /**协议版本号*/
    public static final short PROTOCOL_VERSION = 1;
    /**数据总长度*/
    public static final int MAX_FRAME_LENGTH = 10 * 1024;
    /**长度偏移量*/
    public static final int LENGTH_FIELD_OFFSET = 1;
    /**长度字段长度*/
    public static final int LENGTH_FIELD_LENGTH = 2;
    /**长度的矫正值*/
    public static final int LENGTH_ADJUSTMENT = -2;
    /**丢弃的起始字节数*/
    public static final int INITIAL_BYTES_TO_STRIP = 2;

}
