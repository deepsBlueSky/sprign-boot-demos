package com.nettyServer.listener;

import com.nettyServer.server.StartNetty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * @program: SpringBootDemos
 * @description: netty server 启动监听类
 * @author: Kangsen
 * @create: 2022-08-01 14:13
 *
 * ApplicationListener 或者 commandLineRunner 或者  PostConstruct
 **/
@Configuration
@Slf4j
public class NettyBooter implements CommandLineRunner {

    @Value("${tcp-server.port}")
    private int port;

    @Override
    public void run(String... args) throws Exception {
        StartNetty.getInstance().start(port);
    }
}
