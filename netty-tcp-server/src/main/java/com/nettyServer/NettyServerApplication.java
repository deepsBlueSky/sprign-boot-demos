package com.nettyServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description: netty 实现 tcp  server 端
 * @author: Kangsen
 * @create: 2022-08-01 14:05
 *
 * 自动拆包https://blog.csdn.net/crazymakercircle/article/details/83957259?spm=1001.2014.3001.5502
 *
 *
 * netty启动方式有三种
 * 关于监听器和CommandLineRunner:https://blog.csdn.net/qq_38962655/article/details/112985006
 * 1、使用@PostConstruct注解
 * 2、使用ApplicationListener
 * 3、implements CommandLineRunner
 **/
@SpringBootApplication
public class NettyServerApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(NettyServerApplication.class);
        //监听器 如果不加@Component 或者 @Configuration 就得自己加addListeners new 监听的处理对象
        //springApplication.addListeners(new NettyBooter());
        springApplication.run(args);
    }
}
