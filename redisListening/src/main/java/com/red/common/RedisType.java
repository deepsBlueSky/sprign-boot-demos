package com.red.common;

public enum RedisType {
    STRING("string"),
    LIST("list"),
    SET("set"),
    ZSET("zset"),
    HASH("hash");

    private String des;

    public String getDes(){
        return des;
    }

    RedisType(String des){
        this.des = des;
    }

    public static RedisType getRedisType(String type) {
        for(RedisType redisType : RedisType.values()){
            if(type.equalsIgnoreCase(redisType.getDes())){
                return redisType;
            }
        }
        return null;
    }
}
