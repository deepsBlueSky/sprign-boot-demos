package com.red.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.Version;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @program: SpringBootDemos
 * @description: 实体类基类
 * @author: Kangsen
 * @create: 2021-09-18 11:20
 **/
@Data
@MappedSuperclass
public class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //自动填充创建时间
    @JsonIgnore
    @Column(name = "create_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createTime;

    //自动填充更新时间
    @Column(name = "last_modify_time", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date lastModifyTime;

    //乐观锁
    @Version
    private int version;
}
