package com.red.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @program: SpringBootDemos
 * @description: 商品销售表
 * @author: Kangsen
 * @create: 2021-09-18 12:00
 **/
@Entity
@Setter
@Getter
@ToString
@Table(name = "goodssell")
public class GoodsSell extends BaseEntity{

    @ManyToOne
    private Goods goods;

    @OneToOne
    private Orders order;

    @ManyToOne
    private User user;

    private int count;

    //0:待付款 1:已付款 2:未付款
    private int status;
}
