package com.red.entity;

import lombok.*;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @program: SpringBootDemos
 * @description: 商品表
 * @author: Kangsen
 * @create: 2021-09-18 11:04
 **/
@Entity
@Setter
@Getter
@ToString
@Table(name = "goods")
public class Goods extends BaseEntity{

    //商品名称
    @Column(name = "name")
    private String name;

    //商品价格
    @Column(name = "price")
    private BigDecimal price;
}
