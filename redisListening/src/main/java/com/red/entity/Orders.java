package com.red.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @program: SpringBootDemos
 * @description: 订单
 * @author: Kangsen
 * @create: 2021-09-18 11:52
 **/
@Entity
@Setter
@Getter
@ToString
@Table(name = "orders")
public class Orders extends BaseEntity{
    //支付金额
    private int payAmount;
    //支付状态   0待支付 1支付成功 2支付失败||3过期
    private int status;
    //支付方式： 0 支付宝 1 微信  2 其他
    private int channel;
    //订单编号
    private String tradeNo;
}
