package com.red.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @program: SpringBootDemos
 * @description: 用户表
 * @author: Kangsen
 * @create: 2021-09-18 11:50
 **/
@Entity
@Setter
@Getter
@ToString
@Table(name = "user")
public class User extends BaseEntity{
    private String userName;
    private String password;
    private String tell;
    private String email;
}
