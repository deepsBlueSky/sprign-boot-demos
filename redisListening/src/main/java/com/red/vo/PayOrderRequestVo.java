package com.red.vo;

import lombok.Data;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-09-23 12:00
 **/
@Data
public class PayOrderRequestVo {
    private long goodsId;
    private long userId;
    private int count;
    private int channel;
}
