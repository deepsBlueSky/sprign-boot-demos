package com.red.redisDo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderRedisDo{
    //订单id
    private Long id;
    //支付金额
    private String payAmount;
    //支付方式
    private Integer channel;
    //订单状态
    private Integer status;
    //商品id
    private Long goodsId;
    //订单号
    private String tradeNo;

    public OrderRedisDo() {

    }
}
