package com.red;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
/*************************************************************
本模块是测试redis 事件监听
用户提交订单30分钟未支付时自动取消订单;
如果30分钟内支付了订单就在订单通知的回调中去处理订单状态
数据表：商品表、用户表、商品销售表、订单表
*************************************************************/
/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-08-27 10:57
 **/
@SpringBootApplication
public class RedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class,args);
    }
}
