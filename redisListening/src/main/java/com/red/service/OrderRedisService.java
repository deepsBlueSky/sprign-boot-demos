package com.red.service;

import com.red.redisDo.OrderRedisDo;

/**
 * redis处理订单
 */
public interface OrderRedisService {

    /**
     * redis 保存订单
     * @param outTradeNo
     * @param redisDo
     */
    void saveOrder(String outTradeNo, OrderRedisDo redisDo);

    /**
     * 获取redis保存的订单信息
     * @param outTradeNo
     * @return
     */
    String getOrder(String outTradeNo);

    /**
     * 删除redis中的订单信息
     * @param outTradeNo
     */
    void deleteOrder(String outTradeNo);
}
