package com.red.service;

import com.red.vo.PayOrderRequestVo;

public interface OrderService {
    public String payOrder(PayOrderRequestVo requestVo);
}
