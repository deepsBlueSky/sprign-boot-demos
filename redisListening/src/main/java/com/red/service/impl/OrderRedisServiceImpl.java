package com.red.service.impl;

import com.red.caches.IGlobalCache;
import com.red.common.JsonUtil;
import com.red.redisDo.OrderRedisDo;
import com.red.service.OrderRedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-09-18 13:58
 **/
@Service
@Slf4j
public class OrderRedisServiceImpl implements OrderRedisService {

    @Autowired
    private IGlobalCache globalCache;

    /**
     * redis 保存订单
     *
     * @param outTradeNo
     * @param redisDo
     */
    @Override
    public void saveOrder(String outTradeNo, OrderRedisDo redisDo) {
        log.info("保存缓存：{}","order_"+outTradeNo);
        globalCache.set("order_"+outTradeNo,redisDo,2*60);
    }

    /**
     * 获取redis保存的订单信息
     *
     * @param outTradeNo
     * @return
     */
    @Override
    public String getOrder(String outTradeNo) {
        log.info("获取缓存：{}","order_"+outTradeNo);
        return JsonUtil.objectToJson(globalCache.get("order_"+outTradeNo));
    }

    /**
     * 删除redis中的订单信息
     *
     * @param outTradeNo
     */
    @Override
    public void deleteOrder(String outTradeNo) {
        log.info("删除缓存：{}","order_"+outTradeNo);
        globalCache.del("order_"+outTradeNo);
    }
}
