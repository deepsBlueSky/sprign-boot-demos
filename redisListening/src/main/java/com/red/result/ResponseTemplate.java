package com.red.result;

import lombok.Data;

import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-09-23 11:58
 **/
@Data
public class ResponseTemplate {
    private int code;
    private String message;
    private List<String> errors;
    private Object data;
    public ResponseTemplate(Object data) {
        super();
        this.data = data;
    }
    public ResponseTemplate() {
        super();
    }
}
