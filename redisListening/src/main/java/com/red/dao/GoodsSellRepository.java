package com.red.dao;

import com.red.entity.Goods;
import com.red.entity.GoodsSell;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsSellRepository extends CrudRepository<GoodsSell,Long> {

    //根据商品id修改商品状态
    @Modifying
    @Query("UPDATE GoodsSell o set o.status = ?1 where o.id = ?2")
    int setStatusByGoodsId(int status, long goodsId);

    //通过订单号查询商品销售信息
    //nativeQuery=true) 表示用原生的sql
    @Query(value = "select o from GoodsSell o where o.order_id = ?1",nativeQuery = true)
    GoodsSell findOrderByOrderId(long orderId);
}
