package com.red.dao;

import com.red.entity.Orders;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdersRepository extends CrudRepository<Orders,Long> {

    @Query("select o from Orders o WHERE o.tradeNo = ?1")
    Orders findOrderByTradeNo(String tradeNo);

    @Modifying
    @Query("UPDATE Orders o set o.status = ?1 where o.id = ?2")
    int setStatusByOrderId(int status,long orderId);
}
