package com.commonBean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-12 14:04
 **/
@Data
@Builder
public class Orders implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String orderId;
    private Integer userId;
    private Date createTime;
    private String remark;
}
