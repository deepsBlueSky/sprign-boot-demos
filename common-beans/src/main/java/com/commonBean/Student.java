package com.commonBean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 14:07
 **/
@Data
@Builder
public class Student implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer age;
    private String name;
    private String cla;
    //订单数
    private int count;
}
