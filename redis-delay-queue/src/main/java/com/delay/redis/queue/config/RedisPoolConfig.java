package com.delay.redis.queue.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * @program: SpringBootDemos
 * @description: Redis配置类
 * @author: Kangsen
 * @create: 2022-07-08 11:00
 **/
@Configuration
@EnableConfigurationProperties(RedisPoolProperties.class)
public class RedisPoolConfig {

    @Resource
    private RedisPoolProperties redisPoolProperties;

    /**
     * 初始化一下Jedis的链接池
     * @return
     */
    @Bean
    public JedisPoolConfig initJedisPool(){
        try {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(redisPoolProperties.getMaxTotal());
            jedisPoolConfig.setMaxIdle(redisPoolProperties.getMaxIdle());
            jedisPoolConfig.setMinIdle(redisPoolProperties.getMinIdle());
            jedisPoolConfig.setMaxWaitMillis(redisPoolProperties.getMaxWaitMillis());
            jedisPoolConfig.setNumTestsPerEvictionRun(redisPoolProperties.getNumTestPerEvictionRun());
            jedisPoolConfig.setTimeBetweenEvictionRunsMillis(redisPoolProperties.getTimeBetweenEvictionRunsMillis());
            jedisPoolConfig.setMinEvictableIdleTimeMillis(redisPoolProperties.getMinEvictableIdleTimeMillis());
            jedisPoolConfig.setSoftMinEvictableIdleTimeMillis(redisPoolProperties.getSoftMinEvictableIdleTimeMillis());
            jedisPoolConfig.setTestOnBorrow(redisPoolProperties.isTestOnBorrow());
            jedisPoolConfig.setTestOnReturn(redisPoolProperties.isTestOnReturn());
            jedisPoolConfig.setTestWhileIdle(redisPoolProperties.isTestWhileIdle());
            jedisPoolConfig.setBlockWhenExhausted(redisPoolProperties.isBlockWhenExhausted());
            jedisPoolConfig.setJmxEnabled(redisPoolProperties.isJmxEnable());
            jedisPoolConfig.setLifo(redisPoolProperties.isLifo());
            jedisPoolConfig.setNumTestsPerEvictionRun(redisPoolProperties.getNumTestPerEvictionRun());
            return jedisPoolConfig;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Set<HostAndPort> getSet(){
        String hostAndPortStr = redisPoolProperties.getHostAndPort();
        String[] hostAndPorts = hostAndPortStr.trim().split(",");
        HashSet<HostAndPort> stringHashSet = new HashSet<>();
        for (String andPort : hostAndPorts) {
            stringHashSet.add(new HostAndPort(String.valueOf(andPort.split(":")[0]),
                    Integer.parseInt(andPort.split(":")[1])));
        }
        return stringHashSet;
    }

    @Bean
    @Qualifier("JedisCluster")
    public JedisCluster getJedisCluster(){
        //return new JedisCluster(getSet(), initJedisPool());
        return new JedisCluster(getSet(), 15,5,20,"Mall_3804.vcsa",initJedisPool());
    }

    /**
     * 单机版 需要手动获取实例 完事 手动释放
     * @return 返回redis连接池
     */
    @Bean
    @Qualifier("JedisPool")
    public JedisPool getJedisPool(){
        return new JedisPool(initJedisPool(), "127.0.0.1" , 6379 , 15 );
    }
}
