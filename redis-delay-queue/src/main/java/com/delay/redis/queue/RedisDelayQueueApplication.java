package com.delay.redis.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description: redis 实现的消息延迟队列
 * 生产上不一定适用 作为学习
 * 一、集成redis有两种方式
 * (1)、集成redis官方推荐的插件 jedis客户端
 * (2)、集成SpringBoot自带的redisTemplate;
 * 二、
 * [集成jedis]: https://blog.csdn.net/weixin_45239670/article/details/113185684
 * (1)、引入jedis的依赖
 * (2)、写配置项及配置类
 *
 * 1、redis key过期监控
 * 2、使用redis过期监听实现延迟队列
 * 3、redis 的zset实现延迟队列[相对来说redis 实现较好的方式]
 * @author: Kangsen
 * @create: 2022-07-08 10:41
 **/
@SpringBootApplication
public class RedisDelayQueueApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisDelayQueueApplication.class,args);
    }
}
