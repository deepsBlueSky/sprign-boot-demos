package com.delay.redis.queue.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-11 13:42
 **/
@RestController
@Slf4j
@RequestMapping("/provider")
public class ProviderController {

    @Resource
    private JedisPool jedisPool;

    @RequestMapping(value = "/submitOrder",method = RequestMethod.GET)
    public String submitOrder(){
        log.info("submitOrder ");
        Jedis jedis = jedisPool.getResource();
        String orderNo = "order-" + UUID.randomUUID().toString();
        long currentTimeMillis = System.currentTimeMillis();
        jedis.zadd("order" , currentTimeMillis , orderNo);
        return LocalDateTime.now().toString();
    }
}
