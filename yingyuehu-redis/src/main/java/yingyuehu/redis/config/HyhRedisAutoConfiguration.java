package yingyuehu.redis.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import yingyuehu.redis.helper.RedisDistributionLockHelper;
import yingyuehu.redis.helper.RedisHelper;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 12:05
 **/
@Slf4j
@ConditionalOnWebApplication
@Configuration(proxyBeanMethods = false)
@ComponentScan("yingyuehu.redis.config")
public class HyhRedisAutoConfiguration {
    @Bean
    public RedisHelper redisHelper(){
        return new RedisHelper();
    }

    @Bean
    public RedisDistributionLockHelper redisDistributionLockHelper(){
        return new RedisDistributionLockHelper();
    }
}
