package yingyuehu.redis.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import utils.common.ByteUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 11:57
 **/
@Component
@Slf4j
public class RedisHelper {

    @Resource
    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init(){
        log.info("---redisHelper 初始化---");
    }
    /**
     * 向redis hash几何中存放一个元素
     *
     * @param key
     * @param hashKey
     * @param value
     */
    public void set(byte[] key, byte[] hashKey, byte[] value) {
        try {
            redisTemplate.opsForHash().put(key, hashKey, value);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis set error", e);
        }
    }

    /**
     * Redis value set
     *
     * @param key
     * @param value
     */
    public void set(Object key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis set error", e);
        }
    }

    /**
     * Redis value set
     *
     * @param key
     * @param value
     * @param expireTime
     */
    public void set(Object key, Object value, long expireTime) {
        try {
            redisTemplate.opsForValue().set(key, value, expireTime);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis set error", e);
        }
    }

    /**
     * Redis value set
     *
     * @param key
     * @param value
     * @param expireTime
     * @param timeUnit
     */
    public void set(Object key, Object value, long expireTime, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForValue().set(key, value, expireTime, timeUnit);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis set error", e);
        }
    }

    /**
     * Redis increment
     *
     * @param key
     */
    public Long increment(Object key) {
        try {
            return redisTemplate.opsForValue().increment(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis increment error", e);
        }
        return null;

    }

    /**
     * Redis increment
     *
     * @param key
     */
    public Long decrement(Object key) {
        try {
            return redisTemplate.opsForValue().decrement(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis decrement error", e);
        }
        return null;

    }

    /**
     * Redis increment
     *
     * @param key
     * @param step
     */
    public Long increment(String key, long step) {
        try {
            return redisTemplate.opsForValue().increment(key, step);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis increment error", e);
        }
        return null;

    }

    /**
     * Redis decrement
     *
     * @param key
     * @param step
     */
    public Long decrement(String key, long step) {
        try {
            return redisTemplate.opsForValue().decrement(key, step);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis decrement error", e);
        }
        return null;
    }

    /**
     * 获取Redis hash几何中的一个值
     *
     * @param key
     * @param hashKey
     * @return
     */
    public byte[] get(byte[] key, byte[] hashKey) {
        byte[] b = null;
        try {
            b = (byte[]) redisTemplate.opsForHash().get(key, hashKey);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis get value error", e);
        }
        return b;
    }

    /**
     * Redis get value
     *
     * @param key
     * @return
     */
    public String getStringValue(String key) {
        String value = null;
        try {
            value = (String) redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis get value error", e);
        }
        return value;
    }

    public Integer getIntergerValue(String key) {
        Integer value = null;
        try {
            value = (Integer) redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis get value error", e);
        }
        return value;
    }

    public Object getObjectValue(Object key) {
        Object value = null;
        try {
            value = redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("redis get value error", e);
        }
        return value;
    }

    /**
     * 获取Redis hash几何中的一个对象
     *
     * @param key
     * @param hashKey
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T get(byte[] key, byte[] hashKey, Class<T> clazz) {
        byte[] values = get(key, hashKey);
        if (values != null) {
            return ByteUtils.byte2Object(values, clazz);
        }
        return null;
    }

    /**
     * 获取Redis hash几何中的集合
     *
     * @param key
     * @param hashKey
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> List<T> getArray(byte[] key, byte[] hashKey, Class<T> clazz) {
        byte[] values = get(key, hashKey);
        if (values != null) {
            return ByteUtils.byte2Array(values, clazz);
        }
        return null;
    }

    /**
     * 删除Redis hash几何中的一个或多个值
     *
     * @param key
     * @param hashKeys
     * @return
     */
    public Boolean del(byte[] key, Object... hashKeys) {
        Boolean result = Boolean.FALSE;
        try {
            result = redisTemplate.opsForHash().delete(key, hashKeys) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("从redis中删除键值失败", e);
        }
        return result;
    }


    /**
     * 删除Redis key
     *
     * @param key
     * @return
     */
    public Boolean del(String key) {
        Boolean result = Boolean.FALSE;
        try {
            result = redisTemplate.delete(key);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("从redis中删除键值失败", e);
        }
        return result;
    }

    /**
     * 是否存在
     *
     * @param key
     * @param hashKey
     * @return
     */
    public Boolean hasKey(byte[] key, byte[] hashKey) {
        Boolean result = Boolean.FALSE;
        try {
            result = redisTemplate.opsForHash().hasKey(key, hashKey);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("从redis中获取键值失败", e);
        }
        return result;
    }

    @PreDestroy
    public void destroy() {
        log.info("---Redis助手注销---");
    }
}
