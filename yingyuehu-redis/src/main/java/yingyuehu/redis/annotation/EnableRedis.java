package yingyuehu.redis.annotation;

import org.springframework.context.annotation.Import;
import yingyuehu.redis.config.HyhRedisAutoConfiguration;

import java.lang.annotation.*;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 12:10
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({HyhRedisAutoConfiguration.class})
public @interface EnableRedis {
}
