package com.ks.demo.controller;

import com.google.common.collect.Maps;
import com.ks.demo.config.CommonKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.UUID;

/**
 * @Author：kangsen
 * @Date：2022/9/26/0026 23:20
 * @Versiion：1.0
 * @Desc:
 *
 * 推送消息
 *
 */
@RestController
@Slf4j
public class PushMessageController {

    @Resource
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/pushDirect/{msg}")
    public String pushDirect(@PathVariable("msg") String msg){
        try {
            HashMap<String, String> msgInfo = Maps.newHashMap();
            msgInfo.put("mid", UUID.randomUUID().toString());
            msgInfo.put("msg", msg);
            msgInfo.put("type", "direct");
            msgInfo.put("createTime", LocalDateTime.now().toString());
            log.info("send 消息:{}",msgInfo);
            rabbitTemplate.convertAndSend(CommonKey.DIRECT_EXCHANGE_NAME,CommonKey.DIRECT_ROUTING_KEY,msgInfo);
        } catch (AmqpException e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
        return "success";
    }

    @GetMapping("/pushTopic/{msg}")
    public String pushTopic(@PathVariable("msg") String msg){
        try {
            HashMap<String, String> msgInfo = Maps.newHashMap();
            msgInfo.put("mid", UUID.randomUUID().toString());
            msgInfo.put("msg", msg);
            msgInfo.put("type", "topic");
            msgInfo.put("createTime", LocalDateTime.now().toString());
            log.info("send 消息:{}",msgInfo);
            rabbitTemplate.convertAndSend(CommonKey.TOPIC_EXCHANGE_NAME,CommonKey.TOPIC_ROUTING_KEY,msgInfo);
        } catch (AmqpException e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
        return "success";
    }

    @GetMapping("/pushTopicPrefix/{msg}")
    public String pushTopicPrefix(@PathVariable("msg") String msg){
        try {
            HashMap<String, String> msgInfo = Maps.newHashMap();
            msgInfo.put("mid", UUID.randomUUID().toString());
            msgInfo.put("msg", msg);
            msgInfo.put("type", "topic-prefix");
            msgInfo.put("createTime", LocalDateTime.now().toString());
            log.info("send 消息:{}",msgInfo);
            rabbitTemplate.convertAndSend(CommonKey.TOPIC_EXCHANGE_NAME,CommonKey.TOPIC_ROUTING_KEY_PREFIX + ".prefix",msgInfo);
        } catch (AmqpException e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
        return "success";
    }

    @GetMapping("/pushFanout/{msg}")
    public String pushFanout(@PathVariable("msg") String msg){
        try {
            HashMap<String, String> msgInfo = Maps.newHashMap();
            msgInfo.put("mid", UUID.randomUUID().toString());
            msgInfo.put("msg", msg);
            msgInfo.put("type", "fanout");
            msgInfo.put("createTime", LocalDateTime.now().toString());
            log.info("send 消息:{}",msgInfo);
            rabbitTemplate.convertAndSend("FanoutExchange",null ,msgInfo);
        } catch (AmqpException e) {
            e.printStackTrace();
            return "" + e.getMessage();
        }
        return "success";
    }

    @GetMapping("/submit")
    public String submitOrder(){
        String orderId = UUID.randomUUID().toString();
        rabbitTemplate.convertAndSend("normalExchange","normal","订单：" + orderId);
        return orderId;
    }

}
