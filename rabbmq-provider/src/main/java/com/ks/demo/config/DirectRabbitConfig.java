package com.ks.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/26/0026 22:13
 * @Versiion：1.0
 * @Desc:
 */
@Configuration
public class DirectRabbitConfig {

    /**
     * 定义直连模式队列
     * @return
     */
    @Bean
    public Queue testDirectQueue(){
        /**
         * @param name 队列的名称 - 不能为空；设置为 "" 让代理生成名称。
         * @param durable 如果我们声明一个持久队列，则为 true（该队列将在服务器重新启动后继续存在）
         * @param exclusive 如果我们声明一个独占队列，则为 true（该队列将仅由声明者的连接使用）
         * @param autoDelete 如果服务器在不再使用队列时应该删除队列，则为 true
         * @param arguments 用于声明队列的参数
         */
        //Queue(String name, boolean durable, boolean exclusive, boolean autoDelete,@Nullable Map<String, Object> arguments)

        Map<String,Object> map = new HashMap<>();
        //message在队列queue的最大存活时间为5s
        map.put("x-message-ttl", 5000);
        //设置死信交换机
        map.put("x-dead-letter-exchange","deadExchange");
        //设置死信交换机的路由键
        map.put("x-dead-letter-routing-key", "dead");

        return new Queue(CommonKey.DIRECT_QUEUE_NAME,true,false,false,map);
    }

    /**
     * 定义直连交换机
     * @return
     */
    @Bean
    public DirectExchange directExchange(){
        /**
         * 交换机名称
         * 持久化
         * 不自动删除
         */
        return new DirectExchange(CommonKey.DIRECT_EXCHANGE_NAME,true,false);
    }

    /**
     * 把队列和交换机绑定起来  路由键:direct-routing-key
     * @return
     */
    @Bean
    public Binding bindingDirect(){
       return BindingBuilder.bind(testDirectQueue())
               .to(directExchange())
               .with(CommonKey.DIRECT_ROUTING_KEY);
    }
}
