package com.ks.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 20:16
 * @Versiion：1.0
 * @Desc:
 * 扇形交换机 配置
 */
@Configuration
public class FanoutRabbitConfig {

    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("FanoutExchange");
    }

    @Bean
    public Queue queueA(){
        return new Queue("Fanout.A");
    }

    @Bean
    public Binding bindingA(){
        return BindingBuilder.bind(queueA()).to(fanoutExchange());
    }

    @Bean
    public Queue queueB(){
        return new Queue("Fanout.B");
    }

    @Bean
    public Binding bindingB(){
        return BindingBuilder.bind(queueB()).to(fanoutExchange());
    }

    @Bean
    public Queue queueC(){
        return new Queue("Fanout.C");
    }

    @Bean
    public Binding bindingC(){
        return BindingBuilder.bind(queueC()).to(fanoutExchange());
    }
}
