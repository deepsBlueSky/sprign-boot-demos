package com.ks.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 20:20
 * @Versiion：1.0
 * @Desc:
 */
@Configuration
public class TopicRabbitConfig {

    @Bean
    public Queue topicQueue(){
        return new Queue(CommonKey.TOPIC_QUEUE_NAME);
    }

    @Bean
    public Queue topicPrefixQueue(){
        Map<String,Object> args = new HashMap<>();
        args.put("x-max-priority", 100);
        //
        return new Queue(CommonKey.TOPIC_QUEUE_NAME_PREFIX);
    }

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(CommonKey.TOPIC_EXCHANGE_NAME);
    }

    /**
     * 完全匹配的路由键
     * @return
     */
    @Bean
    public Binding bindingExchange(){
        return BindingBuilder.bind(topicQueue()).to(topicExchange()).with(CommonKey.TOPIC_ROUTING_KEY);
    }

    /**
     * 前缀匹配的路由键
     * @return
     */
    @Bean
    public Binding bindingExchangePrefix(){
        return BindingBuilder.bind(topicPrefixQueue()).to(topicExchange()).with(CommonKey.TOPIC_ROUTING_KEY_PREFIX + ".#");
    }
}
