package com.ks.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author：kangsen
 * @Date：2022/9/26/0026 22:12
 * @Versiion：1.0
 * @Desc:
 * rabbitMq的demo:
 *
 * 生产者  -》   交换机  --》 【路由键】并不是所有的绑定都需要路由键  -》   队列   -》 消费者
 * 交换机有4种类型：
 *
 *  1、direct 直连交换机  需要和直连队列 通过 Routing Key 绑定
 *
 *  2、fanout 扇形交换机  【没有路由键】 直接转发到绑定的队列
 *
 *  3、Topic Exchange 主题交换机 路由键有匹配规则  *  # 匹配规则，
 *
 *  4、Header 头交换机
 *
 *  5、Default 默认交换机
 *
 *  6、Dead Letter  死信交换机
 *
 */
@SpringBootApplication
public class ProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class,args);
    }
}
