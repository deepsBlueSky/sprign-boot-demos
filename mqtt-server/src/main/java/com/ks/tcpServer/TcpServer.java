package com.ks.tcpServer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

/**
 * @program: SpringBootDemos
 * @description: Netty实现TCP服务端
 * @author: Kangsen
 * @create: 2022-06-24 15:56
 **/

public class TcpServer {

    public void start(){
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup(20);
        ServerBootstrap bootstrap = new ServerBootstrap();

    }
}
