package com.ks.domain;

import io.netty.handler.codec.mqtt.MqttQoS;
import lombok.Data;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-27 13:52
 **/
@Data
public class MqttRequest {
    private boolean mutable = false;
    private byte[] payload;
    private MqttQoS mqttQoS = MqttQoS.AT_MOST_ONCE;
    private boolean retained = false;
    private boolean dup = false;
    private int messageId;
}
