package com.ks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-23 14:11
 **/
@SpringBootApplication
public class NettyMqttApplication {
    public static void main(String[] args) {
        SpringApplication.run(NettyMqttApplication.class,args);
        //启动Netty MQtt Server
    }
}
