package com.ks.mqttServer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-23 15:06
 **/
@Slf4j
@Component
public class MqServerInit{

    //@PostConstruct
    public void init(){
        MqttServer mqttServer = new MqttServer();
        //mqttServer.startServer(new InetSocketAddress("127.0.0.1",10088));
        mqttServer.startServer(10088);
    }

    interface PayMethod{
        void pay();
    }

    static class AliPay implements PayMethod{
        @Override
        public void pay() {
            System.out.println("阿里支付");
        }
    }

    class WechatPay implements PayMethod{
        @Override
        public void pay() {
            System.out.println("微信支付");
        }
    }

    interface PayFactory{
        PayMethod getMethod();
    }

    static class AliFactory implements PayFactory{

        @Override
        public PayMethod getMethod() {
            return new AliPay();
        }
    }

    class WechatFactory implements PayFactory{

        @Override
        public PayMethod getMethod() {
            return new WechatPay();
        }
    }

    public static void main(String[] args) {
        PayFactory payFactory = new AliFactory();
        payFactory.getMethod().pay();
    }
}
