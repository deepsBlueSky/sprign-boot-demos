package com.level.contoller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-08 09:11
 **/
@RestController
@Slf4j
public class TexstController {

    @GetMapping("/time")
    public Object test(){
        log.debug("debug 获取当前时间:{}",System.currentTimeMillis());
        log.info("info 获取当前时间:{}",System.currentTimeMillis());
        log.warn("warn 获取当前时间:{}",System.currentTimeMillis());
        log.error("error 获取当前时间:{}",System.currentTimeMillis());
        return LocalDateTime.now();
    }

}
