package com.level;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-08 09:10
 **/
@SpringBootApplication
public class DynamicLogLevelApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicLogLevelApplication.class,args);
    }
}
