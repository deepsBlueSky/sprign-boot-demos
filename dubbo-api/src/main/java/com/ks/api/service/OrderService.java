package com.ks.api.service;

import com.commonBean.Orders;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-12 09:46
 **/

public interface OrderService {

    /**
     * 添加订单
     * @param userId
     * @param orderId
     * @return
     */
    Orders addOrder(int userId,String orderId);

}
