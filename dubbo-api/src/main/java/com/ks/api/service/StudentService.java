package com.ks.api.service;

import com.commonBean.Student;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-12 09:46
 **/

public interface StudentService {

    /**
     * 获取所有的Student
     * @return
     */
    String getAllUser();

    /**
     * 添加学生
     * @param name
     * @param cla
     * @return
     */
    Student addStudent(String name, String cla);
}
