package ftpClitentPool.core;

import ftpClitentPool.config.FtpPoolConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-09-24 11:26
 **/
@Slf4j
@Service
public class FtpClientFactory extends BasePooledObjectFactory<FTPClient> {

    @Autowired
    private FtpPoolConfig ftpPoolConfig;

    public FtpPoolConfig getFtpPoolConfig(){
        return ftpPoolConfig;
    }

    /**
     * 创建ftpClient对象
     * @return
     * @throws Exception
     */
    @Override
    public FTPClient create(){
        try {
            FTPClient ftpClient = new FTPClient();
            ftpClient.setConnectTimeout(ftpClient.getConnectTimeout());
            log.info("连接ftp服务器：" + ftpPoolConfig.getHost() + " : " + ftpPoolConfig.getPort());
            ftpClient.connect(ftpPoolConfig.getHost(),ftpPoolConfig.getPort());
            int replyCode = ftpClient.getReplyCode();
            if(!FTPReply.isPositiveCompletion(replyCode)){
                ftpClient.disconnect();
                log.error("ftp拒绝连接");
                return null;
            }
            boolean login = ftpClient.login(ftpPoolConfig.getUsername(), ftpPoolConfig.getPassword());
            if(!login){
                log.error("ftp登录失败");
                throw new Exception("ftp 登录失败！username:"+ftpPoolConfig.getUsername()+" 密码："+ftpPoolConfig.getPassword());
            }
            ftpClient.setControlEncoding(ftpPoolConfig.getControlEncoding());
            ftpClient.setBufferSize(ftpPoolConfig.getBufferSize());
            ftpClient.setFileType(ftpPoolConfig.getFileType());
            ftpClient.setDataTimeout(ftpPoolConfig.getDataTimeout());
            ftpClient.setUseEPSVwithIPv4(ftpPoolConfig.isUseEPSVwithIPv4());
            if(ftpPoolConfig.isPassiveMode()){//是否是被动模式
                ftpClient.enterLocalPassiveMode();//设置为被动模式
            }
            return ftpClient;
        } catch (Exception e) {
            //e.printStackTrace();
            log.error("FTPClient 创建失败",e);
            return null;
        }
    }

    @Override
    public PooledObject<FTPClient> wrap(FTPClient ftpClient) {
        return new DefaultPooledObject<FTPClient>(ftpClient);
    }

    @Override
    public void destroyObject(PooledObject<FTPClient> p) throws Exception {
        try {
            FTPClient ftpClient = p.getObject();
            ftpClient.logout();
        } catch (IOException e) {
            //e.printStackTrace();
            log.error("销毁ftpclient 对象失败",e);
        }
        super.destroyObject(p);
    }

    @Override
    public boolean validateObject(PooledObject<FTPClient> p) {
        try {
            FTPClient ftpClient = p.getObject();
            boolean conn = false;
            conn = ftpClient.sendNoOp();
        } catch (IOException e) {
            //e.printStackTrace();
            log.error("ftpclient 对象验证连接失败",e);
        }
        return super.validateObject(p);
    }
}
