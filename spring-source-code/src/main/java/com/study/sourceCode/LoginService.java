package com.study.sourceCode;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-14 10:06
 **/
public class LoginService {

    public String login(String username){
        System.out.println("用户 " + username + "登录");
        return "login success";
    }
}
