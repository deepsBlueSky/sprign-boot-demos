package com.study.sourceCode;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-14 10:08
 **/
//@RunWith(SpringRunner.class)
public class TestCase {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        LoginService loginService = applicationContext.getBean("loginService",LoginService.class);
        String login = loginService.login("小明");
        System.out.println("login result:" + login);
    }
    /*@Test
    public void t1(){

    }*/
}
