package com.ks.demo.listener.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 22:44
 * @Versiion：1.0
 * @Desc:
 *
 * 多个消费者节点监听    节点间会轮询，且节点不会重复消费
 *
 */

@Slf4j
@Component
@RabbitListener(queues = "Fanout.A")
public class FanoutAListener{

    @RabbitHandler
    public void receive(Map receiveMsg){
        log.info("receive fanout A: {}", receiveMsg);
    }
}
