package com.ks.demo.listener;

import com.ks.demo.config.CommonKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 20:36
 * @Versiion：1.0
 * @Desc:
 */
@Component
@RabbitListener(queues = CommonKey.TOPIC_QUEUE_NAME_PREFIX)
@Slf4j
public class TopicPrefixListener {

    @RabbitHandler
    public void receiver(Map msg){
        log.info("topic prefix receive : {}",msg);
    }
}
