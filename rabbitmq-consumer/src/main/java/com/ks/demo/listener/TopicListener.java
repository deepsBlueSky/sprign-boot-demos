package com.ks.demo.listener;

import com.ks.demo.config.CommonKey;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 20:36
 * @Versiion：1.0
 * @Desc: 多个消费者节点 会轮询消费  不会重复消费
 */
@Component
@RabbitListener(queues = CommonKey.TOPIC_QUEUE_NAME)
@Slf4j
public class TopicListener {

    @RabbitHandler
    public void receiver(Map msg, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag){
        log.info("topic receive : {}",msg);
        try {
            channel.basicAck(deliveryTag,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
