package com.ks.demo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;

/**
 * @Author：kangsen
 * @Date：2022/9/29/0029 22:11
 * @Versiion：1.0
 * @Desc:
 */
/*@Component
@RabbitListener(queues = "normalQueue")*/
@Slf4j
public class NormalListener {

    @RabbitHandler
    public void receive(String msg){
        log.info("normalQueue receive : {}",msg);
    }

}
