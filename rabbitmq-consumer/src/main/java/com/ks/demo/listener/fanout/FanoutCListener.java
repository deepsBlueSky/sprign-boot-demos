package com.ks.demo.listener.fanout;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author：kangsen
 * @Date：2022/9/27/0027 22:44
 * @Versiion：1.0
 * @Desc:
 */
@Slf4j
@Component
@RabbitListener(queues = "Fanout.C")
public class FanoutCListener{

    @RabbitHandler
    public void receive(Map receiveMsg){
        log.info("receive  fanout C : {}", receiveMsg);
    }
}
