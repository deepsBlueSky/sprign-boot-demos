package com.ks.demo.config;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：kangsen
 * @Date：2022/10/5/0005 21:42
 * @Versiion：1.0
 * @Desc:
 */
@Configuration
public class DeadRecoverer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Bean
    public MessageRecoverer messageRecoverer(){
        return new RepublishMessageRecoverer(rabbitTemplate,"deadExchange","deadQueue");
    }
}
