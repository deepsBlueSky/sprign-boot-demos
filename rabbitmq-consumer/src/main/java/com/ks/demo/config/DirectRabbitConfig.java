package com.ks.demo.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：kangsen
 * @Date：2022/9/26/0026 23:31
 * @Versiion：1.0
 * @Desc:
 *
 * 作为消息者 这里配置后 可以作为消息的发送者
 */
@Configuration
public class DirectRabbitConfig {

    @Bean
    public Queue directQueue(){
        /**
         * 创建直连队列
         * 持久化的队列
         */
        return new Queue(CommonKey.DIRECT_QUEUE_NAME, true);
    }

    @Bean
    public DirectExchange directExchange(){
        /**
         * 创建直连交换机
         */
        return new DirectExchange(CommonKey.DIRECT_EXCHANGE_NAME,true,false);
    }

    /**
     * 绑定直连交换机和直连队列
     * @return
     */
    @Bean
    public Binding directBinding(){
        return BindingBuilder.bind(directQueue())
            .to(directExchange())
            .with(CommonKey.DIRECT_ROUTING_KEY);
    }

}
