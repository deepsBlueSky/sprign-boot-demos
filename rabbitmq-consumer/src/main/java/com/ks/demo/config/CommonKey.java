package com.ks.demo.config;

/**
 * @Author：kangsen
 * @Date：2022/9/26/0026 23:49
 * @Versiion：1.0
 * @Desc:
 */

public class CommonKey {
    //直连队列名
    public static final String DIRECT_QUEUE_NAME = "test-direct-queue-name";
    //直连交换机名
    public static final String DIRECT_EXCHANGE_NAME = "test-direct-exchange-name";
    //直连路由键
    public static final String DIRECT_ROUTING_KEY = "test-direct-routing-key";


    //主题队列名
    public static final String TOPIC_QUEUE_NAME = "test-topic-queue-name";
    public static final String TOPIC_QUEUE_NAME_PREFIX = "test-topic-queue-name-prefix";
}
