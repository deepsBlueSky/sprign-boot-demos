package com.securityMod.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：kangsen
 * @Date：2022/10/25/0025 20:44
 * @Versiion：1.0
 * @Desc:
 */
@RestController
public class TestController {

    @GetMapping("/index")
    public String index(){
        return "hello security";
    }

}
