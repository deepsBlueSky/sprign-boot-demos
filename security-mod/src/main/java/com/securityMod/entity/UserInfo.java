package com.securityMod.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @Author：kangsen
 * @Date：2022/10/25/0025 21:38
 * @Versiion：1.0
 * @Desc:
 */
@Data
@Builder
public class UserInfo {
    private int id;
    private String userName;
    private String address;
    private String password;
    private String role;
}
