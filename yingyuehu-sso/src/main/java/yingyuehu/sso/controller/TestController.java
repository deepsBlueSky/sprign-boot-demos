package yingyuehu.sso.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import yingyuehu.sso.helper.LoginHelper;
import yingyuehu.sso.po.LoginResult;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 14:59
 **/
@RestController
@Slf4j
public class TestController {

    @Autowired
    private LoginHelper loginHelper;

    @GetMapping("/login")
    public LoginResult testLogin(String account,String password,String callback) {
        return loginHelper.login(account, password, callback);
    }
}
