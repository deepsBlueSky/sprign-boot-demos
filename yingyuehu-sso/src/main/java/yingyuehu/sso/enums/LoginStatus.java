package yingyuehu.sso.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 登录状态枚举
 */
@AllArgsConstructor
@Getter
public enum LoginStatus {

    SUCCESS(1,"登录成功"),
    ING(0,"登录中"),
    FAIL(-1,"登录失败"),
    ERROR(-2,"登录异常"),
    CALLBACK_ERROR(-3,"回调异常"),
    ACCOUNT_LOCK(-4,"账户被锁定"),
    EXPIRE(-5,"登录过期");

    /**
     * 登录状态码
     */
    private int code;

    /**
     * 登录状态消息
     */
    private String message;


}
