package yingyuehu.sso.enums;

public enum LoginTypes {
    /**
     * 登入
     */
    IN,

    /**
     * 登出
     */
    OUT;
}
