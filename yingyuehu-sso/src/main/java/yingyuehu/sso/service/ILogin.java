package yingyuehu.sso.service;

import yingyuehu.sso.po.LoginResult;

public interface ILogin {
    /**
     * 用户登录
     * @param account       用户名
     * @param password      密码
     * @param callbackUrl   用户验证回调
     * @return
     */
    LoginResult login(String account, String password, String callbackUrl);
}
