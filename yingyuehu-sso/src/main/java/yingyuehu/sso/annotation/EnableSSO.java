package yingyuehu.sso.annotation;

import org.springframework.context.annotation.Import;
import yingyuehu.sso.config.SSOAutoConfiguration;

import java.lang.annotation.*;

/**
 * @program: SpringBootDemos
 * @description: 开启SSO注解
 * @author: Kangsen
 * @create: 2022-05-20 13:42
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({SSOAutoConfiguration.class})
public @interface EnableSSO {
}
