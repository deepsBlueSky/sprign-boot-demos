package yingyuehu.sso.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 10:39
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginUser {
    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 登录时间
     */
    private String loginTime;
}
