package yingyuehu.sso.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import yingyuehu.sso.enums.LoginStatus;
import yingyuehu.sso.enums.LoginTypes;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 10:20
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResult {

    /**
     * 登录用户对象
     */
    private LoginUser loginUser;

    /**
     * 登录用户令牌
     */
    private UserToken userToken;

    /**
     * 登录状态
     */
    private LoginStatus loginStatus;

    /**
     * 登录类型
     */
    private LoginTypes loginTypes;

}
