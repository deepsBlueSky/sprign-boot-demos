package yingyuehu.sso.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import utils.code.MD5;
import utils.common.StringUtils;

import java.util.Calendar;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 10:40
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserToken {
    /**
     * token
     */
    private String token;
    /**
     * 过期时间
     */
    private String expireTime;

    /**
     * 获取token
     * @return
     */
    public static UserToken getUserToken() {
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE, 30);
        return new UserToken(MD5.getMD5String(StringUtils.ranStr(32)), String.valueOf(nowTime.getTimeInMillis()));
    }

    /**
     * 生成Token
     */
    private String generateToken() {
        return MD5.getMD5String(StringUtils.ranStr(32));
    }
}
