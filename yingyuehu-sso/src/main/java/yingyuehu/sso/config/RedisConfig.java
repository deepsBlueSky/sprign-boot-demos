package yingyuehu.sso.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import yingyuehu.redis.helper.RedisDistributionLockHelper;
import yingyuehu.redis.helper.RedisHelper;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-20 14:51
 **/
@Component
public class RedisConfig {

    @Bean
    public RedisHelper redisHelper(){
        return new RedisHelper();
    }

    @Bean
    public RedisDistributionLockHelper redisDistributionLockHelper(){
        return new RedisDistributionLockHelper();
    }
}
