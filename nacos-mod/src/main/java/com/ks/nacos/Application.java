package com.ks.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-27 09:49
 **/
@SpringBootApplication
//@NacosPropertySource(dataId = "nacos-service-dev.yml", autoRefreshed = true)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
