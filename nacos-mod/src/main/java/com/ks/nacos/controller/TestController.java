package com.ks.nacos.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-27 10:45
 **/
@RestController
@RequestMapping("/config")
@RefreshScope //动态刷新
public class TestController {

    @Value(value = "${nacos-test:}")
    //@NacosValue(value = "${test:hello}", autoRefreshed = true)
    private Object test;

    @Value(value = "${useLocalCache:false}")
    private boolean useLocalCache;

    @GetMapping("/getTest")
    @ResponseBody
    public Object getIndex(){
        return test;
    }

    @GetMapping("/get")
    @ResponseBody
    public boolean get(){
        return useLocalCache;
    }

    public static void main(String[] args) {
        String a = "";
        for (int i =0 ;i < 6;i++){
            a += " _ "+i+"->" + i;
        }
        System.out.println(a);
        a = a.substring(2,a.length());
        System.out.println(a);
    }
}
