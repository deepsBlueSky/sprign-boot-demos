package com.ks.mta.controller;

import com.commonBean.Orders;
import com.commonBean.Student;
import com.ks.api.service.OrderService;
import com.ks.mta.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 10:59
 **/
@RestController
@Slf4j
public class IndexController {

    @Resource
    private StudentService studentService;

    @DubboReference(version = "1.0.0", protocol = "dubbo",loadbalance = "random",retries = 0)
    private com.ks.api.service.StudentService apiStudentService;

    @DubboReference(version = "1.0.0", protocol = "dubbo",loadbalance = "random",retries = 0)
    private OrderService orderService;

    @GetMapping("/index")
    public Object index(){
        return studentService.studentAll();
    }

    @GetMapping("/consumer")
    public Object consumer(){
        return apiStudentService.getAllUser();
    }

    @GetMapping("/addStudent")
    public Student addStudent(@RequestParam("name")String name,
                              @RequestParam("cla")String cla){
        log.info("添加student name:{}  cla:{}",name,cla);
        try {
            return apiStudentService.addStudent(name,cla);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping("/addOrder")
    public Orders addOrder(@RequestParam("userId")int userId){
        log.info("添加order userId:{}",userId);
        try {
            return orderService.addOrder(userId, UUID.randomUUID().toString().replaceAll("-", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 提交订单
     * @param userId
     * @return
     */
    @GetMapping("/subOrder")
    public Orders subOrder(@RequestParam("userId")int userId){
        log.info("提交订单 userId:{}",userId);
        try {
            return studentService.subOrder(userId,UUID.randomUUID().toString().replaceAll("-", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
