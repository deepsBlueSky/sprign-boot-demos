package com.ks.mta.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.commonBean.Student;
import org.springframework.stereotype.Repository;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 14:07
 **/
@Repository
public interface StudentMapper extends BaseMapper<Student> {
}
