package com.ks.mta.service;

import com.commonBean.Orders;
import com.commonBean.Student;

import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 14:13
 **/

public interface StudentService {

    Student getStudent(int id);

    List<Student> studentAll();

    /**
     * 提交订单
     * @param userId
     * @return
     */
    Orders subOrder(int userId, String orderId);
}
