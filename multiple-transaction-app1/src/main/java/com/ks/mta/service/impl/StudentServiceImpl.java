package com.ks.mta.service.impl;

import com.commonBean.Orders;
import com.commonBean.Student;
import com.ks.api.service.OrderService;
import com.ks.mta.mapper.StudentMapper;
import com.ks.mta.service.StudentService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-11 14:14
 **/
@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

    @Resource
    private StudentMapper studentMapper;

    @DubboReference(version = "1.0.0", protocol = "dubbo",loadbalance = "random",retries = 0)
    private OrderService orderService;

    @Override
    public Student getStudent(int id) {
        return studentMapper.selectById(id);
    }

    @Override
    public List<Student> studentAll() {
        return studentMapper.selectByMap(null);
    }


    /**
     * 用户提交订单
     *
     * @param userId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @GlobalTransactional(rollbackFor = Exception.class)
    public Orders subOrder(int userId,String orderId) {
        Student student = studentMapper.selectById(userId);
        if(null == student){
            return null;
        }
        student.setCount(student.getCount() +1);
        int updateById = studentMapper.updateById(student);
        log.info("updateById:{}",updateById);
        if(student.getName().equalsIgnoreCase("王二狗")){
            throw new RuntimeException("不给这人提交订单");
        }
        /*Orders orders = orderService.addOrder(student.getId(), UUID.randomUUID().toString().replaceAll("-", ""));*/
        Orders orders = orderService.addOrder(student.getId(), orderId);
        log.info("orders:{}",orders);
        return orders;
    }
}
