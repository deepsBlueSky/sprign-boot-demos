package com.test.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.util.CollectionUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Description：
 * HTTP调用；https路径会转化为http，所以需要能使用http调用访问的路径才可以使用
 * 用法：HttpUtils.doGet(HttpUtils.create().addHostPort("http://suyun.nevtsp.com/").addProxy("116.30.240.246:80").build());
 */
@Slf4j
public final class HttpUtils {

    private static final int DOWN_LOAD_READ_BYTE_BUFFER = 1024;

    private static final int SO_TIMEOUT_MS = 5000;

    private static final int CONNECTION_TIMEOUT_MS = 3000;

    private static final int SUCCESS = 200;

    private static final int HTTP_PORT = 80;

    private static final int HTTPS_PORT = 443;

    private static final Random RANDOM = new Random();

    /**
     * 创建参数对象信息
     *
     * @return
     */
    public static HttpClientParam create() {
        return new HttpClientParam();
    }

    /**
     * get
     *
     * @param httpClientParam
     * @return
     */
    public static String doGet(final HttpClientParam httpClientParam) {
        HttpGet request = new HttpGet(httpClientParam.getUrl());
        buildHeaderProxy(request, httpClientParam);
        return execute(httpClientParam.getCloseableHttpClient(), request);
    }

    /**
     * Post From
     *
     * @param httpClientParam
     * @return
     */
    public static String doPost(final HttpClientParam httpClientParam) {
        HttpPost request = new HttpPost(httpClientParam.getUrl());
        buildHeaderProxy(request, httpClientParam);
        if (!CollectionUtils.isEmpty(httpClientParam.getBodyMap())) {
            /*List<NameValuePair> nameValuePairList = Lists.newArrayList();
            for (String key : httpClientParam.getBodyMap().keySet()) {
                nameValuePairList.add(new BasicNameValuePair(key, httpClientParam.getBodyMap().get(key)));
            }
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(nameValuePairList, StandardCharsets.UTF_8);
            formEntity.setContentType("application/x-www-form-urlencoded; charset=UTF-8");*/

            JSONObject paramMap = new JSONObject();
            for (String key : httpClientParam.getBodyMap().keySet()) {
                paramMap.put(key, httpClientParam.getBodyMap().get(key));
            }
            /*UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(nameValuePairList, StandardCharsets.UTF_8);
            formEntity.setContentType("application/x-www-form-urlencoded; charset=UTF-8");*/

            request.setEntity(new StringEntity(paramMap.toJSONString(), ContentType.APPLICATION_JSON));
        } else if (!StringUtils.isEmpty(httpClientParam.getBodyStr())) {
            request.setEntity(new StringEntity(httpClientParam.getBodyStr(), StandardCharsets.UTF_8));
        } else if (httpClientParam.getBody() != null) {
            request.setEntity(new ByteArrayEntity(httpClientParam.getBody()));
        }
        return execute(httpClientParam.getCloseableHttpClient(), request);
    }

    /**
     * Put Stream
     *
     * @param httpClientParam
     * @return
     */
    public static String doPut(final HttpClientParam httpClientParam) {
        HttpPut request = new HttpPut(httpClientParam.getUrl());
        buildHeaderProxy(request, httpClientParam);
        if (StringUtils.isNotEmpty(httpClientParam.getBodyStr())) {
            request.setEntity(new StringEntity(httpClientParam.getBodyStr(), StandardCharsets.UTF_8));
        } else if (httpClientParam.getBody() != null) {
            request.setEntity(new ByteArrayEntity(httpClientParam.getBody()));
        }
        return execute(httpClientParam.getCloseableHttpClient(), request);
    }

    /**
     * Delete Stream
     *
     * @param httpClientParam
     * @return
     */
    public static String doDelete(final HttpClientParam httpClientParam) {
        HttpDelete request = new HttpDelete(httpClientParam.getUrl());
        buildHeaderProxy(request, httpClientParam);
        return execute(httpClientParam.getCloseableHttpClient(), request);
    }

    /**
     * 执行信息
     *
     * @param httpClient
     * @param request
     * @return
     */
    private static String execute(CloseableHttpClient httpClient, HttpUriRequest request) {
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(request);
            if (response == null) {
                return JSONObject.toJSONString(new Failed("100000", "返回信息为空"));
            } else {
                if (response.getStatusLine().getStatusCode() == SUCCESS) {
                    return EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
                } else {
                    return JSONObject.toJSONString(new Failed(response.getStatusLine().getStatusCode() + "", "返回信息失败"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("执行失败"+ e);
            return JSONObject.toJSONString(new Failed("100001", e.getMessage()));
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /**
     * 根据URL下载文件信息，保存在指定的目录下面
     *
     * @param urlStr
     * @param fileName
     * @param savePath
     */
    public static void downLoadFromUrl(String urlStr, String fileName, String savePath) {
        downLoadFromUrl(urlStr, fileName, savePath, null, null);
    }

    /**
     * 从网络Url中下载文件
     * http://suyun-nevtsp-foreverfile.oss-cn-shenzhen.aliyuncs.com/vehicle_admin/dbc_excel/2020/11/16/L16_V1.1_20201116095257890.xlsx
     *
     * @param urlStr   下载URL
     * @param fileName 下载文件名
     * @param savePath 保存的路径
     * @param tokeName Token 名称
     * @param token    需要的TOKEN新
     * @throws IOException
     */
    public static void downLoadFromUrl(String urlStr, String fileName, String savePath, String tokeName, String token) {
        FileOutputStream fos = null;
        InputStream is = null;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(CONNECTION_TIMEOUT_MS);
            //防止屏蔽程序抓取而返回403错误
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            if (StringUtils.isNotEmpty(token)) {
                conn.setRequestProperty(tokeName, token);
            }
            //得到输入流
            is = conn.getInputStream();
            //获取自己数组
            byte[] getData = readInputStream(is);
            //文件保存位置
            File saveDir = new File(savePath);
            if (!saveDir.exists()) {
                saveDir.mkdir();
            }
            File file = new File(saveDir + File.separator + fileName);
            fos = new FileOutputStream(file);
            fos.write(getData);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 添加头部信息，判断是否使用代理
     *
     * @param httpRequestBase
     * @param httpClientParam
     */
    private static void buildHeaderProxy(HttpRequestBase httpRequestBase, final HttpClientParam httpClientParam) {
        addHeader(httpRequestBase, httpClientParam.getHeaderMap());
        if (StringUtils.isEmpty(httpClientParam.getProxy()) || !httpClientParam.getProxy().contains(StaticConstant.COLON)) {
            httpRequestBase.setConfig(RequestConfig.custom()
                    .setSocketTimeout(SO_TIMEOUT_MS)
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS).build());
            return;
        }
        Proxy proxy = null;
        if (httpClientParam.getProxy().startsWith(StaticConstant.HTTP)) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(httpClientParam.getProxy().replace(StaticConstant.HTTP, StaticConstant.EMPTY_STR), HTTP_PORT));
        } else if (httpClientParam.getProxy().startsWith(StaticConstant.HTTPS)) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(httpClientParam.getProxy().replace(StaticConstant.HTTPS, StaticConstant.EMPTY_STR), HTTPS_PORT));
        } else {
            String[] ipPort = httpClientParam.getProxy().split(StaticConstant.COLON);
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ipPort[0], Integer.parseInt(ipPort[1])));
        }
        InetSocketAddress socketAddress = (InetSocketAddress) proxy.address();
        String ip = socketAddress.getAddress().getHostAddress();
        Integer port = socketAddress.getPort();
        if (StringUtils.isNotEmpty(ip) && port != null) {
            HttpHost httpHost = new HttpHost(ip, port);
            httpRequestBase.setConfig(RequestConfig.custom().setProxy(httpHost)
                    .setSocketTimeout(SO_TIMEOUT_MS)
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS).build());
        } else {
            httpRequestBase.setConfig(RequestConfig.custom()
                    .setSocketTimeout(SO_TIMEOUT_MS)
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS).build());
        }
        if (httpRequestBase.getHeaders("X-Forwarded-For") == null || httpRequestBase.getHeaders("X-Forwarded-For").length == 0) {
            httpRequestBase.addHeader("X-Forwarded-For", httpClientParam.getForwardIp());
        }
    }

    private static void addHeader(HttpRequestBase httpRequestBase, Map<String, String> headerMap) {
        if (!CollectionUtils.isEmpty(headerMap)) {
            for (Map.Entry<String, String> header : headerMap.entrySet()) {
                httpRequestBase.addHeader(header.getKey(), header.getValue());
            }
        }
        if (headerMap.containsKey("User-Agent")) {
            return;
        }
        httpRequestBase.addHeader("User-Agent", USER_AGENT_ALL[RANDOM.nextInt(USER_AGENT_ALL.length - 1)]);
    }

    /**
     * 从输入流中获取字节数组
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    private static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[DOWN_LOAD_READ_BYTE_BUFFER];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

    /**
     * 访问参数
     */
    @Getter
    public static class HttpClientParam {
        /**
         * 拼装之后的URL信息
         */
        private String url;
        /**
         * 访问客户端
         */
        private CloseableHttpClient closeableHttpClient;
        /**
         * 访问协议IP和端口例如：http://10.0.0.5:8080 不能为空
         */
        private String hostPort;
        /**
         * 访问的方法 例如： /a/query 可以为空
         */
        private String path;
        /**
         * URL带的参数信息 key 为name value为值 可以为空 ，host+path+url会拼接成真正的URL
         */
        private Map<String, String> urlParamMap = Maps.newHashMap();
        /**
         * 头部参数 key 为name value为值 可以为空
         */
        private Map<String, String> headerMap = Maps.newHashMap();
        /**
         * FormData 参数信息
         */
        private Map<String, String> bodyMap = Maps.newHashMap();
        /**
         * body字符串
         */
        private String bodyStr;
        /**
         * body byte数组 bodyMap，bodyStr，body三个只能使用一个，优先级 bodyMap>bodyStr>body
         */
        private byte[] body;
        /**
         * 是否使用代理，如果为空，表示不代理 10.0.0.1:8080
         */
        private String proxy;

        private String forwardIp;

        private HttpClientParam() {
        }

        /**
         * 访问协议IP和端口例如：http://10.0.0.5:8080 不能为空
         */
        public HttpClientParam addHostPort(String hostPort) {
            this.hostPort = hostPort;
            return this;
        }

        /**
         * 访问的方法映射 例如： /a/query 可以为空
         */
        public HttpClientParam addPath(String path) {
            this.path = path;
            return this;
        }

        /**
         * URL带的参数信息 key 为name value为值 可以为空 ，host+path+url会拼接成真正的URL
         *
         * @param key
         * @param value
         * @return
         */
        public HttpClientParam addQuery(String key, String value) {
            this.urlParamMap.put(key, value);
            return this;
        }

        /**
         * URL带的参数信息 key 为name value为值 可以为空 ，host+path+url会拼接成真正的URL
         *
         * @param queryMap
         * @return
         */
        public HttpClientParam addQueryAll(Map<String, String> queryMap) {
            this.urlParamMap.putAll(queryMap);
            return this;
        }

        /**
         * 头部参数 key 为name value为值 可以为空
         *
         * @param key
         * @param value
         * @return
         */
        public HttpClientParam addHeader(String key, String value) {
            this.headerMap.put(key, value);
            return this;
        }

        /**
         * 头部参数 key 为name value为值 可以为空
         *
         * @param headerMap
         * @return
         */
        public HttpClientParam addHeaderAll(Map<String, String> headerMap) {
            this.headerMap.putAll(headerMap);
            return this;
        }

        /**
         * FormData 参数信息 bodyMap，bodyStr，body三个只能使用一个，优先级 bodyMap>bodyStr>body
         *
         * @param key
         * @param value
         */
        public HttpClientParam addBodyMap(String key, String value) {
            this.bodyMap.put(key, value);
            return this;
        }

        /**
         * FormData 参数信息 bodyMap，bodyStr，body三个只能使用一个，优先级 bodyMap>bodyStr>body
         *
         * @param bodyMap
         */
        public HttpClientParam addBodyMapAll(Map<String, String> bodyMap) {
            this.bodyMap.putAll(bodyMap);
            return this;
        }

        /**
         * body字符串 bodyMap，bodyStr，body三个只能使用一个，优先级 bodyMap>bodyStr>body
         *
         * @param bodyStr
         * @return
         */
        public HttpClientParam addBodyStr(String bodyStr) {
            this.bodyStr = bodyStr;
            return this;
        }

        /**
         * byte数组 bodyMap，bodyStr，body三个只能使用一个，优先级 bodyMap>bodyStr>body
         *
         * @param body
         * @return
         */
        public HttpClientParam addBody(byte[] body) {
            this.body = body;
            return this;
        }

        /**
         * 是否使用代理，如果为空，表示不代理 10.0.0.1:8080
         *
         * @param proxy
         * @return
         */
        public HttpClientParam addProxy(String proxy) {
            this.proxy = proxy;
            return this;
        }

        /**
         * 创建对象信息
         */
        public HttpClientParam build() {
            boolean isNoValid = StringUtils.isEmpty(this.hostPort) || (!this.hostPort.startsWith(StaticConstant.HTTP) && !this.hostPort.startsWith(StaticConstant.HTTPS));
            if (isNoValid) {
                System.out.println("路径信息错误 hostPort:{}" + this.hostPort);
                return null;
            }
            if (this.hostPort.endsWith(StaticConstant.SLASH)) {
                this.hostPort = this.hostPort.substring(0, this.hostPort.lastIndexOf(StaticConstant.COLON));
            }
            if (StringUtils.isNotEmpty(this.path) && !this.path.startsWith(StaticConstant.SLASH)) {
                this.path = StaticConstant.SLASH + this.path;
            }
            try {
                if (this.proxy != null) {
                    String tempProxy = null;
                    this.forwardIp = this.proxy.substring(0, this.proxy.lastIndexOf(StaticConstant.COLON));
                    /*if (this.hostPort.startsWith(StaticConstant.HTTP)) {
                        tempProxy = this.hostPort.replace(StaticConstant.HTTP, StaticConstant.EMPTY_STR);
                        this.hostPort = StaticConstant.HTTP + this.proxy;
                    } else {
                        tempProxy = this.hostPort.replace(StaticConstant.HTTPS, StaticConstant.EMPTY_STR);
                        this.hostPort = StaticConstant.HTTPS + this.proxy;
                    }
                    this.proxy = tempProxy;*/
                }
                this.url = buildUrl(this.hostPort, this.path, this.urlParamMap);
                this.closeableHttpClient = wrapClient(this.hostPort);
            } catch (Exception e) {
                System.out.println("创建访问对象错误" +  e);
                return null;
            }
            return this;
        }

        /**
         * 拼接URL
         *
         * @param host
         * @param path
         * @param queryMap
         * @return
         * @throws UnsupportedEncodingException
         */
        private String buildUrl(String host, String path, Map<String, String> queryMap) {
            StringBuilder sbUrl = new StringBuilder();
            sbUrl.append(host);
            if (!StringUtils.isBlank(path)) {
                sbUrl.append(path);
            }
            if (CollectionUtils.isEmpty(queryMap)) {
                return sbUrl.toString();
            }
            StringBuilder sbQuery = new StringBuilder();
            for (Map.Entry<String, String> query : queryMap.entrySet()) {
                if (sbQuery.length() > 0) {
                    sbQuery.append(StaticConstant.AND);
                }
                if (StringUtils.isBlank(query.getKey()) && !StringUtils.isBlank(query.getValue())) {
                    sbQuery.append(query.getValue());
                }
                if (!StringUtils.isBlank(query.getKey())) {
                    sbQuery.append(query.getKey());
                    if (!StringUtils.isBlank(query.getValue())) {
                        sbQuery.append("=");
                        try {
                            sbQuery.append(URLEncoder.encode(query.getValue(), StandardCharsets.UTF_8.name()));
                        } catch (UnsupportedEncodingException e) {
                            System.out.println("编码失败 value: {}" +  query.getValue());
                        }
                    }
                }
            }
            if (sbQuery.length() > 0) {
                sbUrl.append(StaticConstant.UN_KNOW).append(sbQuery);
            }
            return sbUrl.toString();
        }

        private CloseableHttpClient wrapClient(String host) {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            if (host.startsWith(StaticConstant.HTTPS)) {
                SSLContext sslContext = null;
                try {
                    sslContext = SSLContexts.custom().loadTrustMaterial(null, (x509Certificates, s) -> {
                        return true;
                    }).build();
                } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
                    e.printStackTrace();
                }
                //创建httpClient
                return HttpClients.custom().setSSLContext(sslContext).
                        setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
            }
            return httpClient;
        }
    }

    private static final String[] USER_AGENT_ALL = {
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2224.3 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:21.0.0) Gecko/20121011 Firefox/21.0.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
            "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Macintosh; U; Mac OS X Mach-O; en-US; rv:2.0a) Gecko/20040614 Firefox/3.0.0 ",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.0.3) Gecko/2008092414 Firefox/3.0.3",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 AlexaToolbar/alxf-2.0 Firefox/3.6.14",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
            "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0",
            "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)",
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)",
            "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"
    };

    private static final List<String> PROXY_POOL = Lists.newArrayList(
            "39.130.150.43:80",
            "211.139.26.16:80",
            "218.202.1.58:80"
    );

    @Data
    static class Failed{
        String code;
        String msg;

        public Failed(String s, String msg) {
            this.code = s;
            this.msg = msg;
        }
    }

    static class StaticConstant{
        private static String COLON     =   ":";
        private static String HTTP      =   "http";
        private static String HTTPS     =   "https";
        private static String EMPTY_STR =   "";
        private static String SLASH     =   "/";
        private static String AND       =   "&";
        private static String UN_KNOW   =   "?";
    }

    private static String getWebinfo(String myurls) {
        String myres = null;
        try {
            //1.找水源---创建URL（统一资源定位器）
            URL url=new URL(myurls);
            //2.开水闸---openCOnnection
            HttpURLConnection httpURLConnection= (HttpURLConnection) url.openConnection();
            //3.建管道---InputStream
            InputStream inputStream=httpURLConnection.getInputStream();
            //4.建蓄水池---InputStreamReader
            InputStreamReader inputStreamReader=new InputStreamReader(inputStream,"UTF-8");
            //5.水桶盛水——BufferedReader
            BufferedReader bufferedReader=new BufferedReader(inputStreamReader);

            StringBuffer stringBuffer=new StringBuffer();
            String temp=null;

            //循环做盛水工作---while循环
            while ((temp=bufferedReader.readLine())!=null){
                stringBuffer.append(temp);
            }
            //关闭水池入口，从管道到水桶
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            //打印日志
            //System.out.println(stringBuffer.toString());
            myres=stringBuffer.toString();
//            System.out.println("myres--"+myres);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return myres;
    }

    public static void main(String[] args) {
        String bodyStr = "{\n" +
                "  \"timestamp\": 1660874837865,\n" +
                "  \"verifyStr\": \"\",\n" +
                "  \"apiName\": \"getMatchInfoById\",\n" +
                "  \"leagueMatchId\": \"5311729\"\n" +
                "}";
        Map<String, String> body = new HashMap<String, String>();
        body.put("timestamp",String.valueOf(System.currentTimeMillis()));
        body.put("verifyStr","");
        body.put("apiName","getMatchInfoById");
        body.put("leagueMatchId","5311729");
        Map<String, String> header = new HashMap<String, String>();
        header.put("Accept", "*/*");
        header.put("Accept-Encoding", "gzip, deflate, br");
        header.put("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2");
        header.put("Cache-Control", "max-age=0");
        header.put("Connection", "keep-alive");
        header.put("Content-Length", "1290");
        header.put("Content-Type", "text/plain");
        header.put("DNT", "1");
        //header.put("Host", "v.qidian.qq.com");
        header.put("Referer", "http://icanhazip.com/");
        header.put("Origin", "http://icanhazip.com");
        header.put("Sec-Fetch-Dest", "empty");
        header.put("Sec-Fetch-Mode", "cors");
        header.put("Sec-Fetch-Site", "cross-site");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0");

        /*String result = HttpUtils.doPost(
            HttpUtils.create()
                    .addHostPort("http://tt.aicai.com/sportdata/f?agentId=2335059&platform=wap&appVersion=7.4.3&from=jcob&jcobSessionId=06878837-f92e-508e-80aa-2878f353676d")
                    .addBodyMapAll(body)
                    .addHeaderAll(header)
                    .addProxy(PROXY_POOL.get(RANDOM.nextInt(PROXY_POOL.size() - 1)))
                    .build()
        );*/
        /*String webinfo = getWebinfo("https://www.zdaye.com/dayProxy.html");
        Document document = Jsoup.parse(webinfo);
        System.out.println("document=>"+document);
        Elements elements = document.getElementById("J_posts_list")
                .getElementsByClass("thread_title")
                .tagName("a");
        List<String> pageList = new ArrayList<>(elements.size());
        for (Element element : elements) {
            String attr = element.select("a").attr("href");
            pageList.add("https://www.zdaye.com/dayProxy.html".replace("/dayProxy.html",attr));
        }
        System.out.println("pageList => "+ pageList);
        for (String pageUrl : pageList) {
            String pageContent = getWebinfo(pageUrl);
            System.out.println("pageContent=>"+pageContent);
            Document doc = Jsoup.parse(pageContent);
            Elements select = doc.getElementById("ipc").tagName("tbody").select("tr");
            System.out.println(select);
            break;
        }*/

        List<String> proxyList = new ArrayList<>();

        String pageContent = "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge,chrome=1\" /><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><meta http-equiv=\"Cache-Control\" content=\"no-cache\" /><meta name=\"format-detection\" content=\"telephone=no\"><meta name=\"viewport\" content=\"width=device-width, minimum-scale=1.0, maximum-scale=1.0\" /><meta name=\"apple-mobile-web-app-capable\" content=\"yes\" /><title>2022年8月23日14时 国内最新免费HTTP代理IP - 站大爷 - 企业级高品质Http代理IP_Socks5代理服务器_免费代理IP</title><meta name=\"keywords\" content=\"代理ip,代理服务器,在线代理,ip代理,http代理,网络代理,代理软件,免费代理,免费代理ip,代理上网,免费代理服务器,socks5代理,socks代理,站大爷\"><meta name=\"description\" content=\"站大爷每日随时为您分享免费代理IP；免费代理IP由第三方平台提供，质量较低，仅供开发者学习用。若需要高质量代理IP请选购本站的企业级高品质代理IP。58.220.95.31:10174@HTTP#[未知]江苏省扬州市 电信218.202.7.125:80@HTTP#[透明]云南省昆明市 移动58.220.95.30:10174@HTTP#[未知]江苏省扬州市 电信58.220.95.34:101...\"><link href=\"/css/css.css?v=5.7\" rel=\"stylesheet\" type=\"text/css\"><link href=\"/cloud/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css\" rel=\"stylesheet\" type=\"text/css\"/><link href=\"/css/zzsc.css\" rel=\"stylesheet\" type=\"text/css\"><link rel=\"shortcut icon\" href=\"/images/favicon.ico\" type=\"image/x-icon\"><script type=\"text/javascript\" src=\"/js/alert.js?v=1.1\"></script><script type=\"text/javascript\" src=\"/js/jquery.pack.js\"></script><script type=\"text/javascript\" src=\"/js/jquery.SuperSlide.js\"></script><script src=\"/js/gt.js\"></script><script src=\"/cloud/assets/theme/assets/global/plugins/jquery.min.js?v=1.01\" type=\"text/javascript\"></script><script src=\"/cloud/assets/plugin/layer/layer.js\" type=\"text/javascript\"></script><style type=\"text/css\" id=\"styles_js\">.smartnlp-theme-color{background-color: #2d8ef2;}.smartnlp-msg-right-color{background-color: #2d8ef2;}.triangle {border-color: transparent transparent transparent #2d8ef2; } </style></head><body><!--head--><div class=\"header\">            <div class=\"head\">                <div class=\"logo fl\">                    <a href=\"/\">                        <img src=\"/images/logo.png\">                    </a>                </div>                <div class=\"deng fr\">                                       <span>                        <a href=\"/Users/Reg.html\" class=\"reg\">注册</a>                    </span>\t\t\t\t\t <span>                        <a href=\"/Users/Login.html\">登录</a>                                            </span>                </div>                <ul class=\"nav fr\">                    <li>                        <a href=\"/\">首页</a>                        <b></b>                    </li>                    <li>                        <a>HTTP代理&nbsp;&nbsp;<i><img src=\"/images/xia.png\"></i><em><img src=\"/images/xia01.png\"></em><label><img src=\"/images/xia02.png\"></label></a>                        <b></b>  \t\t\t\t\t  <dl style=\"width: 250px;\">                \t<dt><a href=\"/ShortProxy.html\">                    \t<strong><img src=\"/images/nav1.png\"></strong>                        <span>短效优质代理</span>                    </a></dt>                    <dt><a href=\"/PrivateProxy.html\">                    \t<strong><img src=\"/images/nav2.png\"></strong>                        <span>长效优质代理</span>                    </a></dt>                \t</dl>                  </li>\t\t\t\t  \t\t\t\t  \t\t\t\t  <li>                        <a>特色IP池&nbsp;&nbsp;<i><img src=\"/images/xia.png\"></i><em><img src=\"/images/xia01.png\"></em><label><img src=\"/images/xia02.png\"></label></a>                        <b></b>  \t\t\t\t\t  <dl  style=\"width: 250px;\">\t\t\t\t\t                    <dt><a href=\"/ExclusiveProxy.html\">                    \t<strong><img src=\"/images/p06.png\"></strong>                        <span>独享IP池</span>                    </a></dt>\t\t\t\t\t<dt><a href=\"/CotenancyProxy.html\">                    \t<strong><img src=\"/images/nav3.png\"></strong>                        <span>合租IP池</span>                    </a></dt>                \t</dl>                    </li>\t\t\t\t                      <li>                        <a>Socks5代理&nbsp;&nbsp;<i><img src=\"/images/xia.png\"></i><em><img src=\"/images/xia01.png\"></em><label><img src=\"/images/xia02.png\"></label></a>                        <b></b>  \t\t\t\t\t  <dl  style=\"width: 250px;\">                \t<dt><a href=\"/ShortS5Proxy.html\">                    \t<strong><img src=\"/images/nav5.png\"></strong>                        <span>短效Socks5代理</span>                    </a></dt>                    <dt><a href=\"/PrivateS5Proxy.html\">                    \t<strong><img src=\"/images/p05.png\"></strong>                        <span>长效Socks5代理</span>                    </a></dt>                \t</dl>                    </li>\t\t                    <li>                         <a>文档&nbsp;&nbsp;<i><img src=\"/images/xia.png\"></i><em><img src=\"/images/xia01.png\"></em><label><img src=\"/images/xia02.png\"></label></a>                        <b></b>  \t\t\t\t\t  <dl  style=\"width: 250px;\">                \t<dt><a href=\"/Help.html\">                    \t<strong><img src=\"/images/ask.png\"></strong>                        <span>帮助文档</span>                    </a></dt>                    <dt><a href=\"/doc/api/ShortProxy/\">                    \t<strong><img src=\"/images/nav5.png\"></strong>                        <span>API文档</span>                    </a></dt>                \t</dl>                    </li>\t\t\t\t\t\t\t\t\t\t<li>                      <a href=\"/know.html\">IP课堂</a>                        <b></b>                    </li>\t\t\t\t\t\t\t\t\t\t<li>                      <a href=\"/ip/whois/\">IP-Whois</a>                        <b></b>                    </li>\t\t\t\t\t\t\t\t\t\t              </ul>                <div class=\"sav fr\">                    <span>                        <img src=\"/images/nav.png\">                    </span>                    <a>                        <img src=\"/images/nav01.png\">                    </a>                </div>                <div class=\"zcs fr\">                    <a href=\"/Users/Login.html\" class=\"reg\">登陆</a>                </div>            </div>        </div><div class=\"xia ov\">            <ul>                <li>                    <a href=\"/\">首页</a>                </li>                <li><p id=\"p1\">HTTP代理&nbsp;&nbsp;&gt;</p>            \t<dl class=\"dis\" id=\"d1\">                <dt><a href=\"/ShortProxy.html\">                    <strong><img src=\"/images/nav1.png\"></strong>                    <span>短效优质代理</span>                </a></dt>                <dt><a href=\"/PrivateProxy.html\">                    <strong><img src=\"/images/nav2.png\"></strong>                    <span>长效优质代理</span>                </a></dt>                            \t</dl>        \t\t</li>\t\t\t\t\t\t\t\t\t<li><p id=\"p3\">特色IP池&nbsp;&nbsp;&gt;</p>            \t<dl class=\"\" id=\"d3\">\t\t\t\t<dt><a href=\"/ExclusiveProxy.html\">                    <strong><img src=\"/images/nav2.png\"></strong>                    <span>独享IP池</span>                </a></dt>\t\t\t\t<dt><a href=\"/CotenancyProxy.html\">                    <strong><img src=\"/images/nav3.png\"></strong>                    <span>合租IP池</span>                </a></dt>                            \t</dl>        \t\t</li>\t\t\t\t                <li><p id=\"p2\">Socks5代理&nbsp;&nbsp;&gt;</p>            \t<dl class=\"\" id=\"d2\">                <dt><a href=\"/ShortS5Proxy.html\">                    <strong><img src=\"/images/nav1.png\"></strong>                    <span style=\"font-size:12px;\">短效Socks5代理</span>                </a></dt>                <dt><a href=\"/PrivateS5Proxy.html\">                    <strong><img src=\"/images/nav2.png\"></strong>                    <span style=\"font-size:12px;\">长效Socks5代理</span>                </a></dt>            \t</dl>        \t\t</li>\t\t\t\t\t\t\t<li><p id=\"p4\">文档&nbsp;&nbsp;&gt;</p>            \t<dl class=\"\" id=\"d4\">                <dt><a href=\"/Help.html\">                    <strong><img src=\"/images/ask.png\"></strong>                    <span style=\"font-size:12px;\">帮助文档</span>                </a></dt>                <dt><a href=\"/doc/api/ShortProxy/\">                    <strong><img src=\"/images/nav5.png\"></strong>                    <span style=\"font-size:12px;\">API文档</span>                </a></dt>            \t</dl>        \t\t</li>              \t\t\t\t\t\t\t\t            </ul>            <dl class=\"zc ov\">                <dt>                    <a href=\"/Users/Reg.html\" class=\"reg\">注册</a>                </dt>                <dd class=\"fl\">                    <a href=\"/Users/Login.html\" class=\"loginRobot\">登录</a>                </dd>                           </dl>        </div><script>    window.onbeforeunload = function () {        //刷新后页面自动回到顶部        document.documentElement.scrollTop = 0; //ie下        document.body.scrollTop = 0; //非ie    }  </script><script>        $(document).ready(function () {        $(\".sav\").click(function () {            $(\".xia\").toggleClass(\"dis\");            $(\".header\").toggleClass(\"kk\");        });        $(\"#p1\").click(function () {            $(\"#d3\").removeClass(\"dis\");\t\t\t$(\"#d2\").removeClass(\"dis\");\t\t\t$(\"#d4\").removeClass(\"dis\");\t\t\t$(\"#d1\").toggleClass(\"dis\");        });\t\t$(\"#p2\").click(function () {            $(\"#d1\").removeClass(\"dis\");\t\t\t$(\"#d2\").toggleClass(\"dis\");\t\t\t$(\"#d3\").removeClass(\"dis\");\t\t\t$(\"#d4\").removeClass(\"dis\");        });\t\t$(\"#p3\").click(function () {            $(\"#d1\").removeClass(\"dis\");\t\t\t$(\"#d3\").toggleClass(\"dis\");\t\t\t$(\"#d2\").removeClass(\"dis\");\t\t\t$(\"#d4\").removeClass(\"dis\");        });\t\t$(\"#p4\").click(function () {            $(\"#d1\").removeClass(\"dis\");\t\t\t$(\"#d3\").removeClass(\"dis\");\t\t\t$(\"#d2\").removeClass(\"dis\");\t\t\t$(\"#d4\").toggleClass(\"dis\");        });    });    </script>  <style>  .page .layui-btn{\tdisplay:inline;\tpadding:5px;\tpadding-left:8px;\tpadding-right:8px;}  </style><script>$(document).ready(function () {   $(\".header\").addClass(\"bg\");});</script><link href=\"/css/arc/forum.css\" rel=\"stylesheet\" /><link href=\"/css/arc/index.css\" rel=\"stylesheet\" /><link href=\"/css/arc/content-list.css\" rel=\"stylesheet\" /><link href=\"/cloud/assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"><link rel=\"stylesheet\" type=\"text/css\" href=\"/cloud/assets/plugin/layui/css/layui.css\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"/css/table.css\" /><style>.alert-info{\tfont-size:18px !important;}h3{\tmargin-top:0px !important;}.page{\tfont-size:14px;\tmargin-top:20px;}.alert a:hover{\tborder-bottom:0px !important;\tpadding-bottom:0px;}.cont{\tfont-size:16px;\tline-height:2em;}.gonggao{\twidth:200px;}@media screen and (max-width:1149px) {\t.thread_posts_list{\t\twidth:65% !important;\t}\t\t.gonggao{\t\twidth:80px;\t}\t.alert,.cont{\t \t\t width:84%!important;\t }\t \t .alert-info{\t \t\t width:77%!important;\t }}\ttable tr,\ttable th,\ttable td {\t  font-size: 14px;\t\theight:30px;\t\tpadding:10px;\t}@media screen and (max-width:1441px) {\ttable tr,\ttable th,\ttable td {\t  font-size: 14px;\t\theight:30px;\t\tpadding:10px;\t}\t}@media screen and (max-width:1149px) { .alert{\t \t\t width:84%;\t }}.page{\tfont-size:14px;}#tj{\tmargin-top:25px;\tpadding-top: 6px;\tpadding-bottom: 6px;\tline-height:25px;}.ggul li{\twidth:100%;\theight:18px;\tline-height:18px;}.ggul li a{\tdisplay:inline;\tfloat:left;\twidth:50%;\theight:18px;\tline-height:18px;}.ggul li span{\twidth:20%;\tfloat:right;\theight:18px;\tpadding:0px;\tcolor:#fff;\tline-height:18px;}#ipc td a{\tcolor:#3598dc;}</style><div class=\"top\">    <div class=\"abox ov\" style=\"margin-bottom:50px;padding:15px;\">\t\t\t\t\t\t<DIV class=\"common_left\"><DIV class=\"gonggao\" id=\"listhothread\" style=\"\"><DIV class=\"ggtitle\"><SPAN class=\"ggshow\"><a href=\"/dayProxy.html\">每月代理IP</a></SPAN>\t\t\t\t\t\t <!-- <a class=\"ggmore\" href=\"javascript:void(0)\" id=\"buttonthread\">换一换</a> -->\t\t\t\t\t </DIV><UL class=\"ggul\"><LI><a href=\"/dayProxy/2022/8/1.html\">2022年8月</A><SPAN class='label label-default'>91篇</SPAN></LI><LI><a href=\"/dayProxy/2022/7/1.html\">2022年7月</A><SPAN class='label label-default'>123篇</SPAN></LI><LI><a href=\"/dayProxy/2022/6/1.html\">2022年6月</A><SPAN class='label label-default'>119篇</SPAN></LI><LI><a href=\"/dayProxy/2022/5/1.html\">2022年5月</A><SPAN class='label label-default'>124篇</SPAN></LI><LI><a href=\"/dayProxy/2022/4/1.html\">2022年4月</A><SPAN class='label label-default'>116篇</SPAN></LI><LI><a href=\"/dayProxy/2022/3/1.html\">2022年3月</A><SPAN class='label label-default'>124篇</SPAN></LI><LI><a href=\"/dayProxy/2022/2/1.html\">2022年2月</A><SPAN class='label label-default'>109篇</SPAN></LI><LI><a href=\"/dayProxy/2022/1/1.html\">2022年1月</A><SPAN class='label label-default'>123篇</SPAN></LI><LI><a href=\"/dayProxy/2021/12/1.html\">2021年12月</A><SPAN class='label label-default'>124篇</SPAN></LI><LI><a href=\"/dayProxy/2021/11/1.html\">2021年11月</A><SPAN class='label label-default'>120篇</SPAN></LI><LI><a href=\"/dayProxy/2021/10/1.html\">2021年10月</A><SPAN class='label label-default'>124篇</SPAN></LI><LI><a href=\"/dayProxy/2021/9/1.html\">2021年9月</A><SPAN class='label label-default'>218篇</SPAN></LI><LI><a href=\"/dayProxy/2021/8/1.html\">2021年8月</A><SPAN class='label label-default'>445篇</SPAN></LI><LI><a href=\"/dayProxy/2021/7/1.html\">2021年7月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2021/6/1.html\">2021年6月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2021/5/1.html\">2021年5月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2021/4/1.html\">2021年4月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2021/3/1.html\">2021年3月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2021/2/1.html\">2021年2月</A><SPAN class='label label-default'>673篇</SPAN></LI><LI><a href=\"/dayProxy/2021/1/1.html\">2021年1月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/12/1.html\">2020年12月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/11/1.html\">2020年11月</A><SPAN class='label label-default'>697篇</SPAN></LI><LI><a href=\"/dayProxy/2020/10/1.html\">2020年10月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/9/1.html\">2020年9月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2020/8/1.html\">2020年8月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/7/1.html\">2020年7月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/6/1.html\">2020年6月</A><SPAN class='label label-default'>719篇</SPAN></LI><LI><a href=\"/dayProxy/2020/5/1.html\">2020年5月</A><SPAN class='label label-default'>718篇</SPAN></LI><LI><a href=\"/dayProxy/2020/4/1.html\">2020年4月</A><SPAN class='label label-default'>715篇</SPAN></LI><LI><a href=\"/dayProxy/2020/3/1.html\">2020年3月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2020/2/1.html\">2020年2月</A><SPAN class='label label-default'>695篇</SPAN></LI><LI><a href=\"/dayProxy/2020/1/1.html\">2020年1月</A><SPAN class='label label-default'>725篇</SPAN></LI><LI><a href=\"/dayProxy/2019/12/1.html\">2019年12月</A><SPAN class='label label-default'>727篇</SPAN></LI><LI><a href=\"/dayProxy/2019/11/1.html\">2019年11月</A><SPAN class='label label-default'>705篇</SPAN></LI><LI><a href=\"/dayProxy/2019/10/1.html\">2019年10月</A><SPAN class='label label-default'>732篇</SPAN></LI><LI><a href=\"/dayProxy/2019/9/1.html\">2019年9月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2019/8/1.html\">2019年8月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2019/7/1.html\">2019年7月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2019/6/1.html\">2019年6月</A><SPAN class='label label-default'>714篇</SPAN></LI><LI><a href=\"/dayProxy/2019/5/1.html\">2019年5月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2019/4/1.html\">2019年4月</A><SPAN class='label label-default'>719篇</SPAN></LI><LI><a href=\"/dayProxy/2019/3/1.html\">2019年3月</A><SPAN class='label label-default'>735篇</SPAN></LI><LI><a href=\"/dayProxy/2019/2/1.html\">2019年2月</A><SPAN class='label label-default'>672篇</SPAN></LI><LI><a href=\"/dayProxy/2019/1/1.html\">2019年1月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2018/12/1.html\">2018年12月</A><SPAN class='label label-default'>740篇</SPAN></LI><LI><a href=\"/dayProxy/2018/11/1.html\">2018年11月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2018/10/1.html\">2018年10月</A><SPAN class='label label-default'>731篇</SPAN></LI><LI><a href=\"/dayProxy/2018/9/1.html\">2018年9月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2018/8/1.html\">2018年8月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2018/7/1.html\">2018年7月</A><SPAN class='label label-default'>740篇</SPAN></LI><LI><a href=\"/dayProxy/2018/6/1.html\">2018年6月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2018/5/1.html\">2018年5月</A><SPAN class='label label-default'>743篇</SPAN></LI><LI><a href=\"/dayProxy/2018/4/1.html\">2018年4月</A><SPAN class='label label-default'>713篇</SPAN></LI><LI><a href=\"/dayProxy/2018/3/1.html\">2018年3月</A><SPAN class='label label-default'>726篇</SPAN></LI><LI><a href=\"/dayProxy/2018/2/1.html\">2018年2月</A><SPAN class='label label-default'>672篇</SPAN></LI><LI><a href=\"/dayProxy/2018/1/1.html\">2018年1月</A><SPAN class='label label-default'>739篇</SPAN></LI><LI><a href=\"/dayProxy/2017/12/1.html\">2017年12月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2017/11/1.html\">2017年11月</A><SPAN class='label label-default'>720篇</SPAN></LI><LI><a href=\"/dayProxy/2017/10/1.html\">2017年10月</A><SPAN class='label label-default'>744篇</SPAN></LI><LI><a href=\"/dayProxy/2017/9/1.html\">2017年9月</A><SPAN class='label label-default'>290篇</SPAN></LI><LI><a href=\"/dayProxy/2017/8/1.html\">2017年8月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2017/7/1.html\">2017年7月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2017/6/1.html\">2017年6月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2017/5/1.html\">2017年5月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2017/4/1.html\">2017年4月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2017/3/1.html\">2017年3月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2017/2/1.html\">2017年2月</A><SPAN class='label label-default'>28篇</SPAN></LI><LI><a href=\"/dayProxy/2017/1/1.html\">2017年1月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/12/1.html\">2016年12月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/11/1.html\">2016年11月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2016/10/1.html\">2016年10月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/9/1.html\">2016年9月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2016/8/1.html\">2016年8月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/7/1.html\">2016年7月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/6/1.html\">2016年6月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2016/5/1.html\">2016年5月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/4/1.html\">2016年4月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2016/3/1.html\">2016年3月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2016/2/1.html\">2016年2月</A><SPAN class='label label-default'>29篇</SPAN></LI><LI><a href=\"/dayProxy/2016/1/1.html\">2016年1月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2015/12/1.html\">2015年12月</A><SPAN class='label label-default'>31篇</SPAN></LI><LI><a href=\"/dayProxy/2015/11/1.html\">2015年11月</A><SPAN class='label label-default'>30篇</SPAN></LI><LI><a href=\"/dayProxy/2015/10/1.html\">2015年10月</A><SPAN class='label label-default'>7篇</SPAN></LI></UL></DIV>        </DIV>\t\t\t<DIV class=\"thread_posts_list\"><DIV class=\"threadblock_list\" id=\"J_posts_list\"><div class=\"alert alert-info\" role=\"alert\">2022年8月23日14时 国内最新免费HTTP代理IP</DIV><div class='alert fade in alert-warning' style=\"padding:3px; padding-left:8px;font-size:12px;margin-top:10px;margin-bottom:10px;width:500px\"><div style=\"float:left;padding-right:40px;margin-top:6px;\">发布时间：2022/8/23 14:21:29 &nbsp;&nbsp;&nbsp;发布者：站大爷</div> <div class=\"bdsharebuttonbox\" style=\"\"><a href=\"#\" class=\"bds_more\" data-cmd=\"more\"></a><a href=\"#\" class=\"bds_qzone\" data-cmd=\"qzone\" title=\"分享到QQ空间\"></a><a href=\"#\" class=\"bds_tsina\" data-cmd=\"tsina\" title=\"分享到新浪微博\"></a><a href=\"#\" class=\"bds_tqq\" data-cmd=\"tqq\" title=\"分享到腾讯微博\"></a><a href=\"#\" class=\"bds_renren\" data-cmd=\"renren\" title=\"分享到人人网\"></a><a href=\"#\" class=\"bds_weixin\" data-cmd=\"weixin\" title=\"分享到微信\"></a></div></div><div class=\"vpanel\"><img src='/img/share.gif'></div><div class='alert fade in alert-warning' style=\"padding:8px; padding-left:8px;font-size:12px;margin-top:10px;margin-bottom:10px;\">免费代理IP由第三方平台提供，质量较低，仅供开发者学习用。若需要高质量代理IP，请购买本站的 <a href=\"/ShortProxy.html\">短效优质代理</a> 或 <a href=\"/PrivateProxy.html\">长效优质代理</a> 。</div><script>window._bd_share_config={\"common\":{\"bdSnsKey\":{},\"bdText\":\"\",\"bdMini\":\"2\",\"bdPic\":\"\",\"bdStyle\":\"0\",\"bdSize\":\"16\"},\"share\":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>\t<div class=\"cont\"><div class=\"page\">&nbsp;本次发布共<font color='orange'><b>34</b></font>个代理IP，每页20个&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;1&nbsp;&nbsp;<a class=\"layui-btn layui-btn-xs\" title=\"跳到第2页\" href=\"/dayProxy/ip/333745/2.html\">2</a><a class=\"layui-btn layui-btn-xs\" title=\"下一页\" href=\"/dayProxy/ip/333745/2.html\">下一页</a>\t\t\t\t<a class=\"layui-btn layui-btn-xs\" target=_blank style=\"background-color: #e1945d;\" title=\"在线批量验证Http代理IP\" href=\"/ip/CheckHttp/\">在线批量验证Http代理IP</a></div>\t<table id=\"ipc\" style=\"margin-bottom:10px;\">\t<thead><th>IP地址</th><th>端口</th><th>协议</th><th>类型</th><th>地理位置</th> </tr>\t\t</thead>\t\t<tbody>\t\t<tr>\t<td>47.108.137.58&nbsp;&nbsp;<a href=\"/ip/location/47.108.137.58\" title=\"在百度地图里查看47.108.137.58的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>33080&nbsp;&nbsp;<a href=\"/ip/CheckHttp/47.108.137.58:33080\" title=\"立即检测该代理IP_47.108.137.58:33080是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>高匿</td>\t<td>浙江省杭州市 阿里云</td>\t</tr>\t\t<tr>\t<td>39.130.150.44&nbsp;&nbsp;<a href=\"/ip/location/39.130.150.44\" title=\"在百度地图里查看39.130.150.44的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>80&nbsp;&nbsp;<a href=\"/ip/CheckHttp/39.130.150.44:80\" title=\"立即检测该代理IP_39.130.150.44:80是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>透明</td>\t<td>云南省 移动</td>\t</tr>\t\t<tr>\t<td>218.244.147.59&nbsp;&nbsp;<a href=\"/ip/location/218.244.147.59\" title=\"在百度地图里查看218.244.147.59的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>3128&nbsp;&nbsp;<a href=\"/ip/CheckHttp/218.244.147.59:3128\" title=\"立即检测该代理IP_218.244.147.59:3128是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>透明</td>\t<td>北京市 鹏博士中关村IDC(万网租用)</td>\t</tr>\t\t<tr>\t<td>183.224.41.199&nbsp;&nbsp;<a href=\"/ip/location/183.224.41.199\" title=\"在百度地图里查看183.224.41.199的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>80&nbsp;&nbsp;<a href=\"/ip/CheckHttp/183.224.41.199:80\" title=\"立即检测该代理IP_183.224.41.199:80是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>透明</td>\t<td>云南省昆明市 移动</td>\t</tr>\t\t<tr>\t<td>106.75.171.235&nbsp;&nbsp;<a href=\"/ip/location/106.75.171.235\" title=\"在百度地图里查看106.75.171.235的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>8080&nbsp;&nbsp;<a href=\"/ip/CheckHttp/106.75.171.235:8080\" title=\"立即检测该代理IP_106.75.171.235:8080是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>高匿</td>\t<td>广东省广州市 优刻得信息科技有限公司(UCloud)BGP数据中心</td>\t</tr>\t\t<tr>\t<td>39.130.150.42&nbsp;&nbsp;<a href=\"/ip/location/39.130.150.42\" title=\"在百度地图里查看39.130.150.42的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>80&nbsp;&nbsp;<a href=\"/ip/CheckHttp/39.130.150.42:80\" title=\"立即检测该代理IP_39.130.150.42:80是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>透明</td>\t<td>云南省 移动</td>\t</tr>\t\t<tr>\t<td>120.237.144.77&nbsp;&nbsp;<a href=\"/ip/location/120.237.144.77\" title=\"在百度地图里查看120.237.144.77的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/120.237.144.77:9091\" title=\"立即检测该代理IP_120.237.144.77:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>广东省 移动</td>\t</tr>\t\t<tr>\t<td>183.250.163.175&nbsp;&nbsp;<a href=\"/ip/location/183.250.163.175\" title=\"在百度地图里查看183.250.163.175的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/183.250.163.175:9091\" title=\"立即检测该代理IP_183.250.163.175:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>福建省厦门市 移动</td>\t</tr>\t\t<tr>\t<td>182.61.201.201&nbsp;&nbsp;<a href=\"/ip/location/182.61.201.201\" title=\"在百度地图里查看182.61.201.201的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>80&nbsp;&nbsp;<a href=\"/ip/CheckHttp/182.61.201.201:80\" title=\"立即检测该代理IP_182.61.201.201:80是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>北京市 驰骏网络科技有限公司</td>\t</tr>\t\t<tr>\t<td>117.160.132.37&nbsp;&nbsp;<a href=\"/ip/location/117.160.132.37\" title=\"在百度地图里查看117.160.132.37的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/117.160.132.37:9091\" title=\"立即检测该代理IP_117.160.132.37:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>河南省洛阳市 移动</td>\t</tr>\t\t<tr>\t<td>59.48.200.158&nbsp;&nbsp;<a href=\"/ip/location/59.48.200.158\" title=\"在百度地图里查看59.48.200.158的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/59.48.200.158:9091\" title=\"立即检测该代理IP_59.48.200.158:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>山西省朔州市</td>\t</tr>\t\t<tr>\t<td>211.142.96.250&nbsp;&nbsp;<a href=\"/ip/location/211.142.96.250\" title=\"在百度地图里查看211.142.96.250的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/211.142.96.250:9091\" title=\"立即检测该代理IP_211.142.96.250:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>河南省焦作市 移动</td>\t</tr>\t\t<tr>\t<td>112.25.236.167&nbsp;&nbsp;<a href=\"/ip/location/112.25.236.167\" title=\"在百度地图里查看112.25.236.167的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/112.25.236.167:9091\" title=\"立即检测该代理IP_112.25.236.167:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>江苏省苏州市 移动</td>\t</tr>\t\t<tr>\t<td>58.220.95.32&nbsp;&nbsp;<a href=\"/ip/location/58.220.95.32\" title=\"在百度地图里查看58.220.95.32的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>10174&nbsp;&nbsp;<a href=\"/ip/CheckHttp/58.220.95.32:10174\" title=\"立即检测该代理IP_58.220.95.32:10174是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>未知</td>\t<td>江苏省扬州市 电信</td>\t</tr>\t\t<tr>\t<td>218.75.38.154&nbsp;&nbsp;<a href=\"/ip/location/218.75.38.154\" title=\"在百度地图里查看218.75.38.154的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/218.75.38.154:9091\" title=\"立即检测该代理IP_218.75.38.154:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>浙江省杭州市 电信</td>\t</tr>\t\t<tr>\t<td>112.5.56.2&nbsp;&nbsp;<a href=\"/ip/location/112.5.56.2\" title=\"在百度地图里查看112.5.56.2的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>9091&nbsp;&nbsp;<a href=\"/ip/CheckHttp/112.5.56.2:9091\" title=\"立即检测该代理IP_112.5.56.2:9091是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>普匿</td>\t<td>福建省泉州市 移动</td>\t</tr>\t\t<tr>\t<td>58.220.95.34&nbsp;&nbsp;<a href=\"/ip/location/58.220.95.34\" title=\"在百度地图里查看58.220.95.34的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>10174&nbsp;&nbsp;<a href=\"/ip/CheckHttp/58.220.95.34:10174\" title=\"立即检测该代理IP_58.220.95.34:10174是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>未知</td>\t<td>江苏省扬州市 电信</td>\t</tr>\t\t<tr>\t<td>58.220.95.30&nbsp;&nbsp;<a href=\"/ip/location/58.220.95.30\" title=\"在百度地图里查看58.220.95.30的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>10174&nbsp;&nbsp;<a href=\"/ip/CheckHttp/58.220.95.30:10174\" title=\"立即检测该代理IP_58.220.95.30:10174是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>未知</td>\t<td>江苏省扬州市 电信</td>\t</tr>\t\t<tr>\t<td>218.202.7.125&nbsp;&nbsp;<a href=\"/ip/location/218.202.7.125\" title=\"在百度地图里查看218.202.7.125的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>80&nbsp;&nbsp;<a href=\"/ip/CheckHttp/218.202.7.125:80\" title=\"立即检测该代理IP_218.202.7.125:80是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>透明</td>\t<td>云南省昆明市 移动</td>\t</tr>\t\t<tr>\t<td>58.220.95.31&nbsp;&nbsp;<a href=\"/ip/location/58.220.95.31\" title=\"在百度地图里查看58.220.95.31的地理位置\"><i class=\"icon-pointer\"></i></a></td>\t<td>10174&nbsp;&nbsp;<a href=\"/ip/CheckHttp/58.220.95.31:10174\" title=\"立即检测该代理IP_58.220.95.31:10174是否可用\">检测</a></td>\t<td>HTTP</td>\t<td>未知</td>\t<td>江苏省扬州市 电信</td>\t</tr>\t\t</tbody></table>\t<div class=\"page\">&nbsp;本次发布共<font color='orange'><b>34</b></font>个代理IP，每页20个&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;1&nbsp;&nbsp;<a class=\"layui-btn layui-btn-xs\" title=\"跳到第2页\" href=\"/dayProxy/ip/333745/2.html\">2</a><a class=\"layui-btn layui-btn-xs\" title=\"下一页\" href=\"/dayProxy/ip/333745/2.html\">下一页</a>\t\t\t\t<a class=\"layui-btn layui-btn-xs\" target=_blank style=\"background-color: #e1945d;\" title=\"在线批量验证Http代理IP\" href=\"/ip/CheckHttp/\">在线批量验证Http代理IP</a></div>\t\t<div class=\"alert alert-warning\" role=\"alert\" id=\"tj\">下一篇：<a href=\"/dayProxy/ip/333744.html\">2022年8月23日7时 国内最新免费HTTP代理IP</a></DIV>\t\t\t\t<div class=\"alert alert-warning\" role=\"alert\" id=\"tj\">\t分享10篇您可能感兴趣的代理IP知识文章\t<div style=\"margin-top:-18px; line-height:30px;\">\t<br>◆&nbsp;&nbsp;<a href=\"/know/s/5929.html\">选择代理IP的三大绝招都了解吗</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/5825.html\">如何拥有一个网络爬虫代理IP池</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/6794.html\">3分钟带你了解http代理IP的相关信息</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/4948.html\">前置反拦截，爬虫工作好帮手</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/7153.html\">免费代理IP千千万，能使用吗</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/6823.html\">为什么独享代理IP要比共享代理IP贵很多</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/6857.html\">不同的代理IP套餐价格为什么不同</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/6348.html\">有没有比较好的代理IP，免费代理IP太差劲了</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/6034.html\">浅析三种比较常见的代理服务器</a><br>◆&nbsp;&nbsp;<a href=\"/know/s/3849.html\">为什么要选择合租代理IP池</a></DIV>\t</div>\t</div>\t  </DIV>\t</DIV></DIV>\t\t\t\t</div></div><link href=\"/css/foot.css?v=1.25\" rel=\"stylesheet\" type=\"text/css\">  <!--免费试用-->  <div class=\"mbox mbox2 mt ov\">    <div class=\"title ov\">      <span>立即注册站大爷用户，免费试用全部产品</span>    </div>    <div class=\"title01 ov\">      <span>立即注册站大爷用户，免费试用全部产品</span>    </div>    <div class=\"sbox ov\">      <strong class=\"mHide\"><img src=\"/images/tel.jpg\" /></strong>      <p>        <input type=\"text\" placeholder=\"请输入您的手机号\" id=\"phoneNum\" />      </p>      <span><input type=\"button\" value=\"免费试用\" onclick=\"location.href='/Users/Reg.html?phone=' + $('#phoneNum').val();\" class=\"freeUse\" /></span>    </div>  </div>  <!--[if lt IE 9]>      <script type=\"text/javascript\">        $(document).ready(function () {          // val()无参数是取值，有参数是设置          // 为了更好的兼容性，将下面的val()设置与input中的placeholder的值设置相同          $(\".sbox p input\").val(\"请输入您的手机号\");          textFill($(\"input.text1\"));        });        function textFill(input) {          var originalvalue = input.val();          input.focus(function () {            if ($.trim(input.val()) == originalvalue) {              input.val(\"\");            }          });          input.blur(function () {            if ($.trim(input.val()) == \"\") {              input.val(originalvalue);            }          });        }      </script>    <![endif]--><!--foot-->  <div class=\"footer ov\">    <div class=\"foot ov\">      <div class=\"fl\">        <div class=\"logo\">          <img src=\"/images/logo.png\" />        </div>        <div class=\"contact\">          <p class=\"title\">快速咨询热线：</p>          <p class=\"telNum\">024-31823261</p>        </div>        <div class=\"xw\">          <ul>            <li>              <a href=\"#\" target=\"_blank\" style=\"display:none\">                <img src=\"/images/m-wb.png\" />              </a>            </li>            <li class=\"on\">              <a>                <img src=\"/images/m-wx.png\" />              </a>              <p>                <img src=\"/images/wx.jpg\" />              </p>            </li>            <li>              <a id=\"qdqq2\"                 title=\"联系企业QQ客服进行咨询\">                <img src=\"/images/m-qq.png\" />              </a>\t\t\t  <script id=\"qd23550872644e67b33ea1260787e910e1e106b89bce\" src=\"https://wp.qiye.qq.com/qidian/2355087264/4e67b33ea1260787e910e1e106b89bce\" charset=\"utf-8\" async defer></script>            </li>          </ul>        </div>        <ul class=\"lx\">          <li>\t\t    <strong>              <img src=\"/images/f02.png\">            </strong>            <p>国家高新技术企业证书编号：GR202021001869</p>          </li>                    <li>            <strong>              <img src=\"/images/f03.png\">             </strong>            <p>增值电信经营许可证： <a href=\"http://beian.miit.gov.cn/\" target=\"_blank\" style=\"color:#fff\" >辽B2-20180026</a></p>          </li>\t\t  <li>            <strong>              <img src=\"/images/f03.png\">             </strong>            <p>互联网虚拟专用网业务许可证：B1-20181940</p>          </li>          <li>\t\t\t <strong>              <img src=\"/images/f04.png\">             </strong>            <p>公司地址：沈阳市浑南区上深沟村860-2号沈阳国际软件园F7座</p>          </li>\t\t                   </ul>      </div>      <dl class=\"nav fl\">        <div class=\"description\">          <div class=\"item\">            <p>产品</p>            <ul>              <li>                <a href=\"/ShortProxy.html\" style=\"color: white\">短效优质代理</a>              </li>              <li>                <a href=\"/PrivateProxy.html\" style=\"color: white\">长效优质代理</a>              </li>              <li>                <a href=\"/ExclusiveProxy.html\" style=\"color: white\">独享IP池</a>              </li>\t\t\t  <li>                <a href=\"/CotenancyProxy.html\" style=\"color: white\">合租IP池</a>              </li>\t\t\t  <li>                <a href=\"/ShortS5Proxy.html\" style=\"color: white\">短效Socks5代理</a>              </li>              <li><a href=\"/PrivateS5Proxy.html\" style=\"color: white\">长效Socks5代理</a></li>            </ul>          </div>          <div class=\"item\">            <p>资源</p>            <ul>\t\t\t\t\t\t\t                                    <li>                <a href=\"/know.html\" style=\"color: white\">代理IP知识</a>              </li>              <li>                <a href=\"/daili.html\" style=\"color: white\">代理IP问答</a>              </li>\t\t\t\t<li><a href=\"/Ask\" style=\"color: white\">互助社区</a></li>              <li>&nbsp;</li>            </ul>          </div>          <div class=\"item\">            <p>文档</p>            <ul>              <li><a href=\"/Help.html\" style=\"color: white\">帮助文档</a></li>              <li><a href=\"/doc/api/ShortProxy/\" style=\"color: white\">API文档</a></li>              <li><a href=\"/About.html\" style=\"color: white\">关于我们</a></li>\t\t\t  <li><a href=\"/map/sitemap.xml\" style=\"color: white\">网站地图</a></li>              <li>&nbsp;</li>            </ul>          </div>          <div class=\"item\">            <p>工具</p>            <ul>              <li><a href=\"/ip/whois/\">IP-Whois查询</a></li>              <li><a href=\"/ip/dns/\">各省市DNS地址</a></li>              <li><a href=\"/ip/allocate/\">全球IP段分配</a></li>\t\t\t  <li><a href=\"/ip/CheckHttp/\">Http代理IP检测</a></li>\t\t\t  <li><a href=\"/ip/CheckSocks5/\">Socks5代理IP检测</a></li>\t\t\t  <li><a href=\"/ip/location/\">IP地址所在地查询</a></li>              <li>&nbsp;</li>            </ul>          </div>        </div>        <dd style=\"margin-top: 60px\" class=\"qqcontact\">          <p style=\"margin-left:0px !important;\">           <a href=\"javascript:void(0)\" id=\"qq_kfzx\">\t\t\t<span class=\"qqunimg\"></span>\t\t\t<span class=\"qquntext\">              在线咨询\t\t\t </span>            </a>          </p>          <p>            <a href=\"javascript:void(0)\" id=\"qq_tsjy\">\t\t\t<span class=\"qqunimg\"></span>\t\t\t<span class=\"qquntext\">              投诉建议\t\t\t </span>            </a>          </p>          <p>            <a href=\"https://qm.qq.com/cgi-bin/qm/qr?k=_Fe3EAOHYzE18YWnWb2I4UhISN6XwuEJ&jump_from=webapi\" target=\"_blank\">\t\t\t<span class=\"qqunimg\"></span>\t\t\t<span class=\"qquntext\">              站大爷交流一群\t\t\t </span>            </a>          </p>          <p>            <a href=\"https://qm.qq.com/cgi-bin/qm/qr?k=ZOnPjVnrdz11KFdZPwd_E6LmWNUwdF05&jump_from=webapi\" target=\"_blank\">\t\t\t<span class=\"qqunimg\"></span>\t\t\t<span class=\"qquntext\">              站大爷交流二群\t\t\t  </span>            </a>          </p>          <p>           <a href=\"https://qm.qq.com/cgi-bin/qm/qr?k=CD1YVjSeS0anxrQIDbmw8IE2ZK6beKQV&jump_from=webapi\" target=\"_blank\">\t\t   <span class=\"qqunimg\"></span>\t\t\t<span class=\"qquntext\">              Python爬虫学习群\t\t\t  </span>            </a>          </p>        </dd>\t\t\t<script id=\"qd235508726496b615dc6ab8204024be7d44be2a59d0\" src=\"https://wp.qiye.qq.com/qidian/2355087264/96b615dc6ab8204024be7d44be2a59d0\" charset=\"utf-8\" async defer></script>\t\t<script id=\"qd23550872646aa8f8d27ad728a8d3c71875221a8b0a\" src=\"https://wp.qiye.qq.com/qidian/2355087264/6aa8f8d27ad728a8d3c71875221a8b0a\" charset=\"utf-8\" async defer></script>\t  </dl>\t  <span class=\"footsm\">\t  声明：本站不搜集数据不存储数据，也不买卖数据，所有资源仅用作数据传输通道。禁止利用本平台资源从事任何违反本国（地区）法律法规的活动，用户所有操作行为均有日志存档并保留6个月。\t  </span>      <div class=\"bottom fl\" style=\"margin-bottom:40px;\">        <span>站大爷 - 专业的大数据基础服务平台</span>        <span>Copyright@2012-2022 <script>window[\"\\x64\\x6f\\x63\\x75\\x6d\\x65\\x6e\\x74\"][\"\\x77\\x72\\x69\\x74\\x65\"](\"\\u6c88\\u9633\\u5c0f\\u571f\\u79d1\\u6280\\u6709\\u9650\\u516c\\u53f8\");</script> | <a href=\"http://beian.miit.gov.cn/\" target=\"_blank\" style=\"color:#fff\" >辽B2-20180026</a></span>      </div>    </div>  </div>  <script src=\"/cloud/assets/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script><link href=\"/cloud/assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"><link id=\"style_components\" href=\"/cloud/assets/theme/assets/global/css/components-rounded.css\" rel=\"stylesheet\" type=\"text/css\">\t\t\t<div class=\"modal fade \" id=\"gdForm\" tabindex=\"-1\"  role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" style=\"margin-top:50px;\">        <div class=\"modal-dialog\" role=\"document\" style=\"width:780px;\">            <div class=\"modal-content\">                <div class=\"modal-header\">                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"></button>                    <h4 class=\"modal-title\">产品导购</h4>                </div>                <div id=\"relate-modal-body\" class=\"modal-body\" style=\"height:402px;\" >                    <iframe id=\"gdURL\" name=\"gdURL\" border=\"0\" vspace=\"0\" hspace=\"0\" marginwidth=\"0\" marginheight=\"0\" framespacing=\"0\" frameborder=\"0\" scrolling=\"no\"  width=\"100%\" height=\"100%\" src=\"\"></iframe>                </div>                <div class=\"modal-footer\">\t            <button type=\"button\" name=\"smbtn\" id=\"smbtn\"  class=\"btn green-meadow\" onClick=\"CloseGd();\">关 闭</button>                </div>            </div>        </div>    </div>\t<div class=\"modal fade \" id=\"tuForm\" tabindex=\"-2\"  role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" style=\"margin-top:60px;\">        <div class=\"modal-dialog\" role=\"document\" style=\"width:540px;\">            <div class=\"modal-content\">                <div class=\"modal-header\">                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"></button>                    <h4 class=\"modal-title\">免费试用</h4>                </div>                <div id=\"relate-modal-body\" class=\"modal-body\" style=\"height:335px;\" >                    <iframe id=\"tuURL\" name=\"tuURL\" border=\"0\" vspace=\"0\" hspace=\"0\" marginwidth=\"0\" marginheight=\"0\" framespacing=\"0\" frameborder=\"0\" scrolling=\"no\"  width=\"100%\" height=\"100%\" src=\"\"></iframe>                </div>                <div class=\"modal-footer\">\t            <button type=\"button\" name=\"smbtn\" id=\"smbtn2\"  class=\"btn green-meadow\" onClick=\"CloseTu();\">关 闭</button>                </div>            </div>        </div>    </div>\t\t\t\t<div style=\"display:none\">\t\t\t\t<script charset=\"UTF-8\" id=\"LA_COLLECT\" src=\"//sdk.51.la/js-sdk-pro.min.js\"></script>\t\t<script>LA.init({id: \"1vh212XjimTsXGR8\",ck: \"1vh212XjimTsXGR8\"})</script>\t\t<script>\t\tvar _hmt = _hmt || [];\t\t(function() {\t\t  var hm = document.createElement(\"script\");\t\t  hm.src = \"https://hm.baidu.com/hm.js?80f407a85cf0bc32ab5f9cc91c15f88b\";\t\t  var s = document.getElementsByTagName(\"script\")[0]; \t\t  s.parentNode.insertBefore(hm, s);\t\t})();\t\t</script>\t\t</div>\t\t<script>\t\t(function(){\t\t\tvar bp = document.createElement('script');\t\t\tvar curProtocol = window.location.protocol.split(':')[0];\t\t\tif (curProtocol === 'https') {\t\t\t\tbp.src = 'https://zz.bdstatic.com/linksubmit/push.js';\t\t\t}\t\t\telse {\t\t\t\tbp.src = 'http://push.zhanzhang.baidu.com/push.js';\t\t\t}\t\t\tvar s = document.getElementsByTagName(\"script\")[0];\t\t\ts.parentNode.insertBefore(bp, s);\t\t})();\t\tvar kfqq = \"1806511756\";\t\t</script>\t\t<div id=\"top\"></div>\t\t<script type=\"text/javascript\" src=\"/js/zzsc.js\"></script><style>.botNotice{\twidth:100%;\tbackground-color: #faf7b9f0;    position: fixed;    bottom: 0px;    z-index: 999999;    padding: 10px;    color: #bc4d2ceb;\tbox-shadow: 0px 2px 5px #766b6b;\tfont-size:16px;\theight:20px;\tline-height:20px;\twhite-space:nowrap;}#botNoticeBody{\tposition:absolute;\ttop:3; \theight:20px;\tline-height:20px;}</style><div class=\"botNotice\" id=\"botNotice\"><div id=\"botNoticeBody\">敬告：本站严禁任何单位或个人利用本站资源切换IP属地在微博、抖音、今日头条等互联网平台上发布谣言、诋毁攻击、煽动群众等非法内容。</div> </div><script>$.fn.textScroll=function(){ \tvar p = $(this), \t\tc = p.children(),\t\tspeed = 3000;// 值越大，速度越小 \tvar cw = c.width(),\t\tpw = p.width(); \tvar t = (cw / 100) * speed; \tc.css({ left: pw/3 }); \t\tvar totaltime = t;\t\tfunction getSpeed(){\t\tvar remanentWidth = c.position().left - (-cw);\t\tvar totalWidth = pw/3 - (-cw);\t\tvar rSpeed = totaltime * remanentWidth / totalWidth;\t\treturn parseInt(rSpeed);\t}\t\tfunction ani(){  \t\tc.stop(true, false).animate({left: -cw}, getSpeed(), \"linear\", function (){ \t\t\tc.css({ left: pw });  \t\t\tani(); \t\t}); \t}\t  \tp.on({ \t\tmouseenter: function () { \t\t\tc.stop(true, false); \t\t}, \t\tmouseleave: function () { \t\t\tani(); \t\t} \t});  \tani(); }$(\"#botNotice\").textScroll();</script></body></html>";
        Document doc = Jsoup.parse(pageContent);
        Elements select = doc.getElementById("ipc").select("tbody");
        Elements elements = select.select("tr");
        for (Element element : elements) {
            Elements children = element.children();
            if (children.size() == 5) {
                Element elementIp = children.get(0);
                String ip = elementIp.text().substring(0, elementIp.text().indexOf(" ")==-1?elementIp.text().length():elementIp.text().indexOf(" "));
                Element elementPort = children.get(1);
                String port = elementPort.text().substring(0, elementPort.text().indexOf(" ")==-1?elementPort.text().length():elementPort.text().indexOf(" "));
                proxyList.add(ip + ":" + port);
            }
        }

        List<String> resultList = new ArrayList<>();
        for (String proxy : proxyList) {
            try {
                HttpUtils.doGet(
                        HttpUtils.create()
                                .addHostPort("https://icanhazip.com")
//                                .addBodyMapAll(body)
                                .addHeaderAll(header)
                                .addProxy(proxy)
                                .build()
                );
                resultList.add(proxy);
            } catch (Exception e) {
                //e.printStackTrace();
                System.out.println("代理用不成！！！！！！！！！");
            }
        }
        String join = Joiner.on(";").join(resultList);
        System.out.println(join);
    }

    @Test
    public void test(){

        for (int i = 0; i < 10; i++) {
            if(i < 3){
                continue;
            }
            log.info("i : {}", i);
            break;
        }


    }

    @Test
    public void test1(){
        String s = "16 25 08 17 31 38 32 32 39 30 30 31 36 30 36 00 00 00 00 00 00 AA 55 1C 12 03 16 0C 18 0C 0C 01 C8 00 7B 00 00 10 00 00 00 10 00 00 00 90 1E 00 00 90 1e 05 01 00 66 C9 C2 33 30 48 2D 33 00 00 00 00 00 00 00 00 00 12  ";
        System.out.println(s.replace(" ", "").length());
    }
}
