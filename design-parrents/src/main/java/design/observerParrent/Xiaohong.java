package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:47
 * @Versiion：1.0
 * @Desc:
 */

public class Xiaohong implements Observer{
    @Override
    public void response() {
        System.out.println("小红: 全体起立，我卢本伟没有作弊，哦 不对 老师好！");
    }
}
