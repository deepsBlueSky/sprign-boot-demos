package design.observerParrent;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:13
 * @Versiion：1.0
 * @Desc:
 */

public abstract class Person {

    protected static List<Observer> observers = new ArrayList<>();

    public void addUser(Observer user){
        observers.add(user);
    }

    public void removeUser(Observer user){
        observers.remove(user);
    }

    public abstract void comeClass();
}
