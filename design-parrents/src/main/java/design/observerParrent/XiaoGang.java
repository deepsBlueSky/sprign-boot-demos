package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:33
 * @Versiion：1.0
 * @Desc:
 */

public class XiaoGang implements Observer {
    @Override
    public void response() {
        System.out.println("小刚: 我就站着 能把我怎么样！！！");
    }
}
