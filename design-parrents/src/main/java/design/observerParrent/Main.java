package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:34
 * @Versiion：1.0
 * @Desc:
 */

public class Main {
    public static void main(String[] args) {

        Person person = new Teacher();
        Observer xiaoming = new Xiaoming();
        Observer xiaoGang = new XiaoGang();
        Observer xiaohong = new Xiaohong();
        person.addUser(xiaoming);
        person.addUser(xiaoGang);
        person.addUser(xiaohong);
        person.comeClass();

        person.removeUser(xiaohong);
        person.comeClass();
    }
}
