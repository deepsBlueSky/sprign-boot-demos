package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:13
 * @Versiion：1.0
 * @Desc:
 */

public interface Observer {

    void response();

}
