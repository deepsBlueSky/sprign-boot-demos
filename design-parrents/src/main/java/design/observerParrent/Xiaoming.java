package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:33
 * @Versiion：1.0
 * @Desc:
 */

public class Xiaoming implements Observer {
    @Override
    public void response() {
        System.out.println("小明: 卧槽 赶紧坐好！");
    }
}
