package design.observerParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/20/0020 22:28
 * @Versiion：1.0
 * @Desc:
 */

public class Teacher extends Person {

    @Override
    public void comeClass() {
        System.out.println("老师进教室了");
        //通知
        for (Observer observer : observers) {
            observer.response();
        }
    }

}
