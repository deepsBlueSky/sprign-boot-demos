package design.proxyParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/22/0022 22:59
 * @Versiion：1.0
 * @Desc:
 */

public class Main {
    public static void main(String[] args) {
        //创建一个代理类 判断用户level  为空时 不会走到真正的业务
        AbstractPermission permission = new PermissionProxy();
        permission.modifyUserInfo();
        permission.viewNote();
        permission.publishNote();
        permission.modifyNote();

        System.out.println("=================================");
        //设置level会走到真正的业务
        permission.setLevel(1);
        permission.modifyUserInfo();
        permission.viewNote();
        permission.publishNote();
        permission.modifyNote();
    }
}
