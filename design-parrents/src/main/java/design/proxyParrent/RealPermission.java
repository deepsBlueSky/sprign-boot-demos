package design.proxyParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/22/0022 22:52
 * @Versiion：1.0
 * @Desc:
 */

public class RealPermission implements AbstractPermission{

    @Override
    public void modifyUserInfo() {
        System.out.println("修改用户信息！");
    }

    @Override
    public void viewNote() {
        System.out.println("viewNote");
    }

    @Override
    public void publishNote() {
        System.out.println("发布新帖子");
    }

    @Override
    public void modifyNote() {
        System.out.println("修改发布帖子内容");
    }

    @Override
    public void setLevel(int level) {
        System.out.println("设置级别");
    }

}
