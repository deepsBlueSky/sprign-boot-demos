package design.proxyParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/22/0022 22:40
 * @Versiion：1.0
 * @Desc:
 */

public interface AbstractPermission {

    void modifyUserInfo();

    void viewNote();

    void publishNote();

    void modifyNote();

    void setLevel(int level);

}
