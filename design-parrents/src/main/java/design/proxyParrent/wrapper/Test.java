package design.proxyParrent.wrapper;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 11:11
 **/

public class Test {
    public static void main(String[] args) {
        /***
         * 装饰器模式 和 代理模式很像 但是还是右区别的
         *
         * 代理模式是提供代理来控制原有对象的访问 代理对象作为用户和代理目标之间的中介
         *
         * 装饰器模式是在原来对象不变的基础上做了增强【好像代码模式也可以】
         */
        Component component = new DetailComponenet();
        Decorator decorator = new ConcreteDecorator(component);
        decorator.execute();
    }
}
