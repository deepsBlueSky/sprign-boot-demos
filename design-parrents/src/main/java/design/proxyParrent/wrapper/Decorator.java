package design.proxyParrent.wrapper;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 11:06
 **/

public abstract class Decorator extends Component{

    public Component component;
    public Decorator(Component component){
        this.component = component;
    }

    @Override
    public void execute(){
        component.execute();
    }

}
