package design.proxyParrent.wrapper;

/**
 * @program: SpringBootDemos
 * @description: 抽象组件
 * @author: Kangsen
 * @create: 2022-10-14 11:02
 **/

public abstract class Component {

     public abstract void execute();

}
