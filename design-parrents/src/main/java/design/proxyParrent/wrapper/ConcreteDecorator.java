package design.proxyParrent.wrapper;

/**
 * @program: SpringBootDemos
 * @description: 具体装饰器
 * @author: Kangsen
 * @create: 2022-10-14 11:09
 **/

public class ConcreteDecorator extends Decorator{
    public ConcreteDecorator(Component component) {
        super(component);
    }

    public void  before(){
        System.out.println("ConcreteDecorator 前置操作");
    }

    public void after(){
        System.out.println("ConcreteDecorator 后置操作");
    }

    @Override
    public void execute(){
        before();
        component.execute();
        after();
    }
}
