package design.proxyParrent.wrapper;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 11:05
 **/

public class DetailComponenet extends Component{
    @Override
    public void execute() {
        System.out.println("具体执行业务逻辑");
    }
}
