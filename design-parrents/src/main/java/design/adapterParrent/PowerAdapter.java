package design.adapterParrent;

/**
 * @program: SpringBootDemos
 * @description: 电压适配
 * @author: Kangsen
 * @create: 2022-10-14 09:29
 **/

public class PowerAdapter extends Ac220 implements Dc5{

    @Override
    public int outputDc5() {
        int outputAc220 = super.outputAc220();
        int trans = outputAc220 / 44;
        System.out.println("adapter 输入"+ outputAc220 +"输出 "+ trans + "v");
        return trans;
    }
}
