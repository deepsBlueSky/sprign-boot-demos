package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;
import design.adapterParrent.loginV1.LoginAdapter;
import org.apache.commons.lang3.StringUtils;

/**
 * @program: SpringBootDemos
 * @description: 抽象类
 * @author: Kangsen
 * @create: 2022-10-14 10:22
 **/

public abstract class AbstractAdapter extends LoginAdapter implements ILoginAdapter {

    protected ResultMsg loginForRegist(String username,String password){
        if(StringUtils.isBlank(password)){
            //三方登录
            password = "THIRD_EMPTY";
        }
        super.register(username, password);
        return super.login(username, password);
    }

}
