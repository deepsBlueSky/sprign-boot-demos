package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: 搞个适配器的接口
 * @author: Kangsen
 * @create: 2022-10-14 10:21
 **/

public interface ILoginAdapter {

    boolean support(Object object);

    ResultMsg login(String id,Object object);
}
