package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: 微信登录适配
 * @author: Kangsen
 * @create: 2022-10-14 10:32
 **/

public class LoginForWechatAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object object) {
        return object instanceof LoginForWechatAdapter;
    }

    @Override
    public ResultMsg login(String id, Object object) {
        return support(object)?super.loginForRegist(id,null):null;
    }
}
