package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;
import design.adapterParrent.loginV1.IPassportForThird;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 10:33
 **/

public class PassportForThirdAdapter implements IPassportForThird {
    /**
     * QQ登录
     *
     * @param openId
     * @return
     */
    @Override
    public ResultMsg loginForQQ(String openId) {
        return processLogin(openId,LoginForQQAdapter.class);
    }

    /**
     * 微信登录
     *
     * @param openId
     * @return
     */
    @Override
    public ResultMsg loginForWechat(String openId) {
        return processLogin(openId,LoginForWechatAdapter.class);
    }

    /**
     * token登录
     *
     * @param token
     * @return
     */
    @Override
    public ResultMsg loginForToken(String token) {
        return processLogin(token,LoginForTokenAdapter.class);
    }

    /**
     * 电话号码登录
     *
     * @param phone
     * @param code
     * @return
     */
    @Override
    public ResultMsg loginForTel(String phone, String code) {
        return processLogin(phone,LoginForTelAdapter.class);
    }

    private ResultMsg processLogin(String id,Class<? extends ILoginAdapter> clazz){
        try {
            ILoginAdapter iLoginAdapter = clazz.newInstance();
            if (iLoginAdapter.support(iLoginAdapter)) {
                return iLoginAdapter.login(id,iLoginAdapter);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
