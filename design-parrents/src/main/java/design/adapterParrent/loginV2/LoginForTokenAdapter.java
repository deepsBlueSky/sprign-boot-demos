package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: token 登录适配
 * @author: Kangsen
 * @create: 2022-10-14 10:31
 **/

public class LoginForTokenAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object object) {
        return object instanceof LoginForTokenAdapter;
    }

    @Override
    public ResultMsg login(String id, Object object) {
        return support(object)?super.loginForRegist(id,null):null;
    }
}
