package design.adapterParrent.loginV2;

import design.adapterParrent.loginV1.IPassportForThird;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 10:40
 **/

public class Test {
    public static void main(String[] args) {
        IPassportForThird passportForThird = new PassportForThirdAdapter();
        passportForThird.loginForQQ("1212124");

    }
}
