package design.adapterParrent.loginV2;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 10:29
 **/

public class LoginForTelAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object object) {
        return object instanceof LoginForTelAdapter;
    }

    @Override
    public ResultMsg login(String id, Object object) {
        return support(object)?super.loginForRegist(id,null):null;
    }
}
