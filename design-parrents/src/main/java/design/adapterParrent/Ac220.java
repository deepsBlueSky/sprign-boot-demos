package design.adapterParrent;

/**
 * @program: SpringBootDemos
 * @description: 220V交流电压
 * @author: Kangsen
 * @create: 2022-10-14 09:28
 **/

public class Ac220 {

    public int outputAc220(){
        int output = 220;
        System.out.println("直流输出" + output + "V电压");
        return output;
    }

}
