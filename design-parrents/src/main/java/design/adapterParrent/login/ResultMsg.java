package design.adapterParrent.login;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: SpringBootDemos
 * @description: 统一返回类
 * @author: Kangsen
 * @create: 2022-10-14 09:46
 **/
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ResultMsg {
    private int code;
    private String msg;
    private Object data;
}
