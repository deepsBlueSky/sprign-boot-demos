package design.adapterParrent;

/**
 * @program: SpringBootDemos
 * @description: 直流5v
 * @author: Kangsen
 * @create: 2022-10-14 09:29
 **/

public interface Dc5 {

    int outputDc5();

}
