package design.adapterParrent.loginV1;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: 三方登录接口
 * @author: Kangsen
 * @create: 2022-10-14 10:04
 **/

public interface IPassportForThird {

    /**
     * QQ登录
     * @param openId
     * @return
     */
    ResultMsg loginForQQ(String openId);

    /**
     * 微信登录
     * @param openId
     * @return
     */
    ResultMsg loginForWechat(String openId);

    /**
     * token登录
     * @param token
     * @return
     */
    ResultMsg loginForToken(String token);

    /**
     * 电话号码登录
     * @param phone
     * @param code
     * @return
     */
    ResultMsg loginForTel(String phone,String code);
}
