package design.adapterParrent.loginV1;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: 老系统的登录业务流程
 * @author: Kangsen
 * @create: 2022-10-14 09:42
 **/
public class OldLoginService {

    public ResultMsg register(String username, String password){
        System.out.println("用户注册 用户名:"+username+"  密码:"+password);
        return ResultMsg.builder().code(200).msg("注册成功").build();
    }


    public ResultMsg login(String username,String password){
        System.out.println("用户登录 用户名:"+username+"  密码:"+password);
        return ResultMsg.builder().code(200).msg("登录成功").build();
    }
}
