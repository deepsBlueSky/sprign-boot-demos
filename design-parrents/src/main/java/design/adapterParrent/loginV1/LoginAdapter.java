package design.adapterParrent.loginV1;

import design.adapterParrent.login.ResultMsg;

/**
 * @program: SpringBootDemos
 * @description: 匹配登录适配器
 * @author: Kangsen
 * @create: 2022-10-14 10:06
 **/

public class LoginAdapter extends OldLoginService implements IPassportForThird{
    /**
     * QQ登录
     *
     * @param openId
     * @return
     */
    @Override
    public ResultMsg loginForQQ(String openId) {
        return loginForRegist(openId,null);
    }

    /**
     * 微信登录
     *
     * @param openId
     * @return
     */
    @Override
    public ResultMsg loginForWechat(String openId) {
        return loginForRegist(openId,null);
    }

    /**
     * token登录
     *
     * @param token
     * @return
     */
    @Override
    public ResultMsg loginForToken(String token) {
        return loginForRegist(token,null);
    }

    /**
     * 电话号码登录
     *
     * @param phone
     * @param code
     * @return
     */
    @Override
    public ResultMsg loginForTel(String phone, String code) {
        return loginForRegist(phone,code);
    }

    private ResultMsg loginForRegist(String username,String password){
        if(null == password){
            password = "THIRD_EMPTY";
        }
        super.register(username,password);
        return super.login(username,password);
    }
}
