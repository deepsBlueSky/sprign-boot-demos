package design.adapterParrent;

import design.adapterParrent.loginV1.LoginAdapter;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-10-14 09:34
 **/
public class Test {
    public static void main(String[] args) {
        /*Dc5 dc5 = new PowerAdapter();
        System.out.println(dc5.outputDc5());*/

        LoginAdapter loginAdapter = new LoginAdapter();
        loginAdapter.loginForQQ("1111");
        loginAdapter.loginForTel("17729389902","1234");
        loginAdapter.loginForToken("dsdwewewqeqweqweqweqwe");
    }
}
