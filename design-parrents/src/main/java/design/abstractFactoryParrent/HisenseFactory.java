package design.abstractFactoryParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 22:10
 * @Versiion：1.0
 * @Desc:
 */

public class HisenseFactory implements EFactory{
    @Override
    public TV produceTv() {
        return new HisenseTV();
    }

    @Override
    public AirCondition produceAirCondition() {
        return new HisenseAirCondition();
    }
}
