package design.abstractFactoryParrent;

/**
 * @Author：Administrator
 * @Date：2022/9/19/0019 20:37
 * @Versiion：1.0
 */
public interface TV {

    void play();
}
