package design.abstractFactoryParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 21:36
 * @Versiion：1.0
 * @Desc:
 */

public class HisenseAirCondition implements AirCondition{
    @Override
    public void changeTemp() {
        System.out.println("Hisense 空调温度控制");
    }
}
