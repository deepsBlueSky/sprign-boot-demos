package design.abstractFactoryParrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:42
 * @Versiion：1.0
 * @Desc:
 */

public class Consumer {

    private String test;

    public void a(){
        b();
    }

    public void b(){
        System.out.println("method　ｂ");
    }

    static class ClassA{

        public void A1(){
            System.out.println("A1方法");
        }

        public static void A2(){
            System.out.println("A1方法");
        }

    }

    class ClassB extends ClassA{

        public void B1(){
            A1();

            A2();

            System.out.println("B1方法");
        }

    }


    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(2);
        head.next.next.next = new ListNode(2);
        System.out.println(isPalindrome(head));
    }

    static class ListNode{
        private ListNode next;
        private int val;
        public ListNode(int val) {
            this.val = val;
        }
    }
    public static boolean isPalindrome(ListNode head) {
        if(null == head){
            return false;
        }
        //定义栈结构
        Stack<Integer> stack = new Stack<>();
        //链表头数据入栈
        stack.push(head.val);

        List<Integer> list = new ArrayList<>();
        //链表头数据存入数组
        list.add(head.val);
        while (null != head.next) {
            //链表头节点下面的节点数据入栈
            stack.push(head.next.val);
            //链表头节点下面的节点数据存数组
            list.add(head.next.val);
            head = head.next;
        }
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if(list.get(i) != stack.pop().intValue()){
                    return false;
                }
            }
        }
        return true;
    }
}
