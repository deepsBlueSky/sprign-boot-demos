package design.abstractFactoryParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 21:36
 * @Versiion：1.0
 * @Desc:
 */

public interface EFactory {

    TV produceTv();
    AirCondition produceAirCondition();
}
