package design.abstractFactoryParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 21:35
 * @Versiion：1.0
 * @Desc:
 */

public class HaierAirCondition implements AirCondition{
    @Override
    public void changeTemp() {
        System.out.println("haier 空调温度调整");
    }
}
