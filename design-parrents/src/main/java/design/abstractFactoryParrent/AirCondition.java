package design.abstractFactoryParrent;

/**
 * @Author：Administrator
 * @Date：2022/9/19/0019 21:33
 * @Versiion：1.0
 */
public interface AirCondition {

    void changeTemp();
    
}
