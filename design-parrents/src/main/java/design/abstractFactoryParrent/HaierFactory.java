package design.abstractFactoryParrent;


/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 22:04
 * @Versiion：1.0
 * @Desc:
 */

public class HaierFactory implements EFactory {
    @Override
    public TV produceTv() {
        return new HaierTV();
    }

    @Override
    public AirCondition produceAirCondition() {
        return new HaierAirCondition();
    }
}
