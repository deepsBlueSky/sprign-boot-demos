package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:42
 * @Versiion：1.0
 * @Desc:
 */

public class Consumer {
    public static void main(String[] args) {
        TvFactory tvFactory = new LgTvFactory();
        TV tv = tvFactory.productTv();
        tv.play();
    }
}
