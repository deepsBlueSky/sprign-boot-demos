package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:38
 * @Versiion：1.0
 * @Desc:
 */

public class HisenseTV implements TV {
    @Override
    public void play() {
        System.out.println("hisensTV 播放中！！！！");
    }
}
