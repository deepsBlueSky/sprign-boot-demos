package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:39
 * @Versiion：1.0
 * @Desc:
 */

public interface TvFactory {
    /**
     * 生产电视
     * @return
     */
    TV productTv();
}
