package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 21:10
 * @Versiion：1.0
 * @Desc:
 */

public class LgTV implements TV{

    public LgTV() {
        System.out.println("生产LGTV");
    }

    @Override
    public void play() {
        System.out.println("LG TV播放中！！！！！");
    }
}
