package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:41
 * @Versiion：1.0
 * @Desc:
 */

public class HisenseTvFactory implements TvFactory{
    /**
     * 生产电视
     *
     * @return
     */
    @Override
    public TV productTv() {
        System.out.println("生产HisenseTV");
        return new HisenseTV();
    }
}
