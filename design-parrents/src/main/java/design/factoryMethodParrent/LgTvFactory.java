package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 21:11
 * @Versiion：1.0
 * @Desc:
 */

public class LgTvFactory implements TvFactory {
    /**
     * 生产电视
     *
     * @return
     */
    @Override
    public TV productTv() {
        return new LgTV();
    }
}
