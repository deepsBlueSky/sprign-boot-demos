package design.factoryMethodParrent;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:40
 * @Versiion：1.0
 * @Desc:
 */

public class HaierTvFactory implements TvFactory{
    /**
     * 生产电视
     *
     * @return
     */
    @Override
    public TV productTv() {
        System.out.println("生产haier Tv");
        return new HaierTV();
    }
}
