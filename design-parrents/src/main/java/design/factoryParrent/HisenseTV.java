package design.factoryParrent;


import design.factoryMethodParrent.TV;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:38
 * @Versiion：1.0
 * @Desc:
 */

public class HisenseTV implements TV {

    public HisenseTV() {
        System.out.println("生产hisense Tv");
    }

    @Override
    public void play() {
        System.out.println("hisensTV 播放中！！！！");
    }
}
