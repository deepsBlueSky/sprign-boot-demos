package design.factoryParrent;

import design.factoryMethodParrent.TV;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:39
 * @Versiion：1.0
 * @Desc:
 */

public class TvFactory {
    /**
     * 生产电视
     * @return
     */
    public static TV productTv(String type){
        switch (type){
            case "haier":
                return new HaierTV();
            case "hisense":
                return new HisenseTV();
            default:
                return null;
        }
    }
}
