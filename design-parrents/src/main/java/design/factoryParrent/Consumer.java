package design.factoryParrent;

import design.factoryMethodParrent.TV;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:58
 * @Versiion：1.0
 * @Desc:
 */

public class Consumer {
    public static void main(String[] args) {
        String type = "haier";
        TV tv = TvFactory.productTv(type);
        if (null != tv) {
            tv.play();
        }
    }
}
