package design.factoryParrent;

import design.factoryMethodParrent.TV;

/**
 * @Author：kangsen
 * @Date：2022/9/19/0019 20:38
 * @Versiion：1.0
 * @Desc:
 */

public class HaierTV implements TV {

    public HaierTV() {
        System.out.println("生产haier TV");
    }

    @Override
    public void play() {
        System.out.println("haier TV 播放！！！");
    }
}
