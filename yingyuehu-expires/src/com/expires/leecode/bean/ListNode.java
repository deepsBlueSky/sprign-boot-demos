package com.expires.leecode.bean;

/**
 * @Author：Administrator
 * @Date：2022/5/30/0030 20:30
 * @Versiion：1.0
 */

public class ListNode {
    int val;
    ListNode next;
    public ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public boolean hasNext(){
        if(this.next != null){
            return true;
        }
        return false;
    }
}
