package com.expires.leecode.bean;

import java.util.List;

/**
 * @program: SpringBootDemos
 * @description: 多节点的树
 * @author: Kangsen
 * @create: 2022-05-30 14:15
 **/

public class MultipleTreeNode {
    private String value;
    private List<MultipleTreeNode> childNodes;

    public MultipleTreeNode() {
    }

    public MultipleTreeNode(String value, List<MultipleTreeNode> childNodes) {
        this.value = value;
        this.childNodes = childNodes;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<MultipleTreeNode> getchildNodes() {
        return childNodes;
    }

    public void setchildNodes(List<MultipleTreeNode> childNodes) {
        this.childNodes = childNodes;
    }
}
