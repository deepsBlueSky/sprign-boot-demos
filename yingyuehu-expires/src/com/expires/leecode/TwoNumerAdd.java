package com.expires.leecode;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: SpringBootDemos
 * @description: 两数之和  https://leetcode.cn/problems/two-sum/
 * @author: Kangsen
 * @create: 2022-05-27 15:24
 **/

public class TwoNumerAdd {
    public static int[] twoSum(int[] nums, int target) {
        if(nums.length<2){
            throw new RuntimeException("数组太短");
        }
        int[] result = new int[2];

        //暴力硬解
        /*int i = 0;
        boolean writeFlag = false;
        while(i < nums.length ){
            if(!writeFlag){
                for (int i1 = (i+1); i1 < nums.length; i1++) {
                    if(nums[i] + nums[i1] == target){
                        result[0] = i;
                        result[1] = i1;
                        writeFlag = true;
                    }
                }
                i++;
            }else
                break;
        }*/

        //转成map
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complete = target - nums[i];
            if(map.containsKey(complete)){
                result[0] = i;
                result[1] = map.get(complete);
                return result;
            }else{
                map.put(nums[i], i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] result = new int[]{1,2,3,4,5,6,3};
        int[] ints = twoSum(result, 8);
        for (int anInt : ints) {
            System.out.println(anInt);
        }

        //System.out.println(new Integer(-1).intValue()  > new Integer(-8).intValue());
    }
}
