package com.expires.test;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-22 13:59
 **/
public class Student extends Person{
    private String schoolNum;

    public Student() {
        super();
    }

    public Student(String name, int age, String address) {
        super(name, age, address);
    }

    public String getSchoolNum() {
        return schoolNum;
    }

    public void setSchoolNum(String schoolNum) {
        this.schoolNum = schoolNum;
    }
}
