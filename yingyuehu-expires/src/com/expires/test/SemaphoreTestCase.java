package com.expires.test;

import java.util.concurrent.Semaphore;

/**
 * @program: SpringBootDemos
 * @description:  信号量 主要是对一个值进行加(release) 减(acquire)
 * @author: Kangsen
 * @create: 2022-06-09 10:30
 **/

public class SemaphoreTestCase {

    //创建初始值为0的信号量  需要两次release 后才能 acquire
    private Semaphore semaphore = new Semaphore(0);


}
