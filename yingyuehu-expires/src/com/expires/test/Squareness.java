package com.expires.test;

/**
 * @Author：Administrator
 * @Date：2022/6/7/0007 20:23
 * @Versiion：1.0
 */

public class Squareness {

    private int length;
    private int width;


    public int area(int length,int width){
        return length * width;
    }

    public int area(){
        return this.length * this.width;
    }



    public Squareness() {
    }

    public Squareness(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
