package com.expires.test.synctest;

import java.util.concurrent.TimeUnit;

/**
 * @Author：Administrator
 * @Date：2022/6/21/0021 22:02
 * @Versiion：1.0
 *
 * synchronized(this)、synchronized(xx.class)、synchronized(Object)的使用及区别
 *
 *  this 和 obj[一个对象] 效果是等同的  锁住同一个对象
 *
 */

public class SyncTest {

    public void test(){
        synchronized (SyncTest.class){
            try {
                System.out.println(Thread.currentThread().getName() + "use the method and start");
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                System.out.println(Thread.currentThread().getName() + "use the method and end");
            }
        }
    }


    static class ThreadA implements Runnable{

        private SyncTest syncTest;

        public ThreadA(SyncTest syncTest) {
            this.syncTest = syncTest;
        }

        @Override
        public void run() {
            syncTest.test();
        }
    }

    public static void main(String[] args) {
        SyncTest syncTest = new SyncTest();

        new Thread(new ThreadA(syncTest)).start();

        new Thread(new ThreadA(syncTest)).start();
    }

}
