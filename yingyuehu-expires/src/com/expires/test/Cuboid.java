package com.expires.test;

/**
 * @Author：Administrator
 * @Date：2022/6/7/0007 20:25
 * @Versiion：1.0
 */

public class Cuboid extends Squareness{

    private int height;

    public int volume(int height){
        return area() * height;
    }

    public int volume(){
        return area() * this.height;
    }

    public Cuboid() {
    }

    public Cuboid(int height) {
        this.height = height;
    }

    public Cuboid(int length, int width, int height) {
        super(length, width);
        this.height = height;
    }
}
