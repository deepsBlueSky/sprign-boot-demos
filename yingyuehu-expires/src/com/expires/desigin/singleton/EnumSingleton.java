package com.expires.desigin.singleton;

/**
 * @Author：Administrator
 * @Date：2022/6/21/0021 17:25
 * @Versiion：1.0
 */

public enum  EnumSingleton {
    SINGLETON;

    private int count = 0;

    public void say(String name){
        System.out.println("hello "+name);
    }

    public int add(int num){
        count += num;
        return count;
    }
}
