package com.expires.desigin.factorypattern;

/**
 * @Author：Administrator
 * @Date：2022/6/22/0022 21:53
 * @Versiion：1.0
 *
 *
 *工厂模式 之 工厂方法模式
 *
 */

public class FactoryMethod {

    interface Sender{
        void send();
    }

    static class MailSender implements Sender{

        @Override
        public void send() {
            System.out.println("email sender ...");
        }
    }


    static class SmsSender implements Sender{

        @Override
        public void send() {
            System.out.println("sms sender ...");
        }
    }

    static class SendFactory{
        public Sender productNail(){
            return new MailSender();
        }

        public Sender productSms(){
            return new SmsSender();
        }
    }

    public static void main(String[] args) {
        SendFactory sendFactory = new SendFactory();
        Sender sender = sendFactory.productSms();
        sender.send();

        sender = sendFactory.productNail();
        sender.send();
    }
}
