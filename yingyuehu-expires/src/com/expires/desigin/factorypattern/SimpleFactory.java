package com.expires.desigin.factorypattern;

/**
 * @Author：Administrator
 * @Date：2022/6/22/0022 21:32
 * @Versiion：1.0
 *
 * 简单工厂模式
 *
 */

public class SimpleFactory {

    interface Sender{

        void send();

    }


    class MailSender implements Sender{

        @Override
        public void send() {
            System.out.println("mail send...");
        }
    }


    class SmsSender implements Sender{

        @Override
        public void send() {
            System.out.println("sms send ...");
        }
    }

    public Sender getSender(String sender){
        if(sender.equalsIgnoreCase("sms")){
            return new SmsSender();
        }else if(sender.equalsIgnoreCase("email")){
            return new MailSender();
        }else{
            return null;
        }
    }

    public static void main(String[] args) {
        SimpleFactory simpleFactory = new SimpleFactory();
        Sender sms = simpleFactory.getSender("sms");
        sms.send();

        sms = simpleFactory.getSender("email");
        sms.send();
    }
}
