package com.crawel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: SpringBootDemos
 * @description: 爬天天赢球
 * @author: Kangsen
 * @create: 2022-06-10 09:26
 **/
@SpringBootApplication
public class CrawlApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrawlApplication.class,args);
    }

}
