package com.crawel.controller;

import com.crawel.utils.HttpPoolUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-10 09:28
 **/
@RestController
@RequestMapping("/tt")
@Slf4j
public class TtController {

    private static Map<String,String> header = new HashMap<>();

    /*private static List<String> uaList = Lists.newArrayList(
            "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/102.0.5005.63",
            "Mozilla/5.0 (Linux; Android 11; Pixel 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.91 Mobile Safari/537.36 Edg/102.0.5005.63"
    );*/

    static {
        header.put("Content-Type","application/json");
        header.put("User-Agent","Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36 Edg/102.0.5005.63");
        header.put("Accept","application/json, text/plain, */*");
        header.put("Accept-Encoding","gzip, deflate, br");
        header.put("Referer","http://tt.aicai.com/");
        header.put("Origin","http://tt.aicai.com/");
    }

    /**
     * 获取天天赢球的赛事列表 默认是当天的
     * @return
     */
    @RequestMapping(value = "/matchList",method = RequestMethod.GET)
    public String matchList(){
        String path = "";
        //构建参数
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("timestamp",String.valueOf(System.currentTimeMillis())));
        nameValuePairs.add(new BasicNameValuePair("verifyStr",""));
        nameValuePairs.add(new BasicNameValuePair("apiName","getMatchListByDate"));
        nameValuePairs.add(new BasicNameValuePair("game","0"));
        nameValuePairs.add(new BasicNameValuePair("jcTurnFlag","1"));
        nameValuePairs.add(new BasicNameValuePair("pageNo","1"));
        nameValuePairs.add(new BasicNameValuePair("pageSize","20"));
        String executePost = HttpPoolUtils.executePost(path, nameValuePairs, null, header);
        log.info("executePost：{}",executePost);
        return executePost;
    }

}
