package com.es.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：kangsen
 * @Date：2022/10/7/0007 21:03
 * @Versiion：1.0
 * @Desc:
 */
@Configuration
@Slf4j
public class MallEsConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient(){
        log.info("初始化es client");
        return new RestHighLevelClient(
                RestClient.builder(new HttpHost("192.168.3.53",9200,"http")));
    }

}
