package com.es.controller;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：kangsen
 * @Date：2022/10/7/0007 21:13
 * @Versiion：1.0
 * @Desc:
 */
@RestController
public class EsController {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @GetMapping("/test")
    public String test(){
        System.out.println(restHighLevelClient);
        return "success";
    }

}
