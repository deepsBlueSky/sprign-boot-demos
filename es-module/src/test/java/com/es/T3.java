package com.es;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author：kangsen
 * @Date：2022/10/13/0013 19:22
 * @Versiion：1.0
 * @Desc:
 */
@Slf4j
public class T3 {

    @Test
    public void cal(){
        int[] arr1 = new int[]{1,2};
        int[] arr2 = new int[]{3,4,9,8};
        int[] arr3 = new int[]{1,2,3,4,5,6,7};
        int[] arr4 = new int[]{3,7,9,1,575,5};
        List<int[]> parent = Arrays.asList(arr1,arr2,arr3,arr4);

        List<List<Integer>> result = new ArrayList<>();
        /*for (int i = 0; i < parent.size(); i++) {
            parent.get(i),
        }*/
        /*combination(arr1, arr2, 0, result);*/
        /*result = appendAllItem(result,arr1);
        log.info("szie :{}  result:{}",result.size(),result);

        result = appendAllItem(result,arr2);
        log.info("szie :{}  result:{}",result.size(),result);

        result = appendAllItem(result,arr3);
        log.info("szie :{}  result:{}",result.size(),result);

        result = appendAllItem(result,arr4);
        log.info("szie :{}  result:{}",result.size(),result);*/

        for (int[] ints : parent) {
            result = appendAllItem(result,ints);
        }
        log.info("szie :{}  result:{}",result.size(),result);

    }

    public List<List<Integer>> appendAllItem(List<List<Integer>> result,int[] appendArray){
        if(result.isEmpty()){
            List<Integer> add;
            for (int i : appendArray) {
                add = new ArrayList<>();
                add.add(i);
                result.add(add);
            }
            return result;
        }else{
            List<Integer> add;
            List<List<Integer>> r = new ArrayList<>();
            for (List<Integer> list : result) {
                for (int i : appendArray) {
                    add = new ArrayList<>(list.size()+1);
                    add.add(i);
                    add.addAll(list);
                    r.add(add);
                }
            }
            return r;
        }
    }
}
