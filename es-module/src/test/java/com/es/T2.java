package com.es;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author：kangsen
 * @Date：2022/10/13/0013 19:22
 * @Versiion：1.0
 * @Desc:
 */
@Slf4j
public class T2 {

    @Test
    public  void  cal(){
        int[] arr1 = new int[]{1,2};
        int[] arr2 = new int[]{3,4,9,8};
        int[] arr3 = new int[]{1,2,3,4,5,6,7};
        int[] arr4 = new int[]{3,7,9,1,575,5};
        List<int[]> parent = Arrays.asList(arr1,arr2,arr3,arr4 );
        List<Integer> result = new ArrayList<>();
        int[] crr1 = parent.get(0);
        for (int j = 0; j < crr1.length  ;j++) {
            cross(crr1[j],parent,1,result);
        }
        log.info("result:{}",result);
    }

    private static void cross(int v ,List<int[]>  parent, int index,List<Integer> result) {
        int[] crr2 = parent.get(index);
        for (int i = 0; i <crr2.length ; i++){
            int va = v * crr2[i];
            if(index==parent.size()-1){
                result.add(va);
            }else{
                cross( va,parent,index+1,result);
            }
        }
    }
}
