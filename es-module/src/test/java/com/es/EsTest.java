package com.es;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;

/**
 * @Author：kangsen
 * @Date：2022/10/7/0007 21:05
 * @Versiion：1.0
 * @Desc:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class EsTest {

    @Resource
    private RestHighLevelClient restHighLevelClient;

    public void put(){
        try {
            IndexRequest indexRequest = new IndexRequest("users");
            indexRequest.id("2");
            User user = new User();
            user.setUserName("周杰伦");
            user.setAddress("中国台湾");
            user.setAge(26);
            user.setBalance(50000);
            //资源及类型  es 保存的json类型的数据 因此要转成json格式字符串
            indexRequest.source(JSON.toJSONString(user), XContentType.JSON);
            //index相当于mysql中的数据库 database
            //type相当于mysql中的表【es7以后type被废弃 一个index中只有一个默认的type:_doc  index即可以被认为是database又可以被认为是table】
            //document相当于mysql中表的记录

            //存储或者更新
            IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            log.info("response:{}", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void search(){
        try {
            //查询
            SearchRequest searchRequest = new SearchRequest();
            //指定索引
            searchRequest.indices("users");
            //制定DSL
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.matchQuery("userName", "小"));
            //按照年龄的值分布聚合
            searchSourceBuilder.aggregation(AggregationBuilders.terms("Age").field("age").size(10));
            //计算平均薪资
            searchSourceBuilder.aggregation(AggregationBuilders.avg("Avg").field("balance"));

            log.info("检索条件：{}", searchSourceBuilder);
            searchRequest.source(searchSourceBuilder);

            //同步执行（也可以异步执行）
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            log.info("查询结果：{}",searchResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void t1(){
        System.out.println(restHighLevelClient);
        //put();
        search();
    }

    @Test
    public void insertTest(){
        try {
            IndexRequest indexRequest = new IndexRequest();
            indexRequest.index("user").id("10010");
            User user = new User();
            user.setUserName("ks");
            user.setAge(23);
            user.setBalance(1500);
            user.setAddress("陕西西安");

            ObjectMapper objectMapper = new ObjectMapper();
            String userString = objectMapper.writeValueAsString(user);
            indexRequest.source(userString,XContentType.JSON);
            IndexResponse indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
            log.info("insert result:{}", indexResponse.getResult());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //删除索引
    @Test
    public void deleteTest(){
        try {
            AcknowledgedResponse response = restHighLevelClient.indices().delete(new DeleteIndexRequest("uu"),RequestOptions.DEFAULT);
            log.info("AcknowledgedResponse >>>>:{}", response.isAcknowledged());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                restHighLevelClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void updateTest(){
        //更新数据
        try{
            UpdateRequest updateRequest = new UpdateRequest();
            //对那个索引 的id 的哪个值进行 修改
            updateRequest.index("user").id("10010").doc(XContentType.JSON,"address","陕西西安莲湖区");
            UpdateResponse update = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
            log.info("UpdateResponse :{}",update);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void searchTest(){
        //查询索引
        try {
            //indices 查询所有的索引
            GetIndexResponse getIndexResponse = restHighLevelClient.indices().get(new GetIndexRequest("user"), RequestOptions.DEFAULT);
            log.info("getIndexResponse.getAliases>>>>:{}", getIndexResponse.getAliases());
            log.info("getIndexResponse.getMappings>>>>:{}", getIndexResponse.getMappings());
            log.info("getIndexResponse.getSettings>>>>:{}", getIndexResponse.getSettings());


            //查询index


        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                restHighLevelClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Data
    class User implements Serializable {
        private String userName;
        private String address;
        private int age;
        private double balance;
    }

}
