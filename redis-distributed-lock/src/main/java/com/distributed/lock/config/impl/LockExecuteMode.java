package com.distributed.lock.config.impl;

/**
 * @program: SpringBootDemos
 * @description: 分布式锁执行类型
 *
 * 必须执行 和 跳过执行
 *
 *
 * @author: Kangsen
 * @create: 2022-07-14 10:58
 **/
public enum LockExecuteMode {

    //必须执行: 没拿到锁就等待  知道拿到锁为止
    MUSTBEIMPLEMENTED,

    //跳过执行: 没拿到锁跳过 不执行
    SKIPEXECUTION;
}
