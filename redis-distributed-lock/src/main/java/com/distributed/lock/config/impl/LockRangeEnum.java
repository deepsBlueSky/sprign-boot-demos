package com.distributed.lock.config.impl;

/**
 * @program: SpringBootDemos
 * @description: 分布式锁类型
 * @author: Kangsen
 * @create: 2022-07-14 10:58
 **/
public enum LockRangeEnum {

    //进程锁
    APPLICATION,

    //线程锁
    THREAD;
}
