package com.distributed.lock.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-13 15:53
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "redisson")
public class RedissonProperties {
    private String address;
    private String password;
    private int timeout = 3000;
    private int connectionPoolSize = 64;
    private int connectionMinimumIdleSize = 10;
}
