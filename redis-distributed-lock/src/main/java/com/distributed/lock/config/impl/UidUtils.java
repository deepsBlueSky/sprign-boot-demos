package com.distributed.lock.config.impl;

import java.util.UUID;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-14 11:23
 **/

public class UidUtils {
    public static String randomUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

}
