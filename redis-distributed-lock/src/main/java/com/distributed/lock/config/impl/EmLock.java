package com.distributed.lock.config.impl;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-14 10:57
 **/
@Documented
//用于描述注解的生命周期（即：被描述的注解在什么范围内有效） 具体看RetentionPolicy枚举
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EmLock {

    /*默认是进程的分布锁*/
    LockRangeEnum lockRange() default LockRangeEnum.APPLICATION;

    /*分布锁的key*/
    String key();

    /*执行模式  默认 跳过执行模式*/
    LockExecuteMode executeMode() default LockExecuteMode.SKIPEXECUTION;

    /*获取锁等待时间  等待时间内获取到锁直接返回*/
    int waitTime() default 0;

    /*分布锁超时时间*/
    int timeout() default 2;

    /*超时时间单位 默认:s*/
    TimeUnit timeunit() default TimeUnit.SECONDS;

    /*自动解锁*/
    boolean  autoRelease() default false;
}
