package com.distributed.lock.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-12 14:47
 **/
@RestController
@Slf4j
public class TestController {

    @Resource
    private RedisTemplate redisTemplate;

    @GetMapping("/set/{key}")
    public String setKey(@PathVariable("key") String key){
        log.info("==>{}",Thread.currentThread().getName());
        try {
            redisTemplate.opsForValue().set(key, UUID.randomUUID().toString());
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @GetMapping("/get/{key}")
    public String getKey(@PathVariable("key") String key){
        log.info("==>{}",Thread.currentThread().getName());
        try {
            return (String) redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

}
