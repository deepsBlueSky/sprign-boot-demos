package com.distributed.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @program: SpringBootDemos
 * @description:
 * https://blog.csdn.net/zxd1435513775/article/details/122194202
 * https://blog.csdn.net/qq_44413835/article/details/117320028
 * @author: Kangsen
 * @create: 2022-07-12 14:45
 **/
@SpringBootApplication
@EnableScheduling
public class RedisDistributedLockApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisDistributedLockApplication.class,args);
    }
}
