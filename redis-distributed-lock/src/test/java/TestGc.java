import java.util.HashSet;
import java.util.Set;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-07-12 16:07
 **/

public class TestGc {

    private static Set<Object> sets = new HashSet<>();

    public static void main(String[] args) {
        /**
         * 这里会有内存泄漏的问题
         * */
        //set在堆中创建
        Set<Object> set = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            //创建一个引用执行 一个对内存的对象上
            Object obj = new Object();
            //set 持有这个引用
            set.add(obj);
            //引用指向空
            obj = null;
        }
        sets.addAll(set);
        System.out.println(set.size());
    }
}
