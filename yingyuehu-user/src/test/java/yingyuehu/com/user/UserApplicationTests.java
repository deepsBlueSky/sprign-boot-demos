package yingyuehu.com.user;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * task任务去异步执行 然后统一返回结果处理
 * 类似于分布式的思想：地相关性业务分开处理 然后做结果的聚合
 */

//@SpringBootTest
@Slf4j
class UserApplicationTests {

    @Test
    void contextLoads() throws ExecutionException, InterruptedException {
        testAsync2();
        //testAsync3();
        //asynchronousErrorHandling();
    }

    /***
     * 异步处理等待结果框架 twitter future
     * https://juejin.cn/post/7100579897952731167
     */
    public void testAsync1() throws ExecutionException, InterruptedException {
        //异步任务1
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(()->{
            long currentTimeMillis = System.currentTimeMillis();
            try {
                log.info("future1 task => {}",Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("future1 result:{}",(System.currentTimeMillis() - currentTimeMillis)/1000);
            return "future1";
        });
        String result = future1.get();
        log.info("Main goes on...");
        log.info(result);
    }

    /***
     * 异步处理等待结果框架 twitter future
     * https://juejin.cn/post/7100579897952731167
     */
    public void testAsync2() throws ExecutionException, InterruptedException {
        //异步任务1
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(()->{
            try {
                log.info("future1 task => {}",Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("future1 result");
            return "future1";
        });
        //异步任务2
        CompletableFuture<String> future2 = future1.thenApply(s->{
            try {
                log.info("future2 task=>{}",Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("future2 result");
            return "future2";
        });
        String result = future2.get();
        log.info("Main goes on...");
        log.info(result);
    }

    /**
     * 并行多个异步任务  等待结果统一返回
     */
    public void testAsync3() throws ExecutionException, InterruptedException {
        int total = 0;
        //异步任务1
        CompletableFuture<Integer> c1 = CompletableFuture.supplyAsync(() -> {
            long currentTimeMillis = System.currentTimeMillis();
            log.info("currentTimeMillis:{}",currentTimeMillis);
            int sum = 0;
            /*try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            for (int i = 0; i < 199; i++) {
                sum += 1;
            }
            log.info("耗时：{} s",(System.currentTimeMillis() - currentTimeMillis)/1000);
            return sum;
        });

        //异步任务2
        CompletableFuture<Integer> c2 = CompletableFuture.supplyAsync(() -> {
            long currentTimeMillis = System.currentTimeMillis();
            log.info("currentTimeMillis:{}",currentTimeMillis);
            int sum = 0;
            /*try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            for (int i = 0; i < 299; i++) {
                sum += 1;
            }
            log.info("耗时：{} s",(System.currentTimeMillis() - currentTimeMillis)/1000);
            return sum;
        });

        //异步任务3
        CompletableFuture<Integer> c3 = CompletableFuture.supplyAsync(() -> {
            long currentTimeMillis = System.currentTimeMillis();
            log.info("currentTimeMillis:{}",currentTimeMillis);
            int sum = 0;
            /*try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            for (int i = 0; i < 199; i++) {
                sum += 1;
            }
            log.info("耗时：{} s",(System.currentTimeMillis() - currentTimeMillis)/1000);
            return sum;
        });
        long timeMillis = System.currentTimeMillis();
        log.info(" {} ",timeMillis);
        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.allOf(c1,c2,c3);
        voidCompletableFuture.get();
        log.info("主线程这时候在等待任务执行完：{}  耗时：{}",System.currentTimeMillis(),(System.currentTimeMillis()-timeMillis)/1000);
        Integer c1Sum = c1.get();
        Integer c2Sum = c2.get();
        Integer c3Sum = c3.get();
        log.info("c1:{}  {}",c1Sum,System.currentTimeMillis());
        log.info("c2:{}  {}",c2Sum,System.currentTimeMillis());
        log.info("c3:{}  {}",c3Sum,System.currentTimeMillis());
        log.info("total:{}",(c1Sum + c2Sum + c3Sum));
    }


    /**
     * 异步错误处理
     */
    public void asynchronousErrorHandling() throws ExecutionException, InterruptedException {
        String name = "test";
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(()->{
            if("test".equalsIgnoreCase(name)){
                throw new RuntimeException("CompletableFuture error!");
            }
            return "hello "+name;
        }).handle((s,t) -> {
            log.info("s:{}  t:{}",s,t);
            return s != null ? s : "hello,stranger!";
        });
        log.info(completableFuture.get());
    }
}
