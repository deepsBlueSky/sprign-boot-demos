package yingyuehu.com.user;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 *
 * 面试题 淘宝？
 * 实现一个容器  提供两个方法  add   size
 * 写两个线程  县城1添加10个元素到容器中， 线程2实现监控元素的个数  当个数到达5个时 线程2给出提示并结束
 * @Author：Administrator
 * @Date：2022/6/15/0015 22:51
 * @Versiion：1.0
 */

public class TwoThreadTest {


    static class Continer{
        public Continer() {
            this.list = new ArrayList<>();
            //或者使用同步容器: 防止一个线程添加元素后  list的size 再未进行++ 操作时就有线程来读size 导致不一致 因为使用同步容器
            this.list = Collections.synchronizedList(new ArrayList<>());
        }
        //todo: volatile用来修改引用原则上是不能解决此问题  因为他只能保证引用的地址没有变  地址执行堆中的对象 中成员变量啥的变了
        // 他是不知道的  但是这里通过加上sleep 解决了 可能是因为在sleep 期间同步了内容【猜测】  一般还是尽量不要用  或者用 也只是用来修饰简单的值
        private volatile List<Object> list;
        public void add(Object object){
            if(this.list == null){
                this.list = new ArrayList<>();
            }
            this.list.add(object);
        }
        public int size(){
            if(this.list == null){
                return 0;
            }else{
                return this.list.size();
            }
        }
    }

    public static void main(String[] args) {
        Continer continer = new Continer();
        Semaphore semaphore1 = new Semaphore(1);
        Semaphore semaphore2 = new Semaphore(0);
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    semaphore1.acquire();
                    continer.add(i);
                    System.out.println("continer add "+ i);
                    if(continer.size() == 5){
                        semaphore2.release();
                    }else{
                        semaphore1.release();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(()->{
            /*while (true){
                try {
                    semaphore2.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(continer.size() == 5){
                    System.out.println("continer size 等于5了，线程终止");
                    break;
                }else{
                    semaphore1.release();
                }

            }*/

                try {
                    semaphore2.acquire();
                    System.out.println("continer size 等于5了，线程终止");
                    semaphore1.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        });

        t1.start();
        t2.start();


        /*Continer continer = new Continer();
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                continer.add(i);
                System.out.println("continer add "+ i);
                //TimeUnit.SECONDS.sleep(1);
            }
        });

        Thread t2 = new Thread(()->{
            while (true){
                if(continer.size() == 5){
                    System.out.println("continer size 等于5了，线程终止");
                    break;
                }
            }
        });
        t1.start();
        t2.start();*/



    }
}
