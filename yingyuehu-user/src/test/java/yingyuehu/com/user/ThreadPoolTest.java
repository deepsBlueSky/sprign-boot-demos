package yingyuehu.com.user;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

/**
 * @program: SpringBootDemos
 * @description: 线程池测试
 * @author: Kangsen
 * @create: 2022-05-24 12:05
 *
 *
 * 线程池一共有7种，按创建类型分为两类：Executors 和 ThreadPoolExecutor
 *  Executors:
 *      newFixedThreadPool 固定容量的线程池，超过容量数量的线程就会等待
 *      newCachedThreadPool 缓存线程池 线程超过处理所需，会缓存一段时间后会回收，线程数不够则创建线程
 *      newSingleThreadPool 单例线程池，可以保证先进先出的执行顺序
 *      newScheduleThreadPool 可执行延迟任务的线程池
 *      newSingleThreadSchedulePool  单例延迟任务的线程池
 *      newWorkStealingPool 抢占式执行线程池  谁抢到算谁
 *
 *  ThreadPoolExecutor： 最原始的线程池创建方式，有7个参数设置   线程池模型：https://www.zhihu.com/question/299968861/answer/2469077330
 *
 *  一般使用Java提供了创建线程池的接口Executor()，推荐用子类ThreadPoolExecutor来创建线程池。这在阿里巴巴《Java开发手册》中有说明：
 *
 *  线程池的阻塞队列的最大数量范围太大了不可控，如果处理不好 整个机器都会崩掉，使用原始方式创建可以自定义这个参数，可控
 *
 * 【强制】线程池不允许使用 Executors 去创建，而是通过 ThreadPoolExecutor 的方式，这样的处理方式让写的同学更加明确线程池的运行规则，规避资源耗尽的风险。
 * 说明：Executors 返回的线程池对象的弊端如下：
 * 1） FixedThreadPool 和 SingleThreadPool：允许的请求队列长度为 Integer.MAX_VALUE，可能会堆积大量的请求，从而导致 OOM。
 * 2）CachedThreadPool：允许的创建线程数量为 Integer.MAX_VALUE，可能会创建大量的线程，从而导致 OOM。
 **/
@Slf4j
public class ThreadPoolTest {

    //固定长度的线程池测试
    @Test
    public void executorsFixedThreadPoolTest() throws ExecutionException, InterruptedException {
        //定义定长线程池:只有核心线程 没有非核心线程
        /*ExecutorService executorService = Executors.newFixedThreadPool(2);
        log.info("定长线程池:{}",System.currentTimeMillis());*/
        ExecutorService executorService = Executors.newCachedThreadPool();
        //execute 和  sumbit 区别：最终都是执行的execute
        //execute 没有返回值  sumbit有返回
        //抛异常位置不一样，submit是在get的时候抛出异常,不执行get方法就不会提示抛异常
        /*executorService.execute(new Runnable() {
            @Override
            public void run() {
                log.info("execute 执行线程任务！！！！{}",System.currentTimeMillis());
            }
        });
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                log.info("execute 执行线程任务！！！！{}",System.currentTimeMillis());
                //throw new RuntimeException("主动抛出一个异常看看");
            }
        });
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                log.info("execute 执行线程任务！！！！{}",System.currentTimeMillis());
            }
        });*/
        Future<String> submit1 = executorService.submit(()->{
            log.info("使用submit给线程池提交任务1");
            return "使用submit给线程池提交任务1";
        });
        Future<String> submit2 = executorService.submit(()->{
            log.info("使用submit给线程池提交任务2");
            return "使用submit给线程池提交任务2";
        });
        Future<String> submit3 = executorService.submit(() -> {
            log.info("使用submit给线程池提交任务3");
            //throw new RuntimeException("主动抛出一个异常看看");
            return "使用submit给线程池提交任务3";
        });
        Future<?> submit4 = executorService.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 1000+999;
            }
        });
        executorService.shutdown();
        String s1 = submit1.get();
        log.info("s1:{}",s1);
        String s2 = submit2.get();
        log.info("s2:{}",s2);
        String s3 = submit3.get();
        log.info("s3:{}",s3);
        Object s4 = submit4.get();
        log.info("s4:{}",s4);
    }

    public String testSubmit(){
        return "test submit " + Thread.currentThread();
    }

    //newCachedThreadPool 缓存线程池测试:没有核心线程
    @Test
    public void newCachedThreadPoolTest(){
        //查看构造函数，这个线程池不存储任务元素,每次都必须等另一个线程调用移除操作后才会被执行，否则就会一直处于阻塞的状态
        // 适用应用场景：执行大量、耗时少的任务。一个可以无限扩大的线程池，比较适合处理执行时间比较小的任务。
        /*ExecutorService executorService =
                Executors.newCachedThreadPool(Executors.defaultThreadFactory());*/

        /*new Thread(()->{
            log.info("lamda -> start {}",System.currentTimeMillis());
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("还没执行完");
                log.info("lamda -> end {}",System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();*/

        /*Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("lamda -> start {}", System.currentTimeMillis());
                try {
                    TimeUnit.SECONDS.sleep(2);
                    log.info("lamda -> end {}", System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });*/
        Thread thread = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "线程开始执行");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(threadName + "任务执行完毕");
        });
        thread.start();
    }

    //newSingleThreadPool 单例的线程池 只有一个核心线程数 执行完立即收回
    @Test
    public void newSingleThreadExecutorTest(){
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            /*@Override
            public void run() {*/
                log.info("===>单例提交任务1 start {}",System.currentTimeMillis());
                try {
                    TimeUnit.SECONDS.sleep(2);
                    log.info("===>单例提交任务1 end {}",System.currentTimeMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            //}
        });
        /*executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("===>单例提交任务2 {}",System.currentTimeMillis());
            }
        });
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                log.info("===>单例提交任务3 start {}",System.currentTimeMillis());
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("===>单例提交任务3 end {}",System.currentTimeMillis());
            }
        });*/
        executorService.shutdown();
    }

    @Test
    public void threadPoolExecutorPoolTest(){
        /*七个参数：
            int corePoolSize                        //核心线程数
            int maximumPoolSize,                    //线程池的最大线程数
            long keepAliveTime,                     //最大线程数可以存活的时间，就是线程池中除了核心线程之外
            的其他线程最长可以保留的时候，因为在线程池中，除了核心线程即使在无任务的情况下也不能被清理，其余的线程都有存活时间,
            意思是非核心线程可以保留的最长的空闲时间
            TimeUnit unit,                          //keepAliveTime的时间单位
            BlockingQueue<Runnable> workQueue       //阻塞等待队列,一共有7种队列 [待研究一下]
                    ArrayBlockingQueue,             //阻塞数组队列
                    LinkedBlockingQueue,            //阻塞链表队列
                    SynchronousQueue,               //创建一个新的线程来执行新任务,或者是替换已存在的任务
                    PriorityBlockingQueue           //有优先级考虑的任务队列，通过comparator 排序
                    DelayQueue                      //达到超时时间会再去处理任务

            ThreadFactory threadFactory,            //线程工厂，创建线程 默认是正常优先级，非守护线程
            RejectedExecutionHandler handler        //拒绝策略,任务数超过线程池的最大线程数时拒绝执行的策略 4种
                    AbortPolicy 不执行，直接抛异常、
                    DisCardPolicy 不执行 也不抛异常 直接丢弃、
                    DisCardOldSetPolicy 将消息队列中最老的线程任务替换为当前任务执行、
                    CallerRunsPolicy 直接调用execute 执行当前任务
        */

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
            2,//核心线程数
            5,//最大线程数
            2,//非核心线程的最大空闲时间
            TimeUnit.MINUTES,//空闲时间的单位
            new LinkedBlockingDeque<>(),//阻塞等待队列
            Executors.defaultThreadFactory(),//默认线程工厂
            new ThreadPoolExecutor.AbortPolicy()//决绝策略
        );
    }


    public static void main(String[] args) {
        //ThreadPoolTest threadPoolTest = new ThreadPoolTest();
        //threadPoolTest.newCachedThreadPoolTest();
        Thread thread = new Thread(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "线程开始执行");
            try {
                System.out.println(threadName + "开始休眠");
                Thread.sleep(3000);
                System.out.println(threadName + "休眠完毕");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(threadName + "任务执行完毕");
        }, "work");


        thread.start();

        String threadName = Thread.currentThread().getName();
        try {
            thread.sleep(1);
            System.out.println(threadName + "开始休眠");
            thread.sleep(2000);
            System.out.println(threadName + "休眠完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(threadName + "线程执行完毕");
    }
}
