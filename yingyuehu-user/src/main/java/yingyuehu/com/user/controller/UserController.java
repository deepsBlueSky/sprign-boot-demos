package yingyuehu.com.user.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-05-23 10:56
 **/
@RestController
@Slf4j
public class UserController {

    @GetMapping("/user/{name}")
    public String index(@PathVariable String name) {
        log.info("info映月湖用户模块 用户名：{}",name);
        log.info("error映月湖用户模块 用户名：{}",name);
        log.info("warn映月湖用户模块 用户名：{}",name);
        return name;
    }
}
