package com.match.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-06-10 10:23
 **/
@RestController
@RequestMapping("tt")
public class TTController {

    @RequestMapping(value = "/matchList",method = RequestMethod.GET)
    public String matchList(){
        return "tt matchList";
    }

}
