package com.test.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author：kangsen
 * @Date：2022/9/29/0029 23:01
 * @Versiion：1.0
 * @Desc:
 */
@SpringBootApplication
@EnableScheduling
public class DynamicCronJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicCronJobApplication.class,args);
    }
}
