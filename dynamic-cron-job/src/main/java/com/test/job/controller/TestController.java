package com.test.job.controller;

import com.test.job.config.ScheduleTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：kangsen
 * @Date：2022/9/29/0029 23:21
 * @Versiion：1.0
 * @Desc:
 */

@RestController
@Slf4j
public class TestController {

    private final ScheduleTask scheduleTask;

    @Autowired
    public TestController(ScheduleTask scheduleTask) {
        this.scheduleTask = scheduleTask;
    }

    /**
     * 设置新的cron
     * @param cron
     * @return
     */
    @GetMapping("/set")
    public String updateCron(String cron){
        log.info("new cron:{}", cron);
        scheduleTask.setCron(cron);
        return "success";
    }
}
