package com.service;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-13 11:58
 **/

public interface UserService {


    void say(String name);

    void cry(String name);

}
