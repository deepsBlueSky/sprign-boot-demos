package com.service.impl;

import com.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-13 12:12
 **/
@Service
public class UserServiceImpl implements UserService {
    @Override
    public void say(String name) {
        System.out.println("hello "+ name + "!");
    }

    @Override
    public void cry(String name) {
        System.out.println(name + " 哭了 ！！！！！!");
    }
}
