package com.proxyDemo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-13 14:29
 **/

public class SystemProxyHandler implements InvocationHandler{

    private Object proxyObj;
    public SystemProxyHandler(Object obj){
        this.proxyObj = obj;
    }
    /**
     * @param proxy 动态代理对象
     * @param method 表示最终要执行的方法，method,invoke 用于执行被代理对象的方法 也就是真正的目标方法
     * @param args  这个参数就是向目标方法传参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("====================="+method.getName()+"======================");
        // 方法上有注解 或者方法名称来判断是否需要
        // method.getAnnotation()
        System.out.println("方法执行前置");
        Object invoke = null;
        method.invoke(proxyObj, args);
        System.out.println("方法执行结束");
        return invoke;
    }
}
