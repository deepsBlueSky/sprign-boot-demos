package com.proxyDemo;

import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-14 11:03
 **/

public class CglibProxyHandler implements InvocationHandler {

    private Object object;
    public CglibProxyHandler(Object object) {
        this.object = object;
    }

    /**
     * 方法的拦截器
     * 1、目标对象的方法调用
     * 2、行为增强
     * @param o             动态生成代理类的实例
     * @param method
     * @param objects
     * @param methodProxy
     * @return
     * @throws Throwable
     */
    //@Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("方法执行前直");
        Object invoke = methodProxy.invoke(object, objects);
        System.out.println("方法执行后直");
        return invoke;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        System.out.println("方法执行前直");
        Object invoke = method.invoke(object, objects);
        System.out.println("方法执行后直");
        return invoke;
    }
}
