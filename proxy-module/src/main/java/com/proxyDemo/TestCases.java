package com.proxyDemo;

import com.service.UserService;
import com.service.impl.UserServiceImpl;
import org.junit.Test;
import org.springframework.cglib.proxy.Enhancer;

import java.lang.reflect.Proxy;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-09-13 11:49
 **/
public class TestCases extends AbstractTransactionalTests{

    @Test
    public void t1() {
        UserService proxy = (UserService) LogProxy.getObject(new UserServiceImpl());
        proxy.say("小明");
    }

    @Test
    public void t2(){
        UserService userService = (UserService) LogProxy.getObjectByCglib(new UserServiceImpl());
        userService.say("小红");
    }

    @Test
    public void t3(){
        UserService userService = new UserServiceImpl();
        SystemProxyHandler systemProxyHandler = new SystemProxyHandler(userService);
        ClassLoader classLoader = userService.getClass().getClassLoader();
        Class<?>[] interfaces = userService.getClass().getInterfaces();
        UserService proxyInstance = (UserService) Proxy.newProxyInstance(classLoader, interfaces, systemProxyHandler);
        proxyInstance.say("小明");
        proxyInstance.say("小红");

        proxyInstance.cry("小明");
        proxyInstance.cry("小红");
    }

    @Test
    public void t4(){
        UserServiceImpl userService = new UserServiceImpl();
        Class<?>[] interfaces = userService.getClass().getInterfaces();
        CglibProxyHandler cglibProxyHandler = new CglibProxyHandler(userService);
        UserServiceImpl s = (UserServiceImpl) Enhancer.create(UserServiceImpl.class, interfaces, cglibProxyHandler);
        s.say("小王");
    }
}
