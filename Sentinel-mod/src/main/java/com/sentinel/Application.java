package com.sentinel;

import com.alibaba.csp.sentinel.init.InitExecutor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @program: SpringBootDemos
 * @description: sentinel:资源、限流、降级、熔断
 * @author: Kangsen
 * @create: 2021-10-22 10:04
 **/
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        //SpringApplication.run(Application.class,args);

        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        // 连接到控制台，与sentinel控制台通信
        System.setProperty("project.name",
                context.getEnvironment().getProperty("spring.application.name","sentinel"));
        System.setProperty("csp.sentinel.dashboard.server",
                context.getEnvironment().getProperty("sentinel.dashboard.server","localhost:8080"));
        InitExecutor.doInit();

    }
}
