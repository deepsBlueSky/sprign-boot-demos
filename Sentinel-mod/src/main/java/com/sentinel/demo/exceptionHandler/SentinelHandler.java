package com.sentinel.demo.exceptionHandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.sentinel.demo.common.ResultBody;
import com.sentinel.demo.exception.CommonEnum;

/**
 * @program: SpringBootDemos
 * @description: 自定义 sentinel 限流降级类
 * @author: Kangsen
 * @create: 2021-10-25 14:05
 **/
public class SentinelHandler {

    //参数、返回值与资源一致
    public static Object blockHandlerA(String name,BlockException exception){
        //return "这是第一个限流降级方法 =>"+ name;
        //return new BizException(CommonEnum.SERVER_BUSY);
        return JSON.toJSONString(ResultBody.ERROR(CommonEnum.SERVER_BUSY));
    }

    public static String blockHandlerB(BlockException exception){
        return "这是第二个限流降级方法";
    }
}
