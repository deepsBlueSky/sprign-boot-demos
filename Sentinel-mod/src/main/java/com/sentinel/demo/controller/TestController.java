package com.sentinel.demo.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.sentinel.demo.exceptionHandler.SentinelHandler;
import com.sentinel.demo.exception.BizException;
import com.sentinel.demo.exception.CommonEnum;
import com.sentinel.demo.service.TestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-22 10:30
 **/
@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("/index/{name}")
    public String index(@PathVariable String name){
        return testService.circuitBreaker(StringUtils.isBlank(name)?"sentinel":name);
    }

    @GetMapping("/say/{name}")
    @SentinelResource(value = "say",blockHandlerClass = SentinelHandler.class,blockHandler = "blockHandlerA")
    public Object say(@PathVariable String name){
        return testService.say(StringUtils.isBlank(name)?"sentinel":name);
    }

    @GetMapping("/hy/{type}/{name}")
    public String index(@PathVariable int type,@PathVariable String name){
        if(type == 0){
            return testService.circuitBreaker0(StringUtils.isBlank(name)?"张三":name);
        }else if(type == 1){
            return testService.circuitBreaker1(StringUtils.isBlank(name)?"张三":name);
        }else if(type == 2){
            return testService.circuitBreaker2(StringUtils.isBlank(name)?"张三":name);
        }else{
            return testService.circuitBreaker(StringUtils.isBlank(name)?"张三":name);
        }
    }

    @GetMapping("/exception")
    public String exception(){
        throw new BizException(CommonEnum.BODY_NOT_MATCH);
    }
}
