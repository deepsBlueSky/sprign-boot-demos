package com.sentinel.demo.common;

import com.sentinel.demo.exception.BaseErrorInfoInterface;
import com.sentinel.demo.exception.CommonEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-25 11:46
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultBody {
    private String code;
    private String message;
    private Object data;

    public ResultBody(BaseErrorInfoInterface error){
        this.code = error.getResultCode();
        this.message = error.getResultMsg();
    }

    /********************************/
    public static ResultBody SUCCESS(){
        return SUCCESS(null);
    }

    public static ResultBody SUCCESS(Object data){
        return ResultBody.builder().code(CommonEnum.SUCCESS.getResultCode())
                .message(CommonEnum.SUCCESS.getResultMsg())
                .data(data).build();
    }

    public static ResultBody ERROR(BaseErrorInfoInterface error){
        return ERROR(error.getResultCode(), error.getResultMsg(), null);
    }

    public static ResultBody ERROR(String code,String msg){
        return ERROR(code, msg, null);
    }

    public static ResultBody ERROR(String msg){
        return ERROR("-1", msg, null);
    }

    public static ResultBody ERROR(String code,String msg,Object data){
        return ResultBody.builder()
                .code(code)
                .message(msg)
                .data(null)
                .build();
    }
}
