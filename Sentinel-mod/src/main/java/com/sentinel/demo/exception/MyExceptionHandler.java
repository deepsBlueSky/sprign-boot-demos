package com.sentinel.demo.exception;

import com.sentinel.demo.common.ResultBody;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-25 11:28
 **/
@ControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public ResultBody bizExceptionHandler(HttpServletRequest request,BizException e){
        if(e != null){
            return ResultBody.ERROR(e.getErrorCode(),e.getErrorMsg());
        }
        return ResultBody.ERROR(CommonEnum.INTERNAL_SERVER_ERROR);
    }

    /*@ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String exceptionHandler(Exception e){
        System.out.println("未知异常！原因是："+e);
        return e.getMessage();
    }*/
}
