package com.sentinel.demo.exception;

import lombok.Data;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-25 11:34
 **/
@Data
public class BizException extends RuntimeException{
    protected String errorCode;
    protected String errorMsg;

    public BizException(){
        super();
    }

    public BizException(String errorCode,String errorMsg){
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BizException(BaseErrorInfoInterface error){
        this.errorCode = error.getResultCode();
        this.errorMsg = error.getResultMsg();
    }

    public BizException(String errorCode,String errorMsg,Throwable throwable){
        super(errorCode,throwable);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
}
