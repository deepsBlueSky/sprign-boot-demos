package com.sentinel.demo.exception;

public interface BaseErrorInfoInterface {
    //错误码
    String getResultCode();
    //异常描述
    String getResultMsg();
}
