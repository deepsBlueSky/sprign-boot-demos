package com.sentinel.demo.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.sentinel.demo.common.ResultBody;
import com.sentinel.demo.exceptionHandler.SentinelHandler;
import com.sentinel.demo.exception.CommonEnum;
//import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2021-10-22 11:41
 **/
@Service
//@Slf4j
public class TestService {

    public String say(String name){
        return  "hello " + name;
    }

    /**
     * 限流降级
     * @param name
     * @return
     */
    //blockHandler只负责sentinel控制台配置违规
    //fallback只负责业务异常
    //@SentinelResource(value = "sayHello",blockHandler = "sayHelloExceptionHandler")
    //@SentinelResource(value = "sayHello",fallback = "sayHelloExceptionHandler")
    @SentinelResource(value = "sayHello",blockHandlerClass = SentinelHandler.class,blockHandler = "blockHandlerA")
    public String sayHello(String name){
        return  "hello " + name;
    }

    @SentinelResource(value = "circuitBreaker",fallback = "circuitBreakerFallBack",blockHandler = "sayHelloExceptionHandler")
    public String circuitBreaker(String name){
        if("张三".equalsIgnoreCase(name)){
            return "hello" + name;
        }
        throw new RuntimeException("发生异常");
    }

    //响应时间
    @SentinelResource(value = "circuitBreaker0",fallback = "circuitBreakerFallBack",blockHandler = "sayHelloExceptionHandler")
    public String circuitBreaker0(String name){
        if("张三".equalsIgnoreCase(name)){
            return "hello sleep " + name;
        }else{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }finally {
                return "hello sleep 1 seconds " + name;
            }
        }
        //throw new RuntimeException("发生异常");
    }

    //失败率  张三 成功  其他 失败
    @SentinelResource(value = "circuitBreaker1",fallback = "circuitBreakerFallBack",blockHandler = "sayHelloExceptionHandler")
    public String circuitBreaker1(String name){
        if("张三".equalsIgnoreCase(name)){
            return "hello" + name;
        }else{
            throw new RuntimeException("发生异常");
        }
    }

    //次数
    @SentinelResource(value = "circuitBreaker2",fallback = "circuitBreakerFallBack",blockHandler = "sayHelloExceptionHandler")
    public String circuitBreaker2(String name){
        if("张三".equalsIgnoreCase(name)){
            return "hello" + name;
        }
        throw new RuntimeException("发生异常");
    }

    public String circuitBreakerFallBack(String name){
        return "服务异常,熔断降级，请稍后再试！";
    }

    public String sayHelloExceptionHandler(String name, BlockException exception){
        System.out.println("访问过快，限流降级，请稍后再试！");
        return JSON.toJSONString(ResultBody.ERROR(CommonEnum.SERVER_BUSY));
    }
}
