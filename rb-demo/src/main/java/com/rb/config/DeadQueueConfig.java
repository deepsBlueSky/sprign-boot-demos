package com.rb.config;


/**
 * @program: SpringBootDemos
 * @description: 死信队列处理未支付订单
 * @author: Kangsen
 * @create: 2021-08-24 16:36
 **/
public class DeadQueueConfig {
    //业务A队列
    public static final String QUEUE_BUSINESS_A = "queue_business_a";
    //业务B队列
    public static final String QUEUE_BUSINESS_B = "queue_business_b";
    //业务交换机
    public static final String EXCHANGE_BUSINESS = "exchange_business";
    //死信队列
    public static final String QUEUE_DEAD = "queue_dead";
    //死信交换机
    public static final String EXCHANGE_DEAD = "exchange_dead";
    //死信路由
    public static final String ROUTE_DEAD_KEY = "dead.key";

    //延迟队列
    public static final String QUEUE_DELAY = "queue_delay";
    //延迟交换机
    public static final String EXCHANGE_DELAY = "exchange_delay";
    //延迟路由
    public static final String ROUTE_DELAY_KEY = "delay.key";
    //延迟队列的TTL
    public static final Long DELAY_MILL_TIME = 20000L;
}
