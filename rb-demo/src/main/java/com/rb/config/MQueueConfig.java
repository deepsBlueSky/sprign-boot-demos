package com.rb.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: SpringBootDemos
 * @description: 死信队列处理未支付订单
 * @author: Kangsen
 * @create: 2021-08-24 16:36
 **/
@Configuration
public class MQueueConfig {

    //给业务队列A绑上死信
    @Bean("businessQueueA")
    public Queue businessQueueA(){
        Map<String, Object> args = new HashMap<>(2);
        //绑定死信队列
        args.put("x-dead-letter-exchange", DeadQueueConfig.EXCHANGE_DEAD);
        //当前队列的死信路由Key
        args.put("x-dead-letter-routing-key", DeadQueueConfig.ROUTE_DEAD_KEY);
        //当前队列的死信路由Key
        return QueueBuilder.durable(DeadQueueConfig.QUEUE_BUSINESS_A).withArguments(args).build();
    }

    //给业务队列A绑上死信
    @Bean("businessQueueB")
    public Queue businessQueueB(){
        Map<String, Object> args = new HashMap<>(2);
        //绑定死信队列
        args.put("x-dead-letter-exchange", DeadQueueConfig.EXCHANGE_DEAD);
        //当前队列的死信路由Key
        args.put("x-dead-letter-routing-key", DeadQueueConfig.ROUTE_DEAD_KEY);
        return QueueBuilder.durable(DeadQueueConfig.QUEUE_BUSINESS_B).withArguments(args).build();
    }

    //业务交换机
    @Bean("businessExchange")
    public FanoutExchange businessExchange(){
        return new FanoutExchange(DeadQueueConfig.EXCHANGE_BUSINESS);
    }

    //业务队列和业务交换机绑定
    @Bean
    public Binding businessABinding(@Qualifier("businessQueueA") Queue businessQueueA,
                                    @Qualifier("businessExchange") FanoutExchange fanoutExchange){
        return BindingBuilder.bind(businessQueueA).to(fanoutExchange);
    }

    //业务队列和业务交换机绑定
    @Bean
    public Binding businessBBinding(@Qualifier("businessQueueB") Queue businessQueueB,
                                   @Qualifier("businessExchange") FanoutExchange fanoutExchange){
        return BindingBuilder.bind(businessQueueB).to(fanoutExchange);
    }

    //死信队列
    @Bean("deadQueue")
    public Queue deadQueue(){
        return new Queue(DeadQueueConfig.QUEUE_DEAD);
    }

    //死信交换机
    @Bean("deadExchange")
    public DirectExchange deadExchange(){
        return new DirectExchange(DeadQueueConfig.EXCHANGE_DEAD);
    }

    //死信队列和死信交换机绑定
    @Bean
    public Binding deadQueueBinding(@Qualifier("deadExchange") DirectExchange deadExchange,
                                    @Qualifier("deadQueue") Queue deadQueue){
        return BindingBuilder.bind(deadQueue).to(deadExchange).with(DeadQueueConfig.ROUTE_DEAD_KEY);
    }


    /***************************延迟队列**************************/
    @Bean("delayQueue")
    public Queue delayQueue(){
        Map<String,Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange",DeadQueueConfig.EXCHANGE_DEAD);
        args.put("x-dead-letter-routing-key",DeadQueueConfig.ROUTE_DEAD_KEY);
        args.put("x-message-ttl", DeadQueueConfig.DELAY_MILL_TIME);
        return QueueBuilder.durable(DeadQueueConfig.QUEUE_DELAY).withArguments(args).build();
    }

    @Bean("delayExchange")
    public CustomExchange delayExchange(){
        Map<String,Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DeadQueueConfig.EXCHANGE_DELAY,"x-delayed-message",true,false,args);
    }

    @Bean
    public Binding bindingDelayQueue(@Qualifier("delayQueue") Queue delayQueue,
                                     @Qualifier("delayExchange") CustomExchange delayExchange){
        return BindingBuilder.bind(delayQueue).to(delayExchange).with(DeadQueueConfig.ROUTE_DELAY_KEY).noargs();
    }
}
