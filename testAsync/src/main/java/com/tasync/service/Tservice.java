package com.tasync.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tasync.handler.TestHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-25 14:58
 **/
@Component
@Slf4j
public class Tservice {

    @Resource
    private TestHandler testHandler;

    @Async
    public void t1(){
        try {
            log.info("=================t1 start");
            TimeUnit.SECONDS.sleep(5);
            log.info("=================t1 end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void t2(){
        testHandler.t3();
    }


    public static void main(String[] args) {
        String battle = "{\"code\":\"1\",\"isViodeoTurn\":0,\"matchId\":5356686,\"msg\":\"成功\",\"teamStandingMap\":{\"103070\":{\"controlStr\":\"59.0%\",\"cornerKick\":6,\"home\":1,\"passRateStr\":\"0.0%\",\"rcard\":0,\"shoot\":14,\"shootOn\":2,\"ycard\":1},\"98585\":{\"controlStr\":\"41.0%\",\"cornerKick\":3,\"home\":0,\"passRateStr\":\"0.0%\",\"rcard\":0,\"shoot\":6,\"shootOn\":0,\"ycard\":5}}}";
        JSONObject jsonObject = JSON.parseObject(battle);
        JSONObject teamStandingMap = jsonObject.getJSONObject("teamStandingMap");
        Iterator<String> keys = teamStandingMap.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            log.info("key = {}",key);

            JSONObject mapJSONObject = teamStandingMap.getJSONObject(key);
            log.info("teamStandingMap:{}",teamStandingMap);
        }
    }
}
