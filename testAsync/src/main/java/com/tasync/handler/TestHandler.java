package com.tasync.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-25 15:05
 **/
@Component
@Slf4j
public class TestHandler {
    @Async
    public void t3(){
        try {
            log.info("=================t3 start");
            TimeUnit.SECONDS.sleep(5);
            log.info("=================t3 end");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
