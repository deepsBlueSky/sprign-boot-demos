package com.tasync.controller;

import com.tasync.service.Tservice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-25 14:57
 **/
@RestController
public class TestController {

    @Resource
    private Tservice tservice;

    @GetMapping("/test1")
    public String test(){
        tservice.t1();
        return "success";
    }

    @GetMapping("/test2")
    public String test2(){
        tservice.t2();
        return "success";
    }
}
