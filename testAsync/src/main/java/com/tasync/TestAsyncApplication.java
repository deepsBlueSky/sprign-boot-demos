package com.tasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @program: SpringBootDemos
 * @description:
 * @author: Kangsen
 * @create: 2022-08-25 14:56
 **/
@SpringBootApplication
@EnableAsync
public class TestAsyncApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestAsyncApplication.class,args);
    }
}
