package com.testJob.job;

import com.testJob.service.ExecuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Administrator
 * @Date：2022/8/22/0022 20:58
 * @Versiion：1.0
 */
@Component
@Slf4j
public class IndexJob {

    @Resource
    private ExecuService execuService;

//    @Scheduled(cron = "0/2 * * * * ?")
    public void job1(){
        try {
            execuService.crawlerMatch();
            TimeUnit.MILLISECONDS.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//    @Scheduled(cron = "0/2 * * * * ?")
    public void job2(){
        execuService.crawlerMatchScore();
    }

    @Scheduled(cron = "0/2 * * * * ?")
    public void job3(){
        execuService.crawlerMatchEvent();
    }
}
