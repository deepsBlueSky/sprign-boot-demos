package com.testJob.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Author：Administrator
 * @Date：2022/8/22/0022 22:00
 * @Versiion：1.0
 */
@Component
@Slf4j
public class ExecuService {

    public void crawlerMatch(){
        log.info("{} 抓足球赛事。。。。",Thread.currentThread().getName());
    }

    @Async
    public void crawlerMatchScore(){
        try {
            log.info("{} 抓足球比分。。。。",Thread.currentThread().getName());
            TimeUnit.MILLISECONDS.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async("asyncTaskExecutorJob")
    public void crawlerMatchEvent(){
        try {
            log.info("{} 抓足球赛况。。。。",Thread.currentThread().getName());
            TimeUnit.MILLISECONDS.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
