package com.testJob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author：Administrator
 * @Date：2022/8/22/0022 20:57
 * @Versiion：1.0
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class SpringJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringJobApplication.class,args);
    }
}
